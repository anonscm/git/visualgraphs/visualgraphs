<?php

/**
 * Description of Elements
 */

class Element
{
    protected $_nom = ""; // Nom de l'élément (nom dans le fichier de sortie)
    protected $_titre = ""; // Titre de l'élément (nom d'affichage)
    protected $_valeur = ""; // Valeur de l'élément
    protected $_aide = ""; // Texte d'aide lier à l'élément
    
    
    function __construct($tabData) 
    {
        
    }
    
    /////////////////////////////////
    // Initialisation de l'élément //
    /////////////////////////////////
    public function init($tabData)
    {
        $this->_nom = $tabData["nom"];
        
        $this->_titre = $tabData["titre"];
        
        $this->_valeur = $tabData["defaut"];
        
        $this->_aide = $tabData["aide"];
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    // Récupération du contenu de l'élément à partir des données du fichier json //
    ///////////////////////////////////////////////////////////////////////////////
    public function recupContenu($tabData, $tab_liens)
    {
        $valeur = $tabData;
        
        foreach ($tab_liens as $tl)
        {
            if(isset($valeur[$tl]))
                $valeur = $valeur[$tl];
        }
	
        if(isset($valeur[$this->_nom]))
            $valeur = $valeur[$this->_nom];

        if(gettype($valeur) != "array")
            $this->_valeur = $valeur;
    }
    
    ////////////////////////////
    // Affichage de l'élément //
    ////////////////////////////
    public function affichage($nom_categorie)
    {

    }
}
