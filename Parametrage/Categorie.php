<?php

/////////////////////////////////////////////////////////////////////
// Catégorie normal, contenant des sous-catégories et des éléments //
/////////////////////////////////////////////////////////////////////

class Categorie 
{
    protected $_contenu = array(); // Liste des sous-catégies et des éléments
    
    protected $_titre = ""; // Titre de la catégorie
    
    protected $_nom = ""; // Nom de la catégorie (nom dans le fichier de sortie)
    
    protected $_niveau = 1; // Niveau du titre
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // Récupération des informations de la catégorie et création des sous-catégories et des éléments //
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    function __construct($tabData, $niveau = 1, $nom = "") 
    {
        $this->_contenu = array();
        
        $this->_niveau = $niveau;
        
        // Récupération titre et nom pour les catégorie récupérer automatiquement
        $this->_titre = $nom;
        //$this->_titre = str_replace("_", " ", $this->_titre);
        $this->_nom = $nom;
        
        foreach ($tabData as $key => $tabContenu)
        {
            // Récupération du titre et du nom si vide
            if($key == "titre" && $this->_titre == "")
                $this->_titre = $tabContenu;
            else if($key == "nom" && $this->_nom == "")
                $this->_nom = $tabContenu;
            
            // Récupération du type, lancement de la récupération des types automatiques
            else if($key == "type")
            {
                if($tabContenu == "types_noeuds")
                {
                    if(isset($tabData["categorie"]) && $tabData["categorie"]["type"] == "liste")
                        $this->recupTypesAuto($tabData, 'noeuds', 'liste');
                    else
                        $this->recupTypesAuto($tabData, 'noeuds');
                }
                else if($tabContenu == "types_liens")
                {
                    if(isset($tabData["categorie"]) && $tabData["categorie"]["type"] == "liste")
                        $this->recupTypesAuto($tabData, 'liens', 'liste');
                    else
                        $this->recupTypesAuto($tabData, 'liens');
                }
                else if($tabContenu == "proprietes_noeuds")
                    $this->recupProprietesAuto($tabData, 'noeud');
                else if($tabContenu == "proprietes_liens")
                    $this->recupProprietesAuto($tabData, 'lien');
            }
            
            // Création des sous-catégorie et des éléments
            else if(strstr($key, "categorie") && $tabContenu['nom'] != "Defaut_Invisible")
            {
                if(isset($tabContenu["type"]) && $tabContenu["type"] == "liste")
                    $this->_contenu[count($this->_contenu)] = new CategorieListe($tabContenu, $this->_niveau + 1);
                else
                    $this->_contenu[count($this->_contenu)] = new Categorie($tabContenu, $this->_niveau + 1);
            }
            else if(strstr($key, "element") && $tabContenu['nom'] != "Defaut_Invisible")
            {
                if($tabContenu["type"] == "texte")
                    $this->_contenu[count($this->_contenu)] = new ElementTexte($tabContenu);
                else if($tabContenu["type"] == "color")
                    $this->_contenu[count($this->_contenu)] = new ElementCouleur($tabContenu);
                else if($tabContenu["type"] == "nombre")
                    $this->_contenu[count($this->_contenu)] = new ElementNombre($tabContenu);
                else if($tabContenu["type"] == "check")
                    $this->_contenu[count($this->_contenu)] = new ElementCheck($tabContenu);
                else if($tabContenu["type"] == "liste")
                    $this->_contenu[count($this->_contenu)] = new ElementListe($tabContenu, $this->_nom);
                else if($tabContenu["type"] == "file")
                    $this->_contenu[count($this->_contenu)] = new ElementFile($tabContenu);
            }
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////
    // Récupération de la liste des types de noeuds ou de lien et crée les catégories associées //
    //////////////////////////////////////////////////////////////////////////////////////////////
    public function recupTypesAuto($tabData, $element, $type = 'normal')
    {
        if($element == "noeuds")
            $types = Requetes::$listeTypesNoeuds;
        else if($element == "liens")
            $types = Requetes::$listeTypesLiens;
        
        for($i = 0; $i < count($types); $i++)
        {
            if($type == "liste")
                $this->_contenu[count($this->_contenu)] = new CategorieListe($tabData['categorie'], $this->_niveau + 1, $types[$i]);
            else
                $this->_contenu[count($this->_contenu)] = new Categorie($tabData['categorie_Defaut'], $this->_niveau + 1, $types[$i]);
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////
    // Récupération de la liste des types de noeuds ou de lien et crée les catégories associées //
    //////////////////////////////////////////////////////////////////////////////////////////////
    public function recupProprietesAuto($tabData, $element)
    {
        if($element == "noeud")
            $proprietes = Requetes::$listeProprietesNoeuds[$this->_nom];
        else if($element == "lien")
            $proprietes = Requetes::$listeProprietesLiens[$this->_nom];
        
        for($i = 0; $i < count($proprietes); $i++)
        {
            $this->_contenu[count($this->_contenu)] = new ElementCheck($tabData['element_Defaut'], $proprietes[$i]);
        }
    }
    
    /////////////////////////////////////////////////////////
    // Récupération du contenu des données de la catégorie //
    /////////////////////////////////////////////////////////
    public function recupContenu($tabData, $tab_liens)
    {
        $tab_liens[] = $this->_nom;
        
        // Récupération du contenu des sous-catégorie et des éléments
        foreach ($this->_contenu as $e)
        {
            $e->recupContenu($tabData, $tab_liens);
        }
    }
    
    /////////////////////////////////////////////////
    // Affichage de la catégorie et de son contenu //
    /////////////////////////////////////////////////
    public function affichage($nom_categorie = "")
    {   
        if($this->_niveau != 1)
             echo '<fieldset id="'.$nom_categorie.'">';
        
        if($this->_niveau == 1)
            echo "<hr><hr><details open><summary class='titre_params1'>".str_replace("_", " ", $this->_titre)."</summary>";
        else if($this->_niveau == 2)
            echo "<legend><h3 class='titre_params2'>".str_replace("_", " ", $this->_titre)."</h3></legend>";
        else if($this->_niveau == 3)
            echo "<legend><h4 class='titre_params2'>".str_replace("_", " ", $this->_titre)."</h4></legend>";
        else if($this->_niveau == 4)
            echo "<legend><h5 class='titre_params2'>".str_replace("_", " ", $this->_titre)."</h5></legend>";
        
        $nom_categorie .= $this->_nom.'-';

        foreach ($this->_contenu as $e)
        {
            $e->affichage($nom_categorie);
        }
        
        // Si vide afficher message
        if(count($this->_contenu) == 0)
            echo '<p>Catégorie vide</p>';
        
        if($this->_niveau == 1)
            echo '</details>';
        
        if($this->_niveau != 1)
            echo '</fieldset>';
    }
}
