<?php

///////////////////////////////////////////////////////////////////////
// Classe permettant de récupérer et de stocker le nombre de requête //
///////////////////////////////////////////////////////////////////////

class ValeursParams 
{
    public static $_nb_requete = 0; // Contient le nombre de requête 'général' (menu Recherche)
    public static $_nb_requete_noeuds = array(); // Contient le nombre de requête liés au noeuds pour chaques type de noeuds (menu Informations)
    public static $_nb_requete_liens = array(); // Contient le nombre de requête liés au lien pour chaques type de noeuds (menu Informations)
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lecture du fichier de paramétrage et stocke du nombres de requêtes de chaque types (indispensable pour les listes) //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function lireFichier($nomFichier)
    {
        $data = array();

        if(file_exists($nomFichier))
        {
            $fichier = fopen ($nomFichier, "r");
            $contenu = "";

            // Lecture du contenu du fichier
            while (!feof($fichier))
            { 
                $contenu .= fgets($fichier, 4096);
            }

            $tabData = json_decode($contenu, true); // Décodage (JSON -> Tableau PHP)

            fclose($fichier);

            // Récupération des valeurs, à modifier si besoin
			if(isset($tabData["BoutonsRequetes"]))
				self::$_nb_requete = count($tabData["BoutonsRequetes"]);

			if(isset($tabData["BoutonsRequetesNoeuds"]))
			{
				foreach ($tabData["BoutonsRequetesNoeuds"] as $key => $el)
					self::$_nb_requete_noeuds[$key] = count($tabData["BoutonsRequetesNoeuds"][$key]);
			}
			
			if(isset($tabData["BoutonsRequetesLiens"]))
			{
				foreach ($tabData["BoutonsRequetesLiens"] as $key => $el)
					self::$_nb_requete_liens[$key] = count($tabData["BoutonsRequetesLiens"][$key]);
			}
        }
    }
}
