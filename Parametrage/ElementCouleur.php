<?php

/**
 * Description of ElementCouleur
 */

class ElementCouleur extends Element
{
    /////////////////////////////////
    // Initialisation de l'élément //
    /////////////////////////////////
    function __construct($tabData) 
    {
        $this->init($tabData);
    }

    ////////////////////////////
    // Affichage de l'élément //
    ////////////////////////////
    public function affichage($nom_categorie)
    {
	echo '<abbr title="'.$this->_aide.'">';
        
        echo '<p class="texte_params"><div class="nom_element">'.$this->_titre.' : </div>';
        echo '<input name="'.$nom_categorie.''.$this->_nom.'-couleur" id="'.$nom_categorie.''.$this->_nom.'-couleur" class="jscolor" value="'.$this->_valeur.'" /><br/>';
        echo '</p>'; 

        echo '</abbr>';
    }
}
