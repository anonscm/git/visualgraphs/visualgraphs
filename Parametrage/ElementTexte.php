<?php

/**
 * Description of ElementTexte
 */

class ElementTexte extends Element 
{
    protected $_type_champ = "normal"; // Type du champ (normal, area ou grand)
    
    
    /////////////////////////////////
    // Initialisation de l'élément //
    /////////////////////////////////
    function __construct($tabData) 
    {
        $this->init($tabData);
        
        if(isset($tabData["type_champ"]))
            $this->_type_champ = $tabData["type_champ"];
    }

    ////////////////////////////
    // Affichage de l'élément //
    ////////////////////////////
    public function affichage($nom_categorie)
    {
        echo '<abbr title="'.$this->_aide.'">';
        
        echo '<p class="texte_params"><div class="nom_element">'.$this->_titre.' : </div>';
        
        if($this->_type_champ == "area")
            echo '<textarea name="'.$nom_categorie.''.$this->_nom.'-texte" id="'.$nom_categorie.''.$this->_nom.'-texte" rows="2" class="champ_texte_'.$this->_type_champ.'" />'.$this->_valeur.'</textarea> <br/>';
        else
            echo '<input name="'.$nom_categorie.''.$this->_nom.'-texte" id="'.$nom_categorie.''.$this->_nom.'-texte" value="'.$this->_valeur.'" class="champ_texte_'.$this->_type_champ.'" /><br/>';
        
        echo '</p>';
        
        echo '</abbr>';
    }
}
