<?php

/**
 * Description of ElementTexte
 */

class ElementNombre extends Element 
{
    protected $_step = 0.01; // Précision de l'élément
    
    /////////////////////////////////
    // Initialisation de l'élément //
    /////////////////////////////////
    function __construct($tabData) 
    {
        $this->init($tabData);
        
        if(isset($tabData["precision"]))
            $this->_step = $tabData["precision"];
    }

    ////////////////////////////
    // Affichage de l'élément //
    ////////////////////////////
    public function affichage($nom_categorie)
    {
        echo '<abbr title="'.$this->_aide.'">';
        
        echo '<p class="texte_params"><div class="nom_element">'.$this->_titre.' : </div>';
        echo '<input name="'.$nom_categorie.''.$this->_nom.'-nombre" step="'.$this->_step.'" id="'.$nom_categorie.''.$this->_nom.'-nombre" type="number" value="'.$this->_valeur.'" /><br/>';
        echo '</p>'; 
        
        echo '<abbr>';
    }
}
