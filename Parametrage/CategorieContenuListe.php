<?php

///////////////////////////////////////////////////////////////////////////////
// Catégorie de gestion du contenu des listes d'éléments (liste des requête) //
///////////////////////////////////////////////////////////////////////////////

class CategorieContenuListe extends Categorie
{
    ///////////////////////////////////////////
    // Création des éléments de la catégorie //
    ///////////////////////////////////////////
    function __construct($tabData, $niveau = 1, $nom = "") 
    {
        parent::__construct($tabData, $niveau, $nom);
        
        $this->_titre = "";
    }
    
    /////////////////////////////////////////////////////////
    // Récupération du contenu des données de la catégorie //
    /////////////////////////////////////////////////////////
    public function recupContenu($tabData, $tab_liens, $numeroLecture = -1)
    {
        // Si c'est un bouton de requête général (BoutonsRequetes)
        if(count($tab_liens) == 1) 
        {
            if(isset($tabData[$tab_liens[0]]) && isset(array_keys($tabData[$tab_liens[0]])[$numeroLecture]))
                $tab_liens[] = array_keys($tabData[$tab_liens[0]])[$numeroLecture];
        }
        else // Sinon c'est un bouton de requête lier à un élément (BoutonsRequetesNoeuds ou BoutonsRequetesLien)
        {
            if(isset($tabData[$tab_liens[0]][$tab_liens[1]]) && isset(array_keys($tabData[$tab_liens[0]][$tab_liens[1]])[$numeroLecture]))
                $tab_liens[] = array_keys($tabData[$tab_liens[0]][$tab_liens[1]])[$numeroLecture];
        }
        
        // Récupération des éléménets composant la liste
        $i = 0;
        foreach ($this->_contenu as $e)
        {
            $e->recupContenu($tabData, $tab_liens, $i);
            $i ++;
        }
    }
    
    /////////////////////////////////////////////////
    // Affichage de la catégorie et de son contenu //
    /////////////////////////////////////////////////
    public function affichage($nom_categorie = "")
    {
        $nom_categorie .= $this->_nom.'-';
        
        if($this->_niveau != 1)
             echo '<fieldset id="'.$nom_categorie.'">';
        
        if($this->_niveau == 1)
            echo "<hr><hr><h2 class='titre_params1'>".$this->_titre."</h2>";
        else if($this->_niveau == 2)
            echo "<legend><h3 class='titre_params2'>".$this->_titre."</h3></legend>";
        else if($this->_niveau == 3)
            echo "<legend><h4 class='titre_params2'>".$this->_titre."</h4></legend>";
        else if($this->_niveau == 4)
            echo "<legend><h5 class='titre_params2'>".$this->_titre."</h5></legend>";
        
        foreach ($this->_contenu as $e)
        {
            $e->affichage($nom_categorie);
        }
        
        echo '<input id="bouton_retrait_'.$nom_categorie.'" type="button" value="Retirer la requête" />';
        
        if($this->_niveau != 1)
            echo '</fieldset>';
        
        $this->gestionRetraitList($nom_categorie);
    }

    ////////////////////////////////
    // Supression de la catégorie //
    ////////////////////////////////
    public function gestionRetraitList($nom_categorie)
    {
        $scriptEventCacher = '<script>';
        $scriptEventCacher .= '$("#bouton_retrait_'.$nom_categorie.'" ).click(function() {';
        $scriptEventCacher .= '$("#' . $nom_categorie . '").remove();';
        $scriptEventCacher .= '});';
        $scriptEventCacher .= '</script>';
        echo $scriptEventCacher;
    }
}
