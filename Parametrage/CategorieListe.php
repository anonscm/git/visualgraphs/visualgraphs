<?php

////////////////////////////////////////////////////////////////////
// Catégorie de gestion des listes d'éléments (liste des requête) //
////////////////////////////////////////////////////////////////////

class CategorieListe extends Categorie
{

    ///////////////////////////////////////////
    // Création des éléments de la catégorie //
    ///////////////////////////////////////////
    function __construct($tabData, $niveau = 1, $nom = "")
    {
        parent::__construct($tabData, $niveau, $nom);
        
        // Création des catégories a partir du nombre de requêtes dans le fichier 
        if($tabData['nom_liste'] == "BoutonsRequetes")
        {
            while(count($this->_contenu) < ValeursParams::$_nb_requete)
                $this->_contenu[count($this->_contenu)] = new CategorieContenuListe($tabData['categorie_Defaut'], $this->_niveau + 1, "b".count($this->_contenu));
        }
        else if($tabData['nom_liste'] == "BoutonsRequetesNoeuds" && isset(ValeursParams::$_nb_requete_noeuds[$nom]))
        {
            while(count($this->_contenu) < ValeursParams::$_nb_requete_noeuds[$nom])
                $this->_contenu[count($this->_contenu)] = new CategorieContenuListe($tabData['categorie_Defaut'], $this->_niveau + 1, "b".count($this->_contenu));
        }
        else if($tabData['nom_liste'] == "BoutonsRequetesLiens" && isset(ValeursParams::$_nb_requete_liens[$nom]))
        {
            while(count($this->_contenu) < ValeursParams::$_nb_requete_liens[$nom])
                $this->_contenu[count($this->_contenu)] = new CategorieContenuListe($tabData['categorie_Defaut'], $this->_niveau + 1, "b".count($this->_contenu));
        }
        
        // Gestion de l'ajout des éléments (si click bouton)
        if(isset($_GET['action']) && $_GET['action'] == "ajout")
        {
            if(isset($_GET['nom']) && $_GET['nom'] == $this->_nom)
            {
                $this->_contenu[count($this->_contenu)] = new CategorieContenuListe($tabData['categorie_Defaut'], $this->_niveau + 1, "b".count($this->_contenu));
            }
        }
    }
    
    /////////////////////////////////////////////////////////
    // Récupération du contenu des données de la catégorie //
    /////////////////////////////////////////////////////////
    public function recupContenu($tabData, $tab_liens, $numeroLecture = -1)
    {
        // 
        if($numeroLecture == -1)
            $tab_liens[] = $this->_nom;
        else
            $tab_liens[] = array_keys($tabData[$tabData['nom_liste']])[$numeroLecture];
        
        // 
        $i = 0;
        foreach ($this->_contenu as $e)
        {
            $e->recupContenu($tabData, $tab_liens, $i);
            $i ++;
        }
    }
    
    /////////////////////////////////////////////////
    // Affichage de la catégorie et de son contenu //
    /////////////////////////////////////////////////
    public function affichage($nom_categorie = "")
    { 
        if($this->_niveau != 1)
             echo '<fieldset>';
        
        if($this->_niveau == 1)
            echo "<hr><hr><details open><summary class='titre_params1'>".str_replace("_", " ", $this->_titre)."</summary>";
        else if($this->_niveau == 2)
            echo "<legend><h3 class='titre_params2'>".str_replace("_", " ", $this->_titre)."</h3></legend>";
        else if($this->_niveau == 3)
            echo "<legend><h4 class='titre_params2'>".str_replace("_", " ", $this->_titre)."</h4></legend>";
        else if($this->_niveau == 4)
            echo "<legend><h5 class='titre_params2'>".str_replace("_", " ", $this->_titre)."</h5></legend>";
        
        $nom_categorie .= $this->_nom.'-';
        
        echo '<input id="bouton_ajout_'.$this->_nom.'" type="button" value="Ajouter une requête" />';

        foreach ($this->_contenu as $e)
        {
            $e->affichage($nom_categorie);
        }
        
        if($this->_niveau == 1)
            echo '</details>';
        
        if($this->_niveau != 1)
            echo '</fieldset>';
        
        $this->gestionAjoutList($nom_categorie);
    }
    
    //////////////////////////////////////////////////////////////////
    // Gestion de l'ajout d'un element ou d'un catégorie à la liste //
    //////////////////////////////////////////////////////////////////
    public function gestionAjoutList($nom_categorie)
    {
        $scriptEventCacher = '<script>';
        $scriptEventCacher .= '$("#bouton_ajout_'.$this->_nom.'" ).click(function() {';
        $scriptEventCacher .= '$("#formulaire").attr("action", "?action=ajout&nom='.$this->_nom.'#'.$nom_categorie.'b'.count($this->_contenu).'-");';
        $scriptEventCacher .= '$("#formulaire").submit();';
        $scriptEventCacher .= '});';
        $scriptEventCacher .= '</script>';
        echo $scriptEventCacher; 
    }
}
