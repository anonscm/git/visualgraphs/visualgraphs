<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        
        <title>VisualGraphs - Paramètrage</title>
        
        <link rel="icon" type="image/png" href="../img/favicon.png" />
        <link rel="stylesheet" type="text/css" href="../style.css">
        <link rel="icon" type="image/png" href="../VisualGraphs/img/favicon.png" />
		
        <script src="../VisualGraphs/libsJS/jscolor.js"></script>
        <script type="text/javascript" src="../VisualGraphs/libsJS/jquery-3.1.1.min.js"></script>
    </head>
    <body>
        <?php
            // Include relatif au menus de paramétrage
            include 'GestionnaireParametrage.php';
            
            include 'Categorie.php';
            include 'CategorieListe.php';
            include 'CategorieContenuListe.php';
             
            include 'Element.php';
            include 'ElementCouleur.php';
            include 'ElementTexte.php';
            include 'ElementNombre.php';
            include 'ElementListe.php';
            include 'ElementCheck.php';
            include 'ElementFile.php';
            
            include 'Requetes.php';

            include 'Sauvegarde.php';
            include 'ChargeurFichier.php';
            
            include 'ValeursParams.php';
            
            
            $nomFichier = '../VisualGraphs/json/Params.json';

            // Gestion des actions de sauvegarde et de chargement des données
            if(count($_GET) > 0 && isset($_GET['action']))
            {
                if($_GET['action'] == "sauvegarde")
                    Sauvegarde::sauvegarder($nomFichier);
                else if($_GET['action'] == "upload")
                    ChargeurFichier::charger($nomFichier);
            }
            
            ValeursParams::lireFichier($nomFichier);
            
            $r = new Requetes();
            $r->chargerDonnees();
            
            $gestParams = new GestionnaireParametrage('format_param.json', $nomFichier);
        ?>
    </body>
</html>
