<?php

/**
 * Description of ElementTexte
 */

class ElementListe extends Element 
{
    private $_contenu = array(); // Tableau contenu les éléments de la liste
    
    
    /////////////////////////////////
    // Initialisation de l'élément //
    /////////////////////////////////
    function __construct($tabData, $nomCategorie) 
    {
        $this->init($tabData);
        
       if(stristr($tabData["type_categorie"], 'nom'))
       {
            if(stristr($tabData["type_categorie"], 'noeud') && array_key_exists($nomCategorie, Requetes::$listeProprietesNoeuds))
                $this->_contenu = Requetes::$listeProprietesNoeuds[$nomCategorie];
            else if(stristr($tabData["type_categorie"], 'lien') && array_key_exists($nomCategorie, Requetes::$listeProprietesLiens))
            {
                if($nomCategorie != "Defaut")
                    $this->_contenu = array_merge(array($nomCategorie), Requetes::$listeProprietesLiens[$nomCategorie]); // Récupération des propriétés des liens + ajout de son nom dans le tableau
                else
                    $this->_contenu = Requetes::$listeProprietesLiens[$nomCategorie];
            }
       }
       else if(stristr($tabData["type_categorie"], 'taille'))
       {
            if(stristr($tabData["type_categorie"], 'noeud') && array_key_exists($nomCategorie, Requetes::$listeProprietesNoeuds))
            {
                $this->_contenu = array_merge(array(""), Requetes::$listeProprietesNoeuds[$nomCategorie]);
            }
            else if(stristr($tabData["type_categorie"], 'lien') && array_key_exists($nomCategorie, Requetes::$listeProprietesLiens))
            {
                $this->_contenu = array_merge(array(""), Requetes::$listeProprietesLiens[$nomCategorie]);
            }
       }
       
    }

    /////////////////////////////////////////////////////////////////
    // Affichage de l'élément, sous forme de liste avec le contenu //
    /////////////////////////////////////////////////////////////////
    public function affichage($nom_categorie)
    {
        echo '<abbr title="'.$this->_aide.'">';
        
        echo '<p class="texte_params"><div class="nom_element">'.$this->_titre.' : </div>';
        echo '<select id="'.$nom_categorie.''.$this->_nom.'-list" name="'.$nom_categorie.''.$this->_nom.'-list">';
        
        for($i = 0; $i < count($this->_contenu); $i++)
        {
            if($this->_contenu[$i] == $this->_valeur)
                echo '<option value="'.$this->_contenu[$i].'" selected>'.$this->_contenu[$i].'</option>';
            else
                echo '<option value="'.$this->_contenu[$i].'">'.$this->_contenu[$i].'</option>';
        }
        
        echo '</select>';
        echo '</p>';
        
        echo '</abbr>';
    }
}
