<?php

///////////////////////////////////////////////////////////////////
// Script permettant de gérer le chargement de fichiers externes //
///////////////////////////////////////////////////////////////////
class ChargeurFichier 
{
    function __construct() 
    {
        
    }
    
    /////////////////////////////////////////////////////////
    // Remplace le fichier de paramétrage par celui choisi //
    /////////////////////////////////////////////////////////
    static function charger($fichierDestination)
    {
        // Création d'un fichier de sauvegarde avec les ancien paramètres
        $fichierSauvegarde = substr($fichierDestination, 0, strlen($fichierDestination) - 5)."_sav.json";
        unlink($fichierSauvegarde);
        copy($fichierDestination, $fichierSauvegarde);
        
        // Suppression du fichier de destination s'il existe
        unlink($fichierDestination);
        
        // Copie le fichier selectionné à la place 
        copy($_FILES['fichier']['tmp_name'], $fichierDestination);
        
        
        echo '<p class="message_donnees">Le fichier à été chargé.</p><br/><br/>';
    }
    
}