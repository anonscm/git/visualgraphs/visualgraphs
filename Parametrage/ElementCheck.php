<?php

/**
 * Description of ElementTexte
 */

class ElementCheck extends Element 
{
    /////////////////////////////////
    // Initialisation de l'élément //
    /////////////////////////////////
    function __construct($tabData, $nom = "") 
    {
        $this->init($tabData);
        
        if($nom != "")
        {
            $this->_nom = $nom;
            $this->_titre = $nom;
        }
    }

    ////////////////////////////
    // Affichage de l'élément //
    ////////////////////////////
    public function affichage($nom_categorie)
    {
        echo '<abbr title="'.$this->_aide.'">';
        
        echo '<p class="texte_params">';
        
        echo '<input type="hidden" name="'.$nom_categorie.''.$this->_nom.'-check" value="0" />'; // Création d'un champ cacher du même nom (Met le champ à 0 si la checkbox est décoché)
        
        if($this->_valeur == 1)
            echo '<input type="checkbox" class="check" name="'.$nom_categorie.''.$this->_nom.'-check" id="'.$nom_categorie.''.$this->_nom.'-check" style="vertical-align: middle" checked/>';
        else 
            echo '<input type="checkbox" class="check" name="'.$nom_categorie.''.$this->_nom.'-check" id="'.$nom_categorie.''.$this->_nom.'-check" style="vertical-align: middle"/>';
        
        echo '<label for="'.$nom_categorie.''.$this->_nom.'-check">  '.str_replace("_", " ", $this->_titre).' </p></label>'; 
        
        echo '</abbr>';
    }
}
