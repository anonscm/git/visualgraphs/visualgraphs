<?php

class Sauvegarde 
{

    function __construct() 
    {
        
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    // Sauvegarde des paramétrages choisi vers le fichier json indiqué en entrée //
    ///////////////////////////////////////////////////////////////////////////////
    static function sauvegarder($nomFichier)
    {
        $fichier = fopen($nomFichier, "w+");
        
        $data = array(); // Contient les données sous forme de tableau

        foreach ($_POST as $key => $val) // Parcours toutes les viables POST
        {
            $tabKey = explode('-', $key); // Découpage de la chaine de caractère du nom de l'éléménent en 1 tableau

            // Tranformation des données selon leur type
            if($tabKey[count($tabKey) - 1] == "nombre") // Si c'est un nombre on le convertir
                $val = floatval($val);
            else if($tabKey[count($tabKey) - 1] == "couleur") // Si c'est une couleur on ajoute un '#'
                $val = '#' . $val;
            else if($tabKey[count($tabKey) - 1] == "check" && $val == "on") // Si c'est une checkbox on met sa valeur à 1 à on
                $val = 1;
            
            // Transformation des données en tableau
            if(count($tabKey) == 2)
                $data[$tabKey[0]] = $val;
            else if(count($tabKey) == 3)
                $data[$tabKey[0]][$tabKey[1]] = $val;
            else if(count($tabKey) == 4)
                $data[$tabKey[0]][$tabKey[1]][$tabKey[2]] = $val;
            else if(count($tabKey) == 5)
                $data[$tabKey[0]][$tabKey[1]][$tabKey[2]][$tabKey[3]] = $val;
        }

        // Tranformation du tableu de donnée en json et écriture
        fwrite($fichier, json_encode($data, JSON_PRETTY_PRINT));

        fclose($fichier);
        
        echo '<p class="message_donnees">Les modifications ont étés sauvegardées.</p><br/><br/>';
    }
    
    
    
}