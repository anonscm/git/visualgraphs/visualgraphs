﻿<?php

////////////////////////////////////
// Fichier contenant les requêtes //
////////////////////////////////////

// Includes relatifs à la connexion à Neo4J
require_once '../VisualGraphs/vendorPHP/autoload.php';
use GraphAware\Neo4j\Client\ClientBuilder;

include_once("../VisualGraphs/ScriptsPHP/Connexion.php");


class Requetes
{
    public static $client; // Objet permettant de garder la connexion à Neo4J
    public static $listeTypesNoeuds = array(); // Contient la liste des noeuds
    public static $listeTypesLiens = array(); // Contient la liste des liens 
    public static $listeProprietesNoeuds = array(); // Contient la liste des proprietés des noeuds
    public static $listeProprietesLiens = array(); // Contient la liste des proprietés des liens
    
    function __construct() 
    {
        
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // Lancement des requêtes de chargement des données depuis la base Neo4J //
    ///////////////////////////////////////////////////////////////////////////
    public function chargerDonnees()
    {
        try {
            // Connexion à Neo4J
            self::$client = ClientBuilder::create()
                ->addConnection('bolt', RecupLoginNEO4J())
                ->build();
        
            self::$listeTypesNoeuds = $this->recupTypesNoeuds();
            self::$listeTypesLiens = $this->recupTypesLiens();
            
            if(count(self::$listeTypesNoeuds) > 0)
                self::$listeProprietesNoeuds = $this->recupToutesProprietes(self::$listeTypesNoeuds, 'noeuds');
            
            if(count(self::$listeTypesLiens) > 0)
                self::$listeProprietesLiens = $this->recupToutesProprietes(self::$listeTypesLiens, 'liens');

        } catch (Exception $e)
        {
            echo '<h2 class="message_echec_connexion">Echec de la connexion au serveur de Neo4J</h2>';

            return null;
        }
    }
    
    //////////////////////////////////////
    // Récupération des types de noeuds //
    //////////////////////////////////////
    function recupTypesNoeuds()
    {		
        $requete = "MATCH (n) RETURN distinct labels(n)";
        
        $labels = array();
        
        $result = self::$client->run($requete);
        foreach ($result->getRecords() as $record)
        {
            $tab = $record->values();

            $labels[count($labels)] = $tab[0][0];
        }
        
        return $labels;
    }

    /////////////////////////////////////
    // Récupération des types de liens //
    /////////////////////////////////////
    function recupTypesLiens()
    {
        $requete = "MATCH (n1)-[r]-(n2) return distinct type(r)"; 

        $types_relations = array();

        $result = self::$client->run($requete);
        foreach ($result->getRecords() as $record)
        {
            $tab = $record->values();

            for($i = 0; $i < count($tab); $i++)
                $types_relations[count($types_relations)] = $tab[$i];
        }

        return $types_relations;
    }

    //////////////////////////////////////////////////////////
    // Récupération des propriétés de chaque type de noeuds //
    //////////////////////////////////////////////////////////
    function recupToutesProprietes($listTypes, $type)
    {
        $requete = "";

        // Création de la requête à partir des types de noeuds
        for ($i = 0; $i < count($listTypes); $i++)
        {
            if($i > 0)
                $requete .= " UNION ALL ";

            if($type == 'noeuds')
                $requete .= "MATCH (p:".$listTypes[$i].") WITH DISTINCT keys(p) AS keys UNWIND keys AS keyslisting WITH DISTINCT keyslisting AS allfields RETURN collect(allfields) as proprietes";
            else 
                $requete .= "MATCH ()-[r:".$listTypes[$i]."]-() WITH DISTINCT keys(r) AS keys UNWIND keys AS keyslisting WITH DISTINCT keyslisting AS allfields RETURN collect(allfields) as proprietes";
        }

        // Récup de toutes les propriétes (plus groupé par élément)
        if($type == 'noeuds')
            $requete .= " UNION ALL MATCH (p) WITH DISTINCT keys(p) AS keys UNWIND keys AS keyslisting WITH DISTINCT keyslisting AS allfields RETURN collect(allfields) as proprietes";
        else 
            $requete .= " UNION ALL MATCH ()-[r]-() WITH DISTINCT keys(r) AS keys UNWIND keys AS keyslisting WITH DISTINCT keyslisting AS allfields RETURN collect(allfields) as proprietes";

        $requete .= ";";
        $list_proprietes = array();

        $num = 0; // Numero du type de noeud récupéré

        $result = self::$client->run($requete);
        foreach ($result->getRecords() as $record)
        {
            $tab = $record->values();

            for($i = 0; $i < count($tab); $i++)
            {
                if($num < count($listTypes))
                    $list_proprietes[$listTypes[$num]] = $tab[$i];
                else
                    $list_proprietes["Defaut"] = $tab[$i];
            }

            $num ++;
        }

        return $list_proprietes;
    }
}