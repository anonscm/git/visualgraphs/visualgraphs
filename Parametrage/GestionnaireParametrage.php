<?php

/**
 * Description of GestionnaireParametrage
 */

class GestionnaireParametrage 
{
    private $_categorie; // Liste des catégories que contient le menu
    private $_nomFichierStructure; // Nom du fichier contenant la structure
    private $_nomFichierContenu; // Nom du fichier contenant le contenu
    
    
    function __construct($nomFichierStructure, $nomFichierContenu) 
    {
        $this->_nomFichierStructure = $nomFichierStructure;
        $this->_nomFichierContenu = $nomFichierContenu;
        
        $this->recupStructure();
        
        $this->recupContenu();
        
        $this->affichage();
    }
    
    ///////////////////////////////////////////////////////////////////
    // Récupération de la structure des données dans le fichier JSON //
    ///////////////////////////////////////////////////////////////////
    private function recupStructure()
    {
        $data = array();
        
        $fichier = fopen ($this->_nomFichierStructure, "r");
        $contenu = "";
        
        // Lecture du contenu du fichier
        while (!feof($fichier))
        { 
            $contenu .= fgets($fichier, 4096);
        }

        $tabData = json_decode($contenu, true); // Décodage (JSON -> Tableau PHP)
        
        fclose($fichier);
        
        // Création des catégories
        $this->_categorie = array(); 
        foreach ($tabData as $key => $tabContenu)
        {
            if(isset($tabContenu["type"]) && $tabContenu["type"] == "liste")
                $this->_categorie[count($this->_categorie)] = new CategorieListe($tabContenu);
            else
                $this->_categorie[count($this->_categorie)] = new Categorie($tabContenu);
        }
    }
    
    //////////////////////////////////////////////////////////////
    // Récupération du contenu des données dans le fichier JSON //
    //////////////////////////////////////////////////////////////
    private function recupContenu()
    {
        $data = array();

        if(file_exists($this->_nomFichierContenu))
        {
            $fichier = fopen ($this->_nomFichierContenu, "r");
            $contenu = "";

            // Lecture du contenu du fichier
            while (!feof($fichier))
            { 
                $contenu .= fgets($fichier, 4096);
            }

            $tabData = json_decode($contenu, true); // Décodage (JSON -> Tableau PHP)

            fclose($fichier);

            // Création des catégories
            foreach ($this->_categorie as $c)
            {
               $c->recupContenu($tabData, array());
            }
        }
    }
    
    ///////////////////////
    // Affichage du menu //
    ///////////////////////
    public function affichage()
    {
        // Bouton de retour
        echo '<form action="../" id="form_retour_menu"><button href= type="button" id="bouton_retour" ><img id="image_retour" src="../VisualGraphs/img/fleche_retour.png" />Retour</button></form><br/>';
        
        // Système de chargement de données externes
        echo '<br/><form id="form_charger_fichier_param" method="POST" action="?action=upload" enctype="multipart/form-data" title="Remplace le fichier de paramétrage actuel par celui choisi (fichier .json)">';
            echo '<input type="hidden" name="MAX_FILE_SIZE" value="100000">';
            echo 'Importer un fichier de paramétrage (fichier .json) : <input type="file" name="fichier" id="input_charger_fichier_param">';
            echo ' <input type="submit" name="envoyer" value="Charger">';
        echo '</form><br/>';
        
        // Génère un lien d'export du fichier
        $nomFichier = explode('/', $this->_nomFichierContenu)[count(explode('/', $this->_nomFichierContenu)) - 1]; 
        echo '<a href="'.$this->_nomFichierContenu.'" download="'.$nomFichier.'" id="lien_export_parametre">Exporter le fichier de paramétrage</a>';
        
        // Formulaire de paramétrage
        echo '<form method="post" id="formulaire" action="?action=sauvegarde">';
            foreach ($this->_categorie as $c)
            {
                $c->affichage();
            }
            echo '<input id="bouton_valider" type="submit" value="Sauvegarder" />';
        echo '</form>';
        
        echo '<br/><br/><br/><br/>';
    }
}
