
<script type="text/javascript" src="../VisualGraphs/libsJS/jquery-3.1.1.min.js"></script>

<?php

echo '<html>';

	echo '<head>';
        echo '<meta charset="utf-8" />';

		echo '<link rel="stylesheet" type="text/css" href="../style.css">';
		echo '<link rel="icon" type="image/png" href="../VisualGraphs/img/favicon.png" />';
        echo '<title>VisualGraphs - Générateur de requêtes</title>';
    echo '</head>';
	
	// 
	require_once '../VisualGraphs/vendorPHP/autoload.php';
	use GraphAware\Neo4j\Client\ClientBuilder;
	
	include_once("../VisualGraphs/ScriptsPHP/Connexion.php");
	
	echo '<form action="../" id="form_retour_menu"><button href= type="button" id="bouton_retour" ><img id="image_retour" src="../VisualGraphs/img/fleche_retour.png" />Retour</button></form>';
	
	$labels = array();
    $types_relations = array();
	
	// Ajout des élément pour récupérer tous les noeuds et tous les liens
	$labels[0] = "Tous les types de noeuds";
	$types_relations[0] = "Tous les types de liens";
	
	echo "<h2>Générateur de requêtes</h2>";
	
	// Connexion à Neo4J et récupération des type de neouds et de leibs
	try {
		// Connexion à Neo4J
		$client = ClientBuilder::create()
				->addConnection('bolt', RecupLoginNEO4J())
				->build();
		
		$result = $client->run('MATCH (n) RETURN distinct labels(n)'); // Récupération des types de noeuds
		foreach ($result->getRecords() as $record)
		{
			$tab = $record->values();
			
			 $labels[count($labels)] = $tab[0][0];
		}
		// Récupération des types de liens
		$result = $client->run('MATCH (n1)-[r]-(n2) return distinct type(r)'); // Récupération des types de relations
		foreach ($result->getRecords() as $record)
		{
			$tab = $record->values();
			
			for($i = 0; $i < count($tab); $i++)
				$types_relations[count($types_relations)] = $tab[$i];
		}
            
	} catch (Exception $e) 
	{
		echo '<h3 style="color:red"> Echec de la connexion au serveur de Neo4J <br/><br/></h3>';

		return null;
	}
	
    echo '<p>Cette interface permet de générer des requêtes simples pour créer des boutons dans le menu de paramétrage.';
    echo "<br/>Plus d'informations dans <a href='../Documents/Notice_Utilisation_VisualGraphs.pdf'>la notice d'utilisation</a> (Partie 5).</p><br/><br/>";
    
	// Affichage des type de noeuds (list)
	echo '<select name="noeud1" id="noeud1" class="select_menu_requete" title="Type du premier noeud">';
	for($i = 0; $i < count($labels); $i++)
		echo '  <option value="'.$labels[$i].'">'.$labels[$i].'</option>';
	echo '</select>';
	
	echo ' ';
	echo '<img src="../VisualGraphs/img/lien_noeuds2.png" style="background-color:#787878; vertical-align:middle; transform: rotate(180deg);" />';
	echo '<img src="../VisualGraphs/img/lien_noeuds1.png" style="background-color:#787878; vertical-align:middle;" />';
	
	// Affichage des type de liens (list)
	echo '<select name="lien" id="lien" class="select_menu_requete" title="Type du lien entre les noeuds">';
	for($i = 0; $i < count($types_relations); $i++)
		echo '  <option value="'.$types_relations[$i].'">'.$types_relations[$i].'</option>';
	echo '</select>';
	
	echo '<img src="../VisualGraphs/img/lien_noeuds1.png" style="background-color:#787878; vertical-align:middle;" />';
	echo '<img src="../VisualGraphs/img/lien_noeuds2.png" style="background-color:#787878; vertical-align:middle;" />';
	echo ' ';
	
	// Affichage des type de noeuds (list) 
	echo '<select name="noeud2" id="noeud2" class="select_menu_requete" title="Type du second noeud">';
	for($i = 0; $i < count($labels); $i++)
		echo '  <option value="'.$labels[$i].'">'.$labels[$i].'</option>';
	echo '</select>';
	
	// Bouton de génération
	echo '<br/><br/><button id="bouton_generer_menu_requete" title="Générer la requête">Générer</button><br/><br/><br/><br/>';
	
	echo '<p id="text_input_requete"><b>Requête : </b><input id="input_requete" type="text" name="lname"></p>';
	
?>

<script>

	// Genere un requete au click du bouton, selon les valeurs choisie dans les listes
	$('#bouton_generer_menu_requete').click(function() {
		
		$requete = "MATCH ";
		
		if($("#noeud1").val() != "Tous les types de noeuds")
			$requete += "(n1:" + $("#noeud1").val() + ")";
		else
			$requete += "(n1)";
		
		$requete += "-";
		
		if($("#lien").val() != "Tous les types de liens")
			$requete += "[r:" + $("#lien").val() + "]";
		else
			$requete += "[r]";
		
		$requete += "-";
		
		if($("#noeud2").val() != "Tous les types de noeuds")
			$requete += "(n2:" + $("#noeud2").val() + ")";
		else
			$requete += "(n2)";
		
		$requete += " RETURN n1, n2, r";
		
		$('#input_requete').val($requete);
		$('#text_input_requete').show();
		
	});

</script>

</html>
