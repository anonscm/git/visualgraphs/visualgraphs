
/////////////////////////////////////////////////////////////////////////
// Classes permettant de gérer l'image qui sert d'écran d'introduction //
/////////////////////////////////////////////////////////////////////////

class EcranIntroduction
{
    constructor()
    {
        this.actif = false;
        
        // Récupérer le lien à partir du paramétrage
        this.img = null;
        
        this.taille = new Vector2(0, 0);
        this.decallage = new Vector2(0, 0);
    }
    
    //////////////////////////////////////////////
    // Initialisation de l'écran d'introduction //
    //////////////////////////////////////////////
    init(params)
    {
        this.actif = true;
        
        this.img = new Image();
        this.img.src = params.imageIntro;
    }
    
    ///////////////////////////////
    // Vérifie si l'image existe //
    ///////////////////////////////
    imageValide(params)
    {
        if(params.imageIntro == null || params.imageIntro == '')
        {
            return false;
        }
        else
        {
            // Lance un requête HTTP pour voir si l'image existe
            var http = new XMLHttpRequest();
            http.open('HEAD', params.imageIntro, false);
            http.send();

            return http.status != 404;
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////
    // Adaptation de la taille de l'écran lors du changement de la taille de la fenêtre //
    //////////////////////////////////////////////////////////////////////////////////////
    modifTailleEcran(tailleFenetre)
    {
        if(tailleFenetre.x * 0.75 < tailleFenetre.y)
        {
            this.taille.x = tailleFenetre.x;
            this.taille.y = tailleFenetre.x * 0.75;
            this.decallage.x = 0;
            this.decallage.y = tailleFenetre.y/2 - this.taille.y/2;
        }
        else
        {
            this.taille.y = tailleFenetre.y;
            this.taille.x = tailleFenetre.y * 1.33;
            this.decallage.x = tailleFenetre.x/2 - this.taille.x/2;
            this.decallage.y = 0;
        }
    }
    
    ///////////////////////////////////////
    // Gestion de l'affichage de l'écran //
    ///////////////////////////////////////
    afficher(ctx)
    {
        if(this.img != null)
        {
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            ctx.drawImage(this.img, this.decallage.x, this.decallage.y, this.taille.x, this.taille.y);
        }
    }
}