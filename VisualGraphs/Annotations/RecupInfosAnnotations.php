<?php
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // Script de récupération des informations de neo4j liées aux annotations passées en paramètres //
    //////////////////////////////////////////////////////////////////////////////////////////////////

    require_once '../vendorPHP/autoload.php';
    
    use GraphAware\Neo4j\Client\ClientBuilder;
    
    include_once("../ScriptsPHP/Connexion.php");
    
    // Récupération du contenu
    $localStorage = json_decode($_GET['localStorage']);
    
    $noeuds = array();
    $liens = array();

    // Connexion à Neo4J
    $client = ClientBuilder::create()
            ->addConnection('bolt', RecupLoginNEO4J())
            ->build();
    
    // Lance une requete de recupération pour chaque noeud et lien ( à partir des données du LocalStorage passé sous forme de GET à la page)
    while (list($key, $value) = each($localStorage)) 
    {
        if($value != '' && $value != 'undefined')
        {
            $id = 0;
            $type = "";
            $requete = "";
            
            if( strstr($key, "Noeud"))
                $type = "Noeud";
            else
                $type = "Lien";
            
            if($type == "Noeud")
            {
                $id = substr($key, 6);
                $requete = "MATCH (n) WHERE id(n)=" . $id . " RETURN n";
            }
            else if($type == "Lien")
            {
                $id = substr($key, 5);
                $requete = "MATCH (n1)-[r]-(n2) WHERE id(r)=" . $id . " RETURN r";
            }
            
            $result = $client->run($requete);
            foreach ($result->getRecords() as $record)
            {
                $element = null;
                if($type == "Noeud")
                    $element = $record->get("n");
                else if($type == "Lien")
                    $element = $record->get("r");
                
                if($element != null)
                {
                    if($type == "Noeud")
                    {
                        $noeuds[$element->identity()] = array();
                        $noeuds[$element->identity()]["ID"] = $element->identity();
                        $noeuds[$element->identity()]["Type"] = $element->labels()[0];
                        $noeuds[$element->identity()]["Values"] = $element->values();
                        $noeuds[$element->identity()]["Annotation"] = $value;
                    }
                    else if($type == "Lien")
                    {
                        $liens[$element->identity()] = array();
                        $liens[$element->identity()]["ID"] = $element->identity();
                        $liens[$element->identity()]["Type"] = $element->type();
                        $liens[$element->identity()]["Values"] = $element->values();
                        $liens[$element->identity()]["Annotation"] = $value;
                    }
                }
            }
        }
    }
    
    
    $r = array();
    $r["Noeuds"] = $noeuds;
    $r["Liens"] = $liens;
    
    echo json_encode($r);