
<?php
    //////////////////////////////////////////////////////////////////////////////////////////////
    // Script permettant de récupérer la liste des type de noeuds(labels) et des types de liens //
    //////////////////////////////////////////////////////////////////////////////////////////////

    require_once '../vendorPHP/autoload.php';

    use GraphAware\Neo4j\Client\ClientBuilder;

    include_once("Connexion.php");

    // Connexion à Neo4J
    $client = ClientBuilder::create()
            ->addConnection('bolt', RecupLoginNEO4J())
            ->build();  
        
    $query1 = 'MATCH (n) RETURN distinct labels(n)'; // Récupération des types de relations
    $query2 = 'MATCH ()-[r]-() return distinct type(r)'; // Récupération des types de relations

    $tableau = array();
    $tableau['labels'] = array();
    $tableau['types_relation'] = array();
   
     // Récupération des Labels des noeuds
    $result1 = $client->run($query1);
    foreach ($result1->getRecords() as $record)
    {
        $tab = $record->values();
	$tableau['labels'][count($tableau['labels'])] = $tab[0][0];
    }
    
    // Récupération des types de relations
    $result2 = $client->run($query2);
    foreach ($result2->getRecords() as $record)
    {
        $tab = $record->values();
        
        for($i = 0; $i < count($tab); $i++)
            $tableau['types_relation'][count($tableau['types_relation'])] = $tab[$i];
    }
    
    echo json_encode($tableau);
?>




