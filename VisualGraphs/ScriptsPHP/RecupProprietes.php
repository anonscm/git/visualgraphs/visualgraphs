
<?php
    ////////////////////////////////////////////////////////////////////////////////////////////
    // Script permettant de récupérer la liste des propriété pour un type de noeud ou de lien //
    ////////////////////////////////////////////////////////////////////////////////////////////

    require_once '../vendorPHP/autoload.php';

    use GraphAware\Neo4j\Client\ClientBuilder;

    include_once("Connexion.php");

    // Connexion à Neo4J
    $client = ClientBuilder::create()
            ->addConnection('bolt', RecupLoginNEO4J())
            ->build();
		
    if(isset($_POST["element"]))
    {
        $tabElement = explode(" ", $_POST["element"]);
        
        if($tabElement[0] == "noeud") // Récupération de toutes les proprétés des noeuds
        {
            if(count($tabElement) >= 2)
                $requete = 'MATCH (p:'.$tabElement[1].') WITH DISTINCT keys(p) AS keys UNWIND keys AS keyslisting WITH DISTINCT keyslisting AS allfields RETURN allfields;';
            else
                $requete = 'MATCH (p) WITH DISTINCT keys(p) AS keys UNWIND keys AS keyslisting WITH DISTINCT keyslisting AS allfields RETURN allfields;';
        }
        else if($tabElement[0] == "lien") // Récupération de toutes les proprétés des liens
        {
            if(count($tabElement) >= 2)
                $requete = 'MATCH ()-[r:'.$tabElement[1].']-() WITH DISTINCT keys(r) AS keys UNWIND keys AS keyslisting WITH DISTINCT keyslisting AS allfields RETURN allfields;';
            else
                $requete = 'MATCH ()-[r]-() WITH DISTINCT keys(r) AS keys UNWIND keys AS keyslisting WITH DISTINCT keyslisting AS allfields RETURN allfields;';
        }
        
        $list_proprietes = array();

        $result = $client->run($requete);
        foreach ($result->getRecords() as $record)
        {
            $tab = $record->values();

            for($i = 0; $i < count($tab); $i++)
                $list_proprietes[] = $tab[$i];
        }

        echo json_encode($list_proprietes);
    }
?>


