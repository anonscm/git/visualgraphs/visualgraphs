
<?php
    /////////////////////////////////////////////////////////
    // Script permettant de récupérer des données de Noe4J //
    /////////////////////////////////////////////////////////

    require_once '../vendorPHP/autoload.php';
    
    use GraphAware\Neo4j\Client\ClientBuilder;
    
    include_once("Connexion.php");
    
    set_time_limit(0); // Modification du temps d'éxécution maximal
    ini_set('memory_limit', '-1');
    
    // Connexion à Neo4J
    $client = ClientBuilder::create()
        ->addConnection('bolt', RecupLoginNEO4J())
        ->build();
    
    $l_requetes = array();
    $noeuds = array();
    $lien = array();
    $r = array();
    
     // Si la requête contient des termes qui risque d'altérer la base de donnée, on quitte le script
    if(stristr($_POST["requete"], 'CREATE ') === FALSE && stristr($_POST["requete"], 'MERGE ') === FALSE && stristr($_POST["requete"], 'SET ') === FALSE && stristr($_POST["requete"], 'DELETE ') === FALSE && stristr($_POST["requete"], 'REMOVE ') === FALSE && stristr($_POST["requete"], 'create ') === FALSE && stristr($_POST["requete"], 'merge ') === FALSE && stristr($_POST["requete"], 'set ') === FALSE && stristr($_POST["requete"], 'delete ') === FALSE && stristr($_POST["requete"], 'remove ') === FALSE) 
    {
        // Récupération et découpage de la requête
        if(isset($_POST["requete"]) && $_POST["requete"] != '')
        {
            $l_requetes = explode(";", str_replace("'", "", $_POST["requete"]));
        }
        
        // Récupération du contenu de chaque partie de la requête
        for($i = 0; $i < count($l_requetes); $i++)
        {
            if($l_requetes[$i] != "" && $l_requetes[$i] != "\r\n")
            {
                $l_requetes[$i] = str_replace("[-]", "'", $l_requetes[$i]);
                $l_requetes[$i] = utf8_decode($l_requetes[$i]);
                
                try
                {
                    $result = $client->run($l_requetes[$i]);
                } 
                catch (Exception $ex) 
                {
                    //echo json_encode($ex);
                    
                    echo $ex;
                    
                    exit;
                }

                // Création des tableau correpondant aux éléments
                $element = recuperationElementRequete($l_requetes[$i]);

                // Récupération des données
                foreach ($result->getRecords() as $record)
                {
                    for($j = 0; $j < count($element['noeuds']); $j++)
                    {
                        $noeuds = RecupNoeud($noeuds, $record, $element['noeuds'][$j]);
                    }

                    for($j = 0; $j < count($element['liens']); $j++)
                    {
                        $lien = RecupLien($lien, $record, $element['liens'][$j]);
                    }
                }
            }
        }

        $r["Nodes"] = $noeuds;
        $r["Links"] = $lien;

        // Renvoit les tableaux récupérés en format JSON
        echo json_encode($r);
    }
    else
    {
        $r['Erreur'] = "Requête non valide";
        
        echo json_encode($r);
    }
    
    ///////////////////////////////////////////////
    // Permet de récupérer le contenu d'un noeud //
    ///////////////////////////////////////////////
    function RecupNoeud($noeuds, $record, $nomN)
    {
        $a = $record->get($nomN);

        if($a != null)
        {
            $noeuds[$a->identity()] = array();
            $noeuds[$a->identity()]["ID"] = $a->identity();
            $noeuds[$a->identity()]["Labels"] = $a->labels();
            $noeuds[$a->identity()]["Values"] = $a->values();
        }

        return $noeuds;
    }

    //////////////////////////////////////////////
    // Permet de récupérer le contenu d'un lien //
    //////////////////////////////////////////////
    function RecupLien($lien, $record, $nomL)
    {
        $a = $record->get($nomL);

        if($a != null)
        {
            $lien[$a->identity()] = array();
            $lien[$a->identity()]["ID"] = $a->identity();
            $lien[$a->identity()]["Values"] = $a->values();
            $lien[$a->identity()]["Type"] = $a->type();
            $lien[$a->identity()]["StartNode"] = $a->startNodeIdentity();
            $lien[$a->identity()]["EndNode"] = $a->endNodeIdentity();
        }

        return $lien;
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Permet de découpé une chaine de caractrère à partir de plusieurs delimiters //
    /////////////////////////////////////////////////////////////////////////////////
    function multiexplode ($delimiters, $string) 
    {
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }

    ////////////////////////////////////////////////////////////////////////
    // Fonction permettant de récupérer les élements composant la requête // 
    ////////////////////////////////////////////////////////////////////////
    function recuperationElementRequete($requete)
    {
        $element = array();
        $element['noeuds'] = array();
        $element['liens'] = array();

        $l_noeuds = array();
        $l_liens = array();

        $elementActuel = "";
        $debutElementActuel = 0;

        // Récupération des éléments ( Contenu dans le MATCH )

        $tabRequetesMatch = explode("MATCH", $requete);
        $requeteMatch = "";
        for($i = 0; $i < count($tabRequetesMatch); $i++)
        {
            $requeteMatch .= explode("WHERE", $tabRequetesMatch[$i])[0];
        }

        for($i = 0; $i < strlen($requeteMatch) - 1; $i++)
        {
            $caract = substr($requeteMatch, $i, 1);

            if($caract == '(')
            {
                $elementActuel = "noeud";
                $debutElementActuel = $i + 1;
            }
            else if($caract == '[')
            {
                $elementActuel = "lien";
                $debutElementActuel = $i + 1;
            }

            if($caract == ':' || $caract == ')' || $caract == ']' || $caract == '*')
            {
                if(strlen(substr($requeteMatch, $debutElementActuel, $i - $debutElementActuel)) > 0)
                {
                    if($elementActuel == "noeud")
                        $l_noeuds[count($l_noeuds)] = substr($requeteMatch, $debutElementActuel, $i - $debutElementActuel);

                    else if($elementActuel == "lien")
                        $l_liens[count($l_liens)] = substr($requeteMatch, $debutElementActuel, $i - $debutElementActuel);
                }

                $elementActuel = "";
            }
        }

        // Vérifie si les éléments sont renvoyé dans le return (sinon on les recupérent pas) + evite les doublons
		
        $requeteReturn = explode("RETURN", $requete)[1];
        $analyse_return = multiexplode(array(" ", ",", "\r\n"),$requeteReturn);
        $doublon = false;
        for($i = 0; $i < count($analyse_return); $i++)
        {
            for($j = 0; $j < count($l_noeuds); $j++)
            {
                $doublon = false;
                for($k = 0; $k < count($element['noeuds']); $k++)
                    if($element['noeuds'][$k] == $l_noeuds[$j])
                        $doublon = true;

                if($analyse_return[$i] == $l_noeuds[$j] && !$doublon)
                    $element['noeuds'][count($element['noeuds'])] = $l_noeuds[$j];
            }
            for($j = 0; $j < count($l_liens); $j++)
            {
                $doublon = false;
                for($k = 0; $k < count($element['liens']); $k++)
                    if($element['liens'][$k] === $l_liens[$j])
                        $doublon = true;

                if($analyse_return[$i] === $l_liens[$j] && !$doublon)
                    $element['liens'][count($element['liens'])] = $l_liens[$j];
            }
        }

        return $element;
    }

?>




