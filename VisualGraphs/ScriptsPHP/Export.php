
<?php

    /////////////////////////////////////////////////////////
    // Export des données d'un noeuds dans une fichier txt //
    /////////////////////////////////////////////////////////

    $texte = "";

    // Récupération de l'élément à récupérer
    if(isset($_POST["label"]) && $_POST["label"] != '')
    {
        // Récupération du labels et du titre
        $texte .= '' . $_POST["label"] . ' : ' . $_POST["titre"].PHP_EOL;

        if(isset($_POST["propriete"]) && $_POST["propriete"] != '')
        {
            // Récupération des propriétés
            $texte .= PHP_EOL . "Propriétés : " . PHP_EOL;
            $tab_proprietes = explode(";", $_POST["propriete"]);
            for($i = 0; $i < count($tab_proprietes) - 1; $i+=2)
            {
                $texte .= $tab_proprietes[$i] . " : " . $tab_proprietes[$i+1] . PHP_EOL;
            }
        }

        if(isset($_POST["liens"]) && $_POST["liens"] != '')
        {
            // Récupération des liens
            $tab_liens = explode(";", $_POST["liens"]);
            
            $nbLiens = (count($tab_liens)-1)/4;
            $texte .= PHP_EOL . "Liens (" . $nbLiens . ") : " . PHP_EOL;
            
            for($i = 0; $i < count($tab_liens) - 1; $i+=4)
            {
                $type_lien = $tab_liens[$i];
                $nom_labels = $tab_liens[$i+1];
                $texte_noeud = $tab_liens[$i+2];
                $sens = $tab_liens[$i+3];

                if($sens == "sens1")
                    $texte .= "--[" . $type_lien . "]--> ";
                else
                    $texte .= "<--[" . $type_lien . "]-- ";

                $texte .= $texte_noeud . " (" . $nom_labels . ")" . PHP_EOL;
            }
        }
        
        // Récupération des annotation si elles existent
        if(isset($_POST["annotation"]) && $_POST["annotation"] != '')
        {
            $texte .= PHP_EOL . "Annotation : ". $_POST["annotation"] . PHP_EOL;
        }
    }

    // Ouverture du fichier
    $monfichier = fopen('../exports/export.txt', 'a+');
    
    // Vide le fichier
    ftruncate($monfichier, 0);

    // Ecriture des données dans le fichier
    fputs($monfichier, utf8_decode($texte));

    // Fermeture du fichier
    fclose($monfichier);

?>