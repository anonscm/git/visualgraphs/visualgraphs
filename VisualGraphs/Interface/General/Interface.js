
///////////////////////////////////////////////////////////////////////////
// Classe d'interface dont toute les interfaces du menu de droite hérite //
///////////////////////////////////////////////////////////////////////////
class Interface
{
    constructor(nom_classe, nom_affichage)
    {
        this.cacher = false;
        
        this.nom_classe = nom_classe;
        this.nom_div = "interface_" + nom_classe;
        this.nom_div_contenu = "contenu_interf_" + nom_classe;
        this.nom_div_titre = "titre_interface_" + nom_classe;
        
        $("#" + this.nom_div).html("<div id='"+this.nom_div_titre +"' class='titre_cadre_interface'></div><div class='contenu_cadre_interface' id='"+this.nom_div_contenu+"'></div>");
        $("#" + this.nom_div_titre).html("<h3>" + nom_affichage + "</h3>");
    }
    
    ///////////////////////////////////////////////
    // Modifie l'etat d'affichage de l'interface //
    ///////////////////////////////////////////////
    modifEtatAffichage(cacher)
    {
        if(this.cacher != cacher)
        {
            this.cacher = cacher;
            
            if(this.cacher == true)
                $("#interface_" + this.nom_classe).css("display", "none");
            else
                $("#interface_" + this.nom_classe).css("display", "block");
        }
    }
    
    //////////////////////////////////////////////////////////
    // Initialisation de la couleur du titre de l'interface //
    //////////////////////////////////////////////////////////
    initCouleurTitre(couleurTitre)
    {
        $("#titre_interface_" + this.nom_classe).css("background-color", couleurTitre);
    }
    
}