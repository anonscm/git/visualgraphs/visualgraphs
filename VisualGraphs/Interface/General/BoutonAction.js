
/////////////////////////////////////////////////////////
// Bouton permettant de lancer une requete (interface) //
/////////////////////////////////////////////////////////
class BoutonAction
{
    constructor(params, nom, element)
    {
        this.nom = nom;
        this.nomComplet = nom;
        
        this.params = params;
        
        this.element = element;
        
        if(this.element != null)
            this.nomComplet = nom + element.id;
    }
    
    /////////////////////////
    // Affichage du bouton //
    /////////////////////////
    afficher()
    {
        var valeur = "";
        var contenu = "";
        
        if(this.params['Image'] != null)
            contenu = "<img src='"+this.params['Image']+"' alt='"+this.nom+"' height='20' width='20'>";
		else if(this.params['Nom'] != null)
            contenu = this.params['Nom'];
		else
            contenu = this.nom;
	
        valeur += '<button class="bouton_requete" name="'+this.nom+'" id="bRequete_'+this.nomComplet+'">'+contenu+'</button>';
        
        return valeur;
    }
    
    ////////////////////////////////////
    // Gestion du click sur le bouton //
    ////////////////////////////////////
    gestionEvenement()
    {
        var nom = this.nom;
        var params = this.params;
        var element = this.element;
        
        $("#bRequete_" + this.nomComplet).click(function()
        {
            var requete = visualGraphs.params.recupRequete(params["Requete"]);

            var tabRequete = requete.split('|this|');

            if(tabRequete.length > 1)
            {
                // Gestion des requêtes liés à un élément (noeuds ou lien)
                requete = "";
                for(var i = 0; i < tabRequete.length - 1; i++)
                {
                    // Vérifie si demande identifiant neo4j ou propriété 'id' dans la requête
                    if(tabRequete[i].indexOf("id(") >= 0)
                        var texte = element.id;
                    else
                        var texte = element.values['id'];
					
                    requete += tabRequete[i] + texte;
                }
                requete += tabRequete[tabRequete.length - 1];
            }
            else
            {
                tabRequete = requete.split('|#');

                if(tabRequete.length === 1)
                    requete = tabRequete[0];
                else
                {
                    // Gestion des requetes avec champs rempli par les utilisateurs
                    requete = "";
                    var param_indication = params["Param_Indication"].split(';');
                    
                    //Récupération des différents indices et demande à l'utilisateur d'entré les extes correspondanr
                    var indices = new Array();
                    var textesUtilisateur = new Array();
                    for(var i = 1; i < tabRequete.length - 1; i+= 2)
                    {
                        var nouveau = true;
                        
                        for(var j = 0; j < indices.length; j++)
                        {
                            if(parseInt(tabRequete[i]) == indices[j])
                                nouveau = false;
                        }
                        
                        if(nouveau)
                        {
                            indices.push(parseInt(tabRequete[i]));
                            textesUtilisateur.push(prompt('' + param_indication[indices[indices.length - 1]], ''));
                        }
                    }
                    
                    // Création de la requête à partir des information rentrées par l'utilisateur
                    for(var i = 0; i < tabRequete.length - 1; i += 2)
                    {
                        var texte = textesUtilisateur[parseInt(tabRequete[i+1])];

                        if(texte != null)
                        {
                            texte = texte.replace("'", "[-]"); // Gestion du remplacement de [-] par ' 

                            requete += tabRequete[i] + texte; // Ajout du texte entrée par l'utilisateur à la requete
                        }
                        else
                            return; // Quitte la fonction si l'utilisation à cliqué sur 'Annuler'
                    }
                   
                    requete += tabRequete[tabRequete.length - 1];
                }
            }

            // Modification du texte de description (en bas à gauche de l'écran)
            var description = params["Description"];
            if(description)
            {
                var tabDescription = description.split('|this|');
                if(tabDescription.length === 1)
                    description = tabDescription[0];
                else
                {
                    description = "";
                    for(var i = 0; i < tabDescription.length - 1; i++)
                        description += tabDescription[i] + "'" + element.texte + "'";

                    description += tabDescription[tabDescription.length - 1];
                }
            }

            // Lancement de la requête (rechargement des données)
            if(element != null) // Si bouton liée à un element 
                visualGraphs.chargerDonnees(requete, description, element);
            else
                visualGraphs.chargerDonnees(requete, description);
        });
    }
    
}