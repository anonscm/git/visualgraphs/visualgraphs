
/////////////////////////////////////////////////////////////////////////
// Classe permettant d'afficher et de gérer les onglets de l'interface //
/////////////////////////////////////////////////////////////////////////
class OngletInterface
{
    constructor()
    {
        this.onglets = new Array();
        this.onglets.push("Visualisation");
        this.onglets.push("Recherche");
        this.onglets.push("Informations");
        this.onglets.push("Aide");
        this.onglets.push("Retour_au_menu");
        
        var contenu = "";
        contenu += this.ajoutOnglet("Visualisation", "Visualisation", "#FE8282");
        contenu += this.ajoutOnglet("Recherche", "Recherche", "#82C8FE");
        contenu += this.ajoutOnglet("Informations", "Informations", "#6FE17A");
        contenu += this.ajoutOnglet("Aide", "Aide", "#ffcc69");
        
        // Ajout d'un onglet pour retourner sur le menu si on est dans une utilisation locale du logiciel
        if(document.location.href.indexOf("localhost") >= 0)
            contenu += this.ajoutOnglet("Retour au menu", "Retour_au_menu", "#abe1cf");
        
        $("#onglets_interface").html(contenu);
        
        this.gestionEvenements();
    }
    
    ///////////////////////////////////////////
    // Dessin d'un onglet sur l'interterface //
    ///////////////////////////////////////////
    ajoutOnglet(nom, id, couleur)
    {
        var txt = "";

        txt += "<div class='onglet' id='onglet_"+id+"'  >";
        txt += nom;
        txt += "</div>";
        
        return txt;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Gestion des clics sur les onglets, changement de l'interface selectionné et changement des couleurs de l'interface //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    gestionEvenements()
    {
        for (var id in this.onglets)
        {
            $("#onglet_" + this.onglets[id]).click(function() 
            {
                if($(this).attr('id').indexOf("Aide") > 0)
                {
                    // Ouverture du document PDF d'aide aux utilisateurs
                    window.open("Presentation_Interface_VisualGraphs.pdf");
                }
                else if($(this).attr('id').indexOf("Retour_au_menu") > 0)
                {
                    // Retour au menu
                    document.location.href = "../";
                }
                else
                {
                    if($(this).attr('id').indexOf("Visualisation") > 0)
                        visualGraphs.interf.afficherInterface("Visualisation");
                    else if($(this).attr('id').indexOf("Recherche") > 0)
                        visualGraphs.interf.afficherInterface("Recherche");
                    else if($(this).attr('id').indexOf("Informations") > 0)
                        visualGraphs.interf.afficherInterface("Informations");
                }
            });
        }
    }
    
}