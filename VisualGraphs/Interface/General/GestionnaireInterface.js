
/////////////////////////////////////////////
// Classes permettant de gérer l'interface //
/////////////////////////////////////////////
class GestionnaireInterface
{
    constructor(params)
    {
        // Création des interfaces
        this.visualisation = new InterfVisu();
	this.recherche = new InterfRecherche(params);
	this.infos = new InterfInfos();
        
        this.onglets = new OngletInterface();
        
        // Initialisation de l'état d'affichage (Affichage du menu de visualisation)
        this.visualisation.modifEtatAffichage(false);
        this.recherche.modifEtatAffichage(true);
        this.infos.modifEtatAffichage(true);
        
        // Initialisation de la couleur des titre de l'interface
        this.visualisation.initCouleurTitre("#FE8282");
        this.recherche.initCouleurTitre( "#82C8FE");
        this.infos.initCouleurTitre("#6FE17A");
        
        // Initialisation des couleur de base au chargement de l'interface (Interface de Visualisation)
        $("#onglets_interface").css("border-right-color", "#FECDCD");
        $("#onglets_interface").css("background-color", "#FECDCD");
        $("#interface").css("background-color", "#FECDCD");
        
	this.majAffichage();
    }
    
    ////////////////////////////////////////////////////////////
    // Mise à jour des informations affichées par l'interface //
    ////////////////////////////////////////////////////////////
    majAffichage()
    {
	this.recherche.majAffichage();
    }
    
    ///////////////////////////////////////////////
    // Mise à jours de l'interface d'information //
    ///////////////////////////////////////////////
    selectionElement(element)
    {
        if(element != null)
        {
            if(element.constructor.name === "Noeud")
                this.infos = new InterfInfosNoeud(element);
            else if(element.constructor.name === "Lien" || element.constructor.name === "LienMemeNoeud")
                this.infos = new InterfInfosLien(element);
        }
        else
            this.infos = new InterfInfos();
        
        this.infos.initCouleurTitre("#6FE17A");
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////
    // Affichage de l'interface choisi en paramétre et modification du design de l'interface //
    ///////////////////////////////////////////////////////////////////////////////////////////
    afficherInterface(nomInterface)
    {
        this.visualisation.modifEtatAffichage(true);
        this.recherche.modifEtatAffichage(true);
        this.infos.modifEtatAffichage(true);

        if(nomInterface == "Visualisation")
        {
            this.visualisation.modifEtatAffichage(false);

            $("#onglets_interface").css("border-right-color", "#FECDCD");
            $("#onglets_interface").css("background-color", "#FECDCD");
            $("#interface").css("background-color", "#FECDCD");
        }
        else if(nomInterface == "Recherche")
        {
            this.recherche.modifEtatAffichage(false);

            $("#onglets_interface").css("border-right-color", "#d8eeff");
            $("#onglets_interface").css("background-color", "#d8eeff");
            $("#interface").css("background-color", "#d8eeff");
            $("#interface_actions").css("border-right-color", "#d8eeff");
        }
        else if(nomInterface == "Informations")
        {
            this.infos.modifEtatAffichage(false);

            $("#onglets_interface").css("border-right-color", "#c8ebcb");
            $("#onglets_interface").css("background-color", "#c8ebcb");
            $("#interface").css("background-color", "#c8ebcb");
            $("#interface_infos").css("border-right-color", "#c8ebcb");
        }
    }
}