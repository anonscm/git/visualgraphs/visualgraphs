
/////////////////////////////////////////////////////////////////////////
// Interface permettant de gérer les options de visualisation du graph //
/////////////////////////////////////////////////////////////////////////
class InterfVisu extends Interface
{
    constructor()
    {
        super("visualisation", "Visualisation");
        
        this.typeNoeuds = new Array(); // Contient les type de noeuds
        this.typeLiens = new Array(); // Contient les type de liens
    }
    
    //////////////////////////////////////////////////////////////////////
    // Réinitialisation des valeurs de récupération des type de données //
    //////////////////////////////////////////////////////////////////////
    initValeur()
    {
        for (var id in this.typeNoeuds)
            this.typeNoeuds[id].init();
        for (var id in this.typeLiens)
            this.typeLiens[id].init(); 
    }
    
    //////////////////////////////////////
    // Récupération des types de noeuds //
    //////////////////////////////////////
    recupTypeNoeuds(noeuds)
    {
        for(var i = 0; i < noeuds.length; i++)
        {
            for(var j = 0; j < noeuds[i].labels.length; j++)
            {
                if(this.typeNoeuds[noeuds[i].labels[j]] == null)
                    this.typeNoeuds[noeuds[i].labels[j]] = new ElementVisu("Noeud", noeuds[i].labels[j]);
                
                this.typeNoeuds[noeuds[i].labels[j]].nb ++;
            }
        }
    }
    
    /////////////////////////////////////
    // Récupération des types de liens //
    /////////////////////////////////////
    recupTypeLiens(liens)
    {
        for(var i = 0; i < liens.length; i++)
        {
            if(this.typeLiens[liens[i].type] == null)
                this.typeLiens[liens[i].type] = new ElementVisu("Lien", liens[i].type);
            
            this.typeLiens[liens[i].type].nb ++;
        }
    }
    
    /////////////////////////////////////////////////////////////
    // Mise à jours des informations affichées par l'interface //
    /////////////////////////////////////////////////////////////
    majInfos(noeuds, liens)
    {
        this.initValeur();
   	
        this.recupTypeNoeuds(noeuds);
        this.recupTypeLiens(liens);
        
        var valeur = '';
        
        // Icone de capture d'ecran + Textes
        valeur += "<a href='#' download='capture_graph.png' id='capture_ecran'><p><img id='image_capture' src='img/camera.png' alt='Capture' title='Faire une capture du graph' /> Faire une capture d'écran</p></a>";
   	
        valeur += '<p class="labelInterfVisualisation"><b>Noeuds</b> <small>(' + noeuds.length + ')</small></p>';
   	
        // Gestion Type de Noeuds
        for (var id in this.typeNoeuds)
        {
            if(this.typeNoeuds[id].nb > 0)
            {
                valeur += this.typeNoeuds[id].afficher();
            }
        }
        
        valeur += "<br/><br/>";
        valeur += '<p class="labelInterfVisualisation"><b>Liens</b> <small>(' + liens.length + ')</small></p>'
        
        // Gestion type de liens
        for (var id in this.typeLiens)
        {
            if(this.typeLiens[id].nb > 0)
            {
                valeur += this.typeLiens[id].afficher();
            }
        }
        
        valeur += "<br/>";

   	$("#" + this.nom_div_contenu).html(valeur);
        
        this.gestionEvenements();
    }
    
    ////////////////////////////
    // Gestion des evenements //
    ////////////////////////////
    gestionEvenements()
    {
        for (var id in this.typeLiens)
           this.typeLiens[id].gestionEvenements();
        for (var id in this.typeNoeuds)
           this.typeNoeuds[id].gestionEvenements();
       
        // Gestion de la capture d'écran
        $("#capture_ecran").click(function() 
        {
			/********** export svg **********/
            var url = "data:image/svg+xml;utf8," + encodeURIComponent(paper.project.exportSVG({asString:true}));
            var link = document.createElement("a");
            link.download = "capture_graph.svg";
            link.href = url;
            link.click();
			/********************************/
			
			/********** export bitmap **********/
            var dataURL  = canvas.toDataURL("image/png");
            $(this).attr("href", dataURL);
			/***********************************/
        });
    }
    
    ///////////////////////////////////////
    // Cacher le noeuds du labels choisi //
    ///////////////////////////////////////
    cacherNoeud(nomLabel)
    {
        this.typeNoeuds[nomLabel].cacher = true;
   	
        // Possibilitée de le deplacer
        for(var i = 0; i < visualGraphs.noeuds.length; i++)
        {
            var cacher = true;
   	
            for(var j = 0; j < visualGraphs.noeuds[i].labels.length; j++)
            {
                if(this.typeNoeuds[visualGraphs.noeuds[i].labels[j]].cacher == false)
                    cacher = false;
            }
   	
            visualGraphs.noeuds[i].cacher = cacher;
   	
            if(cacher == true)
                for(var j = 0; j < visualGraphs.noeuds[i].liens.length; j++)
                    visualGraphs.noeuds[i].liens[j].cacher = true;
   	}
   	
        this.majInfos(visualGraphs.noeuds, visualGraphs.liens);
    }
   
    ////////////////////////////////////////
    // Affiche le noeuds du labels choisi //
    ////////////////////////////////////////
    afficherNoeud(nomLabel)
    {  	
        this.typeNoeuds[nomLabel].cacher = false;
   	
        // Ajout verif en plus pour limiter nb boucles ...
        for(var i = 0; i < visualGraphs.noeuds.length; i++)
        {
            var cacher = true;
   		
            for(var j = 0; j < visualGraphs.noeuds[i].labels.length; j++)
            {
                if(!this.typeNoeuds[visualGraphs.noeuds[i].labels[j]].cacher)
                    cacher = false;
            }
   		
            visualGraphs.noeuds[i].cacher = cacher;
            
            if(!cacher)
                for(var j = 0; j < visualGraphs.noeuds[i].liens.length; j++)
                    if(!visualGraphs.noeuds[i].liens[j].n1.cacher && !visualGraphs.noeuds[i].liens[j].n2.cacher && !this.typeLiens[visualGraphs.noeuds[i].liens[j].type].cacher)
                        visualGraphs.noeuds[i].liens[j].cacher = false;
        }
   	
        this.majInfos(visualGraphs.noeuds, visualGraphs.liens);
    }
    
    ////////////////////////////////////
    // Cache les liens du type choisi //
    ////////////////////////////////////
    cacherLien(nomLabel)
    {
        this.typeLiens[nomLabel].cacher = true;
   	
        for(var i = 0; i < visualGraphs.liens.length; i++)
        {
            if(visualGraphs.liens[i].type == nomLabel)
                visualGraphs.liens[i].cacher = true;
        }
   	
        this.majInfos(visualGraphs.noeuds, visualGraphs.liens);
    }
   
    //////////////////////////////////////
    // Affiche les liens du type choisi //
    //////////////////////////////////////
    afficherLien(nomLabel)
    {  	
        this.typeLiens[nomLabel].cacher = false;
   	
        for(var i = 0; i < visualGraphs.liens.length; i++)
        {
            if(visualGraphs.liens[i].type == nomLabel && !visualGraphs.liens[i].n1.cacher && !visualGraphs.liens[i].n2.cacher)
                visualGraphs.liens[i].cacher = false;
        }
   	
        this.majInfos(visualGraphs.noeuds, visualGraphs.liens);
    }
}