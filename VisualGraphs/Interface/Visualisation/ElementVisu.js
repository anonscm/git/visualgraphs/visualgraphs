
///////////////////////////////////////////////////////////////////////////
// Gestion d'un élément de l'interface de visualisation (Noeud ou Lien)  //
///////////////////////////////////////////////////////////////////////////
class ElementVisu
{
    constructor(type, nom)
    {
        this.type = type; // Type de l'élément (Noeud ou Lien)
        
        this.nb = 0; // Indique nombre d'élements de ce type
        this.cacher = false; // Indique si les éléments de ce type sont cachés
        
        this.nom = nom; // Nom de l'élement (Type de Label ou Type de Relation)
        this.nomAffichage = this.nom.replaceAll('_', ' '); // Nom utiliser pour l'affichage (sans '_')
    }
   
    //////////////////////////////////////////////////////////////
    // Initialisation de l'élément ( rechargement des données ) //
    //////////////////////////////////////////////////////////////
    init()
    {
        this.nb = 0; 
    }
    
    ////////////////////////////
    // Affichage de l'élement //
    ////////////////////////////
    afficher()
    {
        // Récupéation de la couleur et de la classe d'affichage selon le type d'élément
        if(this.type == "Noeud")
        {
            var classe = "element_inter_visu_noeud";
            var class_icone = "icone_element_inter_visu_noeud";
            var couleur = visualGraphs.style.RecupElementNoeud(this.nom, "Couleur", "rgb(255, 255, 255)");
            var background = 'background: radial-gradient(' + ModifierCouleur(couleur, 1.1) + ', ' + ModifierCouleur(couleur, 0.95) + ');';
        }
        else
        {
            var classe = "element_inter_visu_lien";
            var class_icone = "icone_element_inter_visu_lien";
            var couleur = visualGraphs.style.RecupElementLien(this.nom, "Couleur", "rgb(255, 255, 255)");
            
            var background = ''; 
            if(visualGraphs.style.RecupElementLien(this.nom, "Diriger", 0) == 1)
                background = 'background: linear-gradient(to right, ' + ModifierCouleur(couleur, 1.07) + ', ' + ModifierCouleur(couleur, 0.93) + ');';
        }
        
        var valeur = '<div class="' + classe + '">';
        
        if(this.cacher)
            valeur += "<button id='bouton_inter_visu_" + this.type + "_" + this.nom + "' class='bouton_inter_visu_cacher'>";
        else
            valeur += "<button id='bouton_inter_visu_" + this.type + "_" + this.nom + "' class='bouton_inter_visu'>";
        
        valeur += '<div class="'+class_icone+'" style="background-color:' + couleur + ';' + background + '"></div>';
        valeur += "" + this.nomAffichage + " <small>(" + this.nb + ")</small>";

        // Gestion du bouton pour Cacher/Afficher les éléments
        if(this.cacher)
        {
            var nom_fonction = "afficher" + this.type;
            var lien_img = "img/mini_oeil_barrer.png"
        }
        else
        {
            var nom_fonction = "cacher" + this.type;
            var lien_img = "img/mini_oeil.png"
        }
		
        valeur += '</button>';
        valeur += '</div>';
       
        return valeur;
    }
    
    ////////////////////////////////////
    // Gestion du click sur le bouton //
    ////////////////////////////////////
    gestionEvenements()
    {
        var cacher = this.cacher;
        var nom = this.nom;
        var type = this.type;
        
        $("#bouton_inter_visu_" + this.type + "_" + this.nom).click(function()
        {
            if(cacher)
            {
                if(type == "Noeud")
                    visualGraphs.interf.visualisation.afficherNoeud(nom);
                else if(type == "Lien")
                    visualGraphs.interf.visualisation.afficherLien(nom);
            }
            else 
            {
                if(type == "Noeud")
                    visualGraphs.interf.visualisation.cacherNoeud(nom);
                else if(type == "Lien")
                    visualGraphs.interf.visualisation.cacherLien(nom);
            }
        });
    }

}