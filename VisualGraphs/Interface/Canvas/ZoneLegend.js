
///////////////////////////////////////////////////////////////////////////
// Zone affichant la légende de l'écran actuel (Afficher dans le canvas) //
///////////////////////////////////////////////////////////////////////////
class ZoneLegend
{
    constructor(params)
    {
        this.texte = params.descriptionRequeteBase; // Texte affiché
        
        this.tailleTexte = 20; // Taille d'affichage du texte
        
        this.pos = new Vector2(0, 0); // Position de la souris = position d'affichage
        
        this.imgCaptureSelect = false; // Indique si le curseur est sur l'image de capture d'écran
        this.cacherTitre = false; // Indique si le curseur est sur le titre
        
        this.zoneTitre = new Zone(new Vector2(0, 0), new Vector2(0, 0)); // Zone correspondant aux textes
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation de la position de la zone, remodifier à chaque changement de taille de la fenetre //
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    initPosition(camera)
    {
        this.pos = new Vector2(10, camera.taille.y - 40); 
        
        //this.zoneCapture = new Zone(this.pos, new Vector2(36, 0));
        this.zoneTitre = new Zone(new Vector2(this.pos.x, this.pos.y), new Vector2(0, 20));
    }
    
    /////////////////////////////////////////
    // Modification du texte de la requête //
    /////////////////////////////////////////
    modifTexte(texte)
    {
        this.texte = texte;
    }
    
    ////////////////////////////////////
    // Affichage du cadre et du titre //
    ////////////////////////////////////
    afficher(ctx, style)
    {
		/********** verzion paper.js **********/
		if (!this.cacherTitre) {
			var text = new PointText({
				point: [this.pos.x + 10, this.pos.y + this.tailleTexte + 2],
				content: this.texte,
				fontFamily: 'Arial',
				fontSize: this.tailleTexte,
				fillColor: '#000000'
			});
			
            // Mise à jour des variables de taille
            var tailleXTitre = text.bounds.width + 20;
            this.zoneTitre.taille = new Vector2(tailleXTitre, (this.tailleTexte) * 1.5);
			
			var rect = new Path.Rectangle({
				point: [this.pos.x, this.pos.y],
				size: [this.zoneTitre.taille.x, this.zoneTitre.taille.y],
				fillColor: style.cadre["CadreLegend"]["CouleurFond"],
				strokeColor: style.cadre["CadreLegend"]["CouleurBordure"],
				strokeWidth: style.cadre["CadreLegend"]["TailleBordure"]
			});
			text.insertAbove(rect);
		}
		/**************************************/
		
		/********** version html5 **********/
        /*ctx.beginPath();
        
        // Initialisaition des paramètres d'affichage
        ctx.font = this.tailleTexte + "px Arial";

        ctx.fillStyle = style.cadre["CadreLegend"]["CouleurFond"];
        ctx.strokeStyle = style.cadre["CadreLegend"]["CouleurBordure"];
        ctx.lineWidth = style.cadre["CadreLegend"]["TailleBordure"];
        
        // Affichage du rectangle principal
        if(!this.cacherTitre)
        {
            ctx.rect(this.pos.x, this.pos.y, tailleXTitre, this.zoneTitre.taille.y); 
            ctx.fill();
            ctx.stroke();
        }
       
        // Affichage du texte
        if(!this.cacherTitre)
        {
            ctx.fillStyle = "#000000";
            ctx.textAlign = "start";
            ctx.fillText(this.texte, this.pos.x + 10, this.pos.y + this.tailleTexte + 2);
        }*/
		/***********************************/
    }
}