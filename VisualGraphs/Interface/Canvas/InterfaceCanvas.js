
/////////////////////////////////////////////////////////////////////////////
// Interface permetant afficher des informations directement sur le canvas //
/////////////////////////////////////////////////////////////////////////////
class InterfaceCanvas
{
    constructor()
    {
        this.actif = false; // Etat d'affichage de l'interface
        
        this.texte = ""; // Texte affiché
        
        this.tailleTexte = 15; // Taille d'affichage du texte
        
        this.pos = new Vector2(0, 0); // position de la souris = position d'affichage
        
        this.timerAffichage = new Timer(3000); // Timer de temps sans mouvement avant affichage
    }

    //////////////////////////////////////////////////////////////////////////////
    // Appeller à chaque mouvement de souris, reinitialise le timer d'affichage //
    //////////////////////////////////////////////////////////////////////////////
    mouvementSouris(posSouris)
    {
        this.actif = false;
        
        this.timerAffichage.reInit();
        
        this.pos = posSouris;
    }
    
    //////////////////////////////////////////////////////////////////
    // Mise à jour du cadre, gestion du timer d'affichage des infos //
    //////////////////////////////////////////////////////////////////
    update(noeuds, camera)
    {
        this.timerAffichage.update();        
        
        if(this.timerAffichage.fin && !this.actif)
        {
            // Verif collision avec un noeuds
            for(var i = 0; i < noeuds.length; i++)
            {
                if(noeuds[i].verifCollision(this.pos.x, this.pos.y, camera))        
                {
                    this.actif = true;
            
                    this.texte = noeuds[i].texte;
                }
            }
        }
    }
    
    //////////////////////////////////////////////////
    // Affichage des informations ( texte + cadre ) //
    //////////////////////////////////////////////////
    afficherInfos(ctx)
    {
        if(this.actif && this.texte)
        {
			/********** version paper.js **********/
			var text = new PointText({
				point: [this.pos.x, this.pos.y],
				content: this.texte,
				justification: 'center',
				fontFamily: 'Arial',
				fontSize: this.tailleTexte,
				fillColor: '#000000'
			});
			var rect = new Path.Rectangle({
				point: [this.pos.x - text.bounds.width / 2 - 2, this.pos.y - (this.tailleTexte)],
				size: [text.bounds.width + 4, (this.tailleTexte) + 5],
				fillColor: '#ffffff'
			});
			text.insertAbove(rect);
			/**************************************/
			
			/********** version html5 **********/
            /*ctx.font = this.tailleTexte + "px Arial";
            
            ctx.textAlign = "center";
            
            ctx.fillStyle = "#000000";
            ctx.fillRect(this.pos.x - ctx.measureText(this.texte).width / 2 - 2, this.pos.y - (this.tailleTexte), ctx.measureText(this.texte).width + 4, (this.tailleTexte) + 5);
            
            ctx.fillStyle = "#ffffff";
            ctx.fillText(this.texte, this.pos.x, this.pos.y);*/
			/***********************************/
        }
    }
}
