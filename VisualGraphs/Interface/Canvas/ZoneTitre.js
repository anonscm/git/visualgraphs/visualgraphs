
////////////////////////////////////////////////////////////////////////////////////////////////
// Zone affichant le titre de la base de donnée en cours de lecture (Afficher dans le canvas) //
////////////////////////////////////////////////////////////////////////////////////////////////
class ZoneTitre
{
    constructor(params)
    {
        this.texte = params.titre; // Texte affiché
        
        this.tailleTexte = 23; // Taille d'affichage du texte
        
        this.pos = new Vector2(0, 0); // Position de la souris = position d'affichage
        
        this.cacherTitre = false; // Indique si le curseur est sur le titre
        
        this.zoneTitre = new Zone(new Vector2(0, 0), new Vector2(0, 0)); // Zone correspondant aux textes
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation de la position de la zone, remodifier à chaque changement de taille de la fenetre //
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    initPosition(camera)
    {
        this.pos = new Vector2(10, 10); 
        this.zoneTitre = new Zone(new Vector2(this.pos.x, this.pos.y), new Vector2(0, 20));
    }
    
    ////////////////////////////////////
    // Affichage du cadre et du titre //
    ////////////////////////////////////
    afficher(ctx, style)
    {
		/********** verzion paper.js **********/
		if (!this.cacherTitre) {
			var text = new PointText({
				point: [this.pos.x + 10, this.pos.y + this.tailleTexte + 2],
				content: this.texte,
				fontFamily: 'Arial',
				fontSize: this.tailleTexte,
				fillColor: '#000000'
			});
			
			// Mise à jour des variables de taille
            var tailleXTitre = text.bounds.width + 20;
            this.zoneTitre.taille = new Vector2(tailleXTitre, (this.tailleTexte) * 1.5);
		
			var rect = new Path.Rectangle({
				point: [this.pos.x, this.pos.y],
				size: [this.zoneTitre.taille.x, this.zoneTitre.taille.y],
				fillColor: style.cadre["CadreTitre"]["CouleurFond"],
				strokeColor: style.cadre["CadreTitre"]["CouleurBordure"],
				strokeWidth: style.cadre["CadreTitre"]["TailleBordure"]
			});
			text.insertAbove(rect);
		}
		/**************************************/
		
		/********** version html5 **********/
        /*ctx.beginPath();
        
        // Initialisaition des paramètres d'affichage
        ctx.font = this.tailleTexte + "px Arial";

        ctx.fillStyle = style.cadre["CadreTitre"]["CouleurFond"];
        ctx.strokeStyle = style.cadre["CadreTitre"]["CouleurBordure"];
        ctx.lineWidth = style.cadre["CadreTitre"]["TailleBordure"];
        
        // Affichage du rectangle principal
        if(!this.cacherTitre)
        {
            ctx.rect(this.pos.x, this.pos.y, tailleXTitre, this.zoneTitre.taille.y); 
            ctx.fill();
            ctx.stroke();
        }
        
        // Affichage du texte
        if(!this.cacherTitre)
        {
            ctx.fillStyle = "#000000";
            ctx.textAlign = "start";
            ctx.fillText(this.texte, this.pos.x + 10, this.pos.y + this.tailleTexte + 2);
        }*/
		/***********************************/
    }
}