
//////////////////////////////////////////////////////////////////////////////////
// Interface permettant de faire des actions (boutons de requetes + recherches) //
//////////////////////////////////////////////////////////////////////////////////
class InterfRecherche extends Interface
{
    constructor(params)
    {
        super("actions", "Recherche");
        
        this.boutonsRequetes = new Array(); // Tableau contenant les boutons de requêtes
        for (var id in params.boutonsRequetes)
        {
            this.boutonsRequetes[id] =  new BoutonAction(params.boutonsRequetes[id], id);
        }
        
        this.cadreRecherche = new CadreRechercheAvance(); // Cadre contenant le système de recherche avancée
    }

    /////////////////////////////////////////////////////////////
    // Mise à jours des informations affichées par l'interface //
    /////////////////////////////////////////////////////////////
    majAffichage()
    {
       var valeur = this.creeBoutonsRequetes();
       valeur += this.cadreRecherche.afficher();
       
       $("#" + this.nom_div_contenu).html(valeur);
       
       this.gestionEvenementsBoutons();
       this.cadreRecherche.gestionEvenements();
    }
   
    /////////////////////////////////////////////////
    // Ajout des boutons des création des requetes //	
    /////////////////////////////////////////////////
    creeBoutonsRequetes()
    {
        var valeur = "";		
	
        for (var id in this.boutonsRequetes)
        {
            valeur += this.boutonsRequetes[id].afficher();
        }
		
	return valeur;
    }

    ///////////////////////////////////////////////////////////////
    // Gestion du click sur les boutons de lancement de requêtes //	
    ///////////////////////////////////////////////////////////////
    gestionEvenementsBoutons()
    {
        for (var id in this.boutonsRequetes)
        {
            this.boutonsRequetes[id].gestionEvenement();
        }
    }
}
	