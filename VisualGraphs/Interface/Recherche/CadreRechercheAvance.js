
///////////////////////////////////////////////////////////////////////////////////////
// Cadre permettant de lancer des recherches évoluées, contenu dans le menu d'action //
///////////////////////////////////////////////////////////////////////////////////////
class CadreRechercheAvance
{
    constructor()
    {
        this.nbProprietes = 1; // Nombre de propriétés actuellement affichées
        this.nbProprietesMax = 5; // Nombre de propriété maximal
    }

    /////////////////////////////////////////
    // Création du contenu de la recherche //
    /////////////////////////////////////////
    afficher()
    {
        var valeur = '<div class="cadre_recherche">';
        
        valeur += '<h4>Recherche Avancée : </h4>';
        
        // Affichage du choix du label
        valeur += '<div class="select_interf_recherche"><label class="label_recherche" for="recherche_type">Type : </label>'; 
        valeur += '<select name="recherche_type" id="recherche_type">';
        valeur += '</select></div><br/>';

        // Affichage de la liste des propriétés
        valeur += "<div id='list_propriete_recherche'>";
        valeur += this.ajoutChoixPropriete(1, true);
        valeur += this.ajoutChoixPropriete(2, false);
        valeur += this.ajoutChoixPropriete(3, false);
        valeur += this.ajoutChoixPropriete(4, false);
        valeur += this.ajoutChoixPropriete(5, false);
        valeur += "</div>";
        
        // Affichage du bouton d'ajout d'un propriété
        valeur += '<img id="ajouter_propriete_recherche" src="img/agrandir_menu7.png" style="margin-top:3px;border:1px solid black;margin-left:2px;margin-right:7px;margin-bottom:-1px"/>';
        
        // Affichage du bouton de lancement de la recherche
        valeur += '<input class="bouton_lancer_recherche" type="submit" id="bouton_valider_recherche" value="Lancer" />';
        
        valeur += "</div>";
        
        return valeur;
    }
    
    ///////////////////////////////////////////////
    // Ajout d'un cadre de choix d'une propriété //
    ///////////////////////////////////////////////
    ajoutChoixPropriete(numero, visible)
    {
        if(visible)
            var valeur = "<div class='cadre_propriete_recherche' id='propriete_recherche"+numero+"' style=''>";
        else
            var valeur = "<div class='cadre_propriete_recherche' id='propriete_recherche"+numero+"' style='display:none'>";
        
        // Ajout bouton cacher la cadre de propriété
        valeur += '<img id="cacher_propriete_recherche_'+numero+'" src="img/retrecir_menu6.png" height="12" width="12" style="border:1px solid black;margin-left:2px;margin-right:7px;margin-bottom:-1px"/>';
        
        // Choix de la propriété
        valeur += '<div class="select_interf_recherche"><label class="label_recherche" for="recherche_propriete'+numero+'">Propriété : </label>'; 
        valeur += '<select class="choix_propriete_recherche" name="recherche_propriete'+numero+'" id="recherche_propriete'+numero+'">';
        valeur += '</select></div>';
        
        // Choix du type de condition
        valeur += '<div id="ligne_barre_recherche"><label for="barre_recherche"> </label>';
        valeur += '<select class="condition_recherche" name="select_condition'+numero+'" id="select_condition'+numero+'">';
        valeur += '<option value="=">égale</option>';
        valeur += '<option value="contient">contient</option>';
        valeur += '<option value="<">plus petit</option>';
        valeur += '<option value="<=">plus petit ou égale</option>';
        valeur += '<option value=">">plus grand</option>';
        valeur += '<option value=">=">plus grand ou égale</option>';
        valeur += '</select>';
       
        // Barre de recherche
        valeur += ' <input type="text" class="barre_recherche" name="barre_recherche'+numero+'" id="barre_recherche'+numero+'" />';
        
        valeur += '</div>';
        
        valeur += "</div>";
        
        return valeur;
    }
    
    ///////////////////////////////////////////////////////////////////
    // Gestion des actions sur les controleurs du cadre de recherche //
    ///////////////////////////////////////////////////////////////////
    gestionEvenements()
    {
        // Chargement des valeurs du nombre proprietés pour la gestion des boutons
        var nbProprietes = this.nbProprietes;
        var nbProprietesMax = this.nbProprietesMax;
        
        LancerRequete("", "ScriptsPHP/RecupLabels.php", this.RecupType); // Lancement auto de la requete de chargement des labels
        
        // Gestion de la recup des propriètés au changement de type de données
        $("#recherche_type").change(function()
        {
            var paramsRequete = "element=" + $("#recherche_type").val();
            
            LancerRequete(paramsRequete, "ScriptsPHP/RecupProprietes.php", visualGraphs.interf.recherche.cadreRecherche.RecupProprietes);
        });
        
        // Gestion du lancement de la requete de recherche
        $("#bouton_valider_recherche").click(function()
	{
            var requete = ""; // Contient le code de la requête
            var requeteWhere = ""; // Contient la partie après le WHERE de la requête
            var nomElement = ""; // Nom de l'élément sur lequel est placé la condition
            var valeur = ""; // 

            // Gestion de la selection du nom de l'élément sur lequel est placé la condition (si noeud ou lien)
            if($("#recherche_type").val().split(" ")[0] == "noeud")
                nomElement = "n";
            else if($("#recherche_type").val().split(" ")[0] == "lien")
                nomElement = "r";

            // Generation de la partie située après le WHERE de la requete à partir des propriètés
            for(var i = 1; i < nbProprietes+1; i++)
            {
                if($("#recherche_propriete"+i).val() != null)
                {
                    if(i == 1)
                        requeteWhere += "WHERE ";

                    // Récupération de la valeur de la barre de recherche
                    valeur = $("#barre_recherche"+i).val();
                    if(visualGraphs.params.verifDate($("#recherche_type").val().split(" ")[0], $("#recherche_type").val().split(" ")[1], $("#recherche_propriete"+i).val())) 
                    {
                        var list_valeurs = new Array();

                        // Découpage des valeurs
                        while(valeur.indexOf("/") > 0)
                        {
                            list_valeurs.push(valeur.substr(0, valeur.indexOf("/")));
                            valeur = valeur.substr(valeur.indexOf("/") + 1);
                        }
                        list_valeurs.push(valeur);

                        // Création de la valeur numérique correpondant à la date
                        valeur = 0;
                        for(var j = 0; j < list_valeurs.length; j++)
                        {
                            if(j == list_valeurs.length - 1)
                                valeur += parseFloat(list_valeurs[j]) * 10000;
                            else if(j == list_valeurs.length - 2)
                                valeur += parseFloat(list_valeurs[j]) * 100;
                            else
                                valeur += parseFloat(list_valeurs[j]) * 1;
                        }
                    }

                    // Si ce n'est pas un nombre ajout de guillemet ([-] convetie en '"' dans le script PHP)
                    if(isNaN(valeur) == true)
                    {
                        if($("#select_condition"+i).val() == "contient")
                            valeur = '[-].*' + valeur + '.*[-]';
                        else
                            valeur = '[-]' + valeur + '[-]';
                    }
                    else
                    {
                        if($("#select_condition"+i).val() == "contient")
                            valeur = '[-].*' + valeur + '.*[-]';
                    }

                    // Chargement de la condition
                    if($("#select_condition"+i).val() == "=")
                        requeteWhere += nomElement + '.' + $("#recherche_propriete"+i).val() + ' = ' + valeur + ' ';
                    else if($("#select_condition"+i).val() == "<")
                        requeteWhere += nomElement + '.' + $("#recherche_propriete"+i).val() + ' < ' + valeur + ' ';
                    else if($("#select_condition"+i).val() == ">")
                        requeteWhere += nomElement + '.' + $("#recherche_propriete"+i).val() + ' > ' + valeur + ' ';
                    else if($("#select_condition"+i).val() == "<=")
                        requeteWhere += nomElement + '.' + $("#recherche_propriete"+i).val() + ' <= ' + valeur + ' ';
                    else if($("#select_condition"+i).val() == ">=")
                        requeteWhere += nomElement + '.' + $("#recherche_propriete"+i).val() + ' >= ' + valeur + ' ';
                    else if($("#select_condition"+i).val() == "contient")
                        requeteWhere += nomElement + '.' + $("#recherche_propriete"+i).val() + ' =~ ' + valeur + ' ';

                    if(i < nbProprietes)
                        requeteWhere += "AND ";
                }
            }

            // Création de la requete (selon si lien ou noeud)
            if($("#recherche_type").val().split(" ")[0] == "noeud")
            {
                if($("#recherche_type").val().split(" ").length >= 2)
                    requete = 'MATCH (n:' + $("#recherche_type").val().split(" ")[1] + ') ' + requeteWhere + 'RETURN n';
                else
                    requete = 'MATCH (n) ' + requeteWhere + 'RETURN n';
            }
            else if($("#recherche_type").val().split(" ")[0] == "lien")
            {
                if($("#recherche_type").val().split(" ").length >= 2)
                    requete = 'MATCH (n1)-[r:' + $("#recherche_type").val().split(" ")[1] + ']-(n2) ' + requeteWhere + ' RETURN n1, n2, r';
                else
                    requete = 'MATCH (n1)-[r]-(n2) ' + requeteWhere + ' RETURN n1, n2, r';
            }
            
            var result = "requete='" + requete + "'";

            // Génération du texte de legend à partir des propriétés choisie
            var texteLegend = '';
            if($("#recherche_type").val().split(" ").length < 2)
                texteLegend = 'Recherche des tous les ' + $("#recherche_type").val().split(" ")[0] + 's';
            else
                texteLegend = 'Recherche des ' + $("#recherche_type").val().split(" ")[0] + 's de type ' + $("#recherche_type").val().split(" ")[1].replaceAll("_", " ");

            if(nbProprietes == 1 && $("#recherche_propriete"+i).val() != null)
                texteLegend += ' dont la propriété ';
            else if(nbProprietes > 1 && $("#recherche_propriete"+i).val() != null)
                texteLegend += ' dont les propriétés ';

            for(var i = 1; i < nbProprietes+1; i++)
            {
                if($("#recherche_propriete"+i).val() != null)
                {
                    texteLegend += $("#recherche_propriete"+i).val(); // Ajout de la propriété

                    // Ajout de la condition de recherche
                    if($("#select_condition"+i).val() == "=")
                        texteLegend += " est égale à ";
                    else if($("#select_condition"+i).val() == "<")
                        texteLegend += " est inférieur à ";
                    else if($("#select_condition"+i).val() == ">")
                        texteLegend += " est supérieur à ";
                    else if($("#select_condition"+i).val() == "<=")
                        texteLegend += " est inférieur ou égale à ";
                    else if($("#select_condition"+i).val() == ">=")
                        texteLegend += " est supérieur ou égale à ";
                    else if($("#select_condition"+i).val() == "contient")
                        texteLegend += " contient ";

                    texteLegend += '"' + $("#barre_recherche"+i).val() + '"'; // Ajout du contenu de la barre de recherche

                    if(i < nbProprietes)
                        texteLegend += " et ";
                }
            }
            
            // Modification de la légende et chargement des données
            visualGraphs.chargerDonnees(result, texteLegend);
	});
        
        // Gestion de l'ajout des choix de propriétés
        $("#ajouter_propriete_recherche").click(function()
	{
            nbProprietes ++;
            $("#propriete_recherche"+nbProprietes).css("display", "block"); // Affiche le cadre
            
            // Gestion des icones pour retirer un cadre
            $("#cacher_propriete_recherche_"+(nbProprietes-1)).css("display", "none");
            $("#cacher_propriete_recherche_"+nbProprietes).css("display", "inline-block");
            
            // Si nombre de propriété max atteint --> cacher bouton ajouter cadre
            if(nbProprietes >= nbProprietesMax)
                $("#ajouter_propriete_recherche").css("display", "none");
        });
        
        // Gestion du retrait des choix de propriétés
        for(var i=1; i<=this.nbProprietesMax; i++)
        {
            $("#cacher_propriete_recherche_"+i).click(function() 
            {
                $("#propriete_recherche"+(nbProprietes)).css("display", "none"); // Cache le cadre
                
                // Si nombre de propriété max pas atteint --> afficher bouton ajouter cadre
                if(nbProprietes <= nbProprietesMax)
                    $("#ajouter_propriete_recherche").css("display", "inline-block");
                
                nbProprietes --;
                
                $("#cacher_propriete_recherche_"+nbProprietes).css("display", "inline-block"); // Affichage de l'icones pour retirer un cadre
            });
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // Récupération des labels des noeuds et des types de liens ( appellé depuis fonction de requête ) //
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    RecupType(tabResult)
    {
       if(tabResult != null)
       {
            // Ajout des labels des noeuds à la liste
            $("#recherche_type").html($("#recherche_type").html() +  "<optgroup label='Noeuds : '>");
            $("#recherche_type").html($("#recherche_type").html() +  "<option value='noeud'>&nbsp;&nbsp;&nbsp;Tous les noeuds</option>");
            for(var i = 0; i < tabResult['labels'].length; i++)
            {
                $("#recherche_type").html($("#recherche_type").html() +  "<option value='noeud "+tabResult['labels'][i]+"'>&nbsp;&nbsp;&nbsp;"+tabResult['labels'][i].replaceAll('_', ' ')+"</option>");
            }
            $("#recherche_type").html($("#recherche_type").html() +  "</optgroup>");

            // Ajout des types de liens à la liste
            $("#recherche_type").html($("#recherche_type").html() +  "<optgroup label='Liens : '>");
            $("#recherche_type").html($("#recherche_type").html() +  "<option value='lien'>&nbsp;&nbsp;&nbsp;Tous les liens</option>");
            for(var i = 0; i < tabResult['types_relation'].length; i++)
            {
                $("#recherche_type").html($("#recherche_type").html() +  "<option value='lien "+tabResult['types_relation'][i]+"'>&nbsp;&nbsp;&nbsp;"+tabResult['types_relation'][i].replaceAll('_', ' ')+"</option>");
            }
            $("#recherche_type").html($("#recherche_type").html() +  "</optgroup>");
            
            // Lancement de la requête de recherche de propriété
            var paramsRequete = "element=" + $("#recherche_type").val();
            LancerRequete(paramsRequete, "ScriptsPHP/RecupProprietes.php", visualGraphs.interf.recherche.cadreRecherche.RecupProprietes);
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Récupération des propriétés depuis le label choisi ( appellé depuis fonction de requête ) //
    ///////////////////////////////////////////////////////////////////////////////////////////////
    RecupProprietes(tabProprietes)
    {
        for(var j=1; j<=5; j++) // Récupération pour chaque cadre de choix de propriété
        {
            $("#recherche_propriete"+j).html("");
            
            for(var i = 0; i < tabProprietes.length; i++) // Récupération de chaque propriété
            {
                $("#recherche_propriete"+j).html($("#recherche_propriete"+j).html() +  "<option value='"+tabProprietes[i]+"'>"+tabProprietes[i].replaceAll('_', ' ')+"</option>");   
            }
        }
    }
}