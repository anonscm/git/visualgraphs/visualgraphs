
///////////////////////////////////////////////////////////////////////
// Interface permettant d'afficher des information du lien selectionné //
/////////////////////////////////////////////////////////////////////////
class InterfInfosLien extends InterfInfos
{
    constructor(lien)
    {
        super(lien);
    }
    
    //////////////////////////////////////////////////////////////////
    // Récupération des boutons de requete liée au lien selectionné //
    //////////////////////////////////////////////////////////////////
    recupBoutonsRequete(lien)
    {
        if(visualGraphs.params.boutonsRequetesLiens != undefined)
        {
            for (var id in visualGraphs.params.boutonsRequetesLiens["*"])
            {
                this.boutonsRequetes[id] =  new BoutonAction(visualGraphs.params.boutonsRequetesLiens["*"][id], id, lien);
            }

            for (var id in visualGraphs.params.boutonsRequetesLiens[lien.type])
            {
                this.boutonsRequetes[id] =  new BoutonAction(visualGraphs.params.boutonsRequetesLiens[lien.type][id], id, lien);
            }
        }
    }
    
    ////////////////////////////////////////
    // Affichage des informations du lien //
    ////////////////////////////////////////
    afficher(lien)
    {
        var infos = "";
	
        infos += this.afficherLien(lien);
	infos += this.afficherProprietes(lien);
        
        infos += "<h4>Actions : </h4>";
        infos += this.barreExploration.afficher();
	infos += this.afficherBoutonsRequetes();
        
        infos += this.afficherCadreAnnotation(lien);
        
        infos += this.afficherInfosGrappe(lien);
	
	// Affichage du nouveau contenu
	$("#" + this.nom_div_contenu).html(infos);
        
        this.gestionCadreAnnotation(lien);
    }
    
    ///////////////////////////////////////////////////////////////////////
    // Affichage des informations du lien (noeuds reliée + type de lien) //
    ///////////////////////////////////////////////////////////////////////
    afficherLien(lien)
    {
        var infos = "";
        
        // Affichage du type de lien et des noeud qui sont relié
        var nbTotalCouleur = parseInt(lien.couleur.slice(1, 3), 16) + parseInt(lien.couleur.slice(3, 5), 16) + parseInt(lien.couleur.slice(5, 7), 16); // Récupére l'addition des 3 valeurs de la couleurs 
        
        if(nbTotalCouleur > (255/2) * 3) // Si la couleur de fond est clair on met le texte en noir, sinon on le met en blanc
            var couleurTexte = "#000000";
        else
            var couleurTexte = "#ffffff";
        
        // Affichage du premier noeud
        infos += '<div style="text-align:center">';

        infos += '<button id="' + lien.n1.id + '" class="interf_infos_lien_aff_noeud" style="margin-top : 0px;margin-bottom : 0px;background-color:' + lien.n1.couleur + ';color: '+ lien.n1.couleurTexte +'" onClick="visualGraphs.selectNoeud('+ lien.n1.id + ');">';
        
        if(lien.n1.labels[0].replaceAll("_", " ") != lien.n1.texte)
            infos += lien.n1.texte + " (" + lien.n1.labels[0].replaceAll("_", " ") + ")</button>";
        else
            infos += lien.n1.texte + "</button>";
        
        // Affichage du lien et de deuximages pour donner l'apparence de lien
	infos += '<div style="text-align:center;"><img src="img/lien_infos7.png" style="background-color:' + lien.couleur + '"></div>';
        
	infos += "<div class='interf_infos_lien_aff_type'";
	infos += ' style="margin-top : 0px;margin-bottom : 0px;background-color:' + lien.couleur + ';color: ' + couleurTexte + '" >';
        
        if(lien.type.replaceAll("_", " ") != lien.texte)
            infos += lien.texte + " (" + lien.type.replaceAll("_", " ") + ")</div>";
        else
            infos += lien.texte + "</div>";
        
	infos += '<div style="text-align:center;"><img src="img/lien_infos6.png" style="background-color:' + lien.couleur + '"></div>';
        
        // Affichage du second noeud
	infos += '<button id="' + lien.n2.id + '" class="interf_infos_lien_aff_noeud" style="margin-bottom:7px;background-color:' + lien.n2.couleur + ';color: '+ lien.n2.couleurTexte +'" onClick="visualGraphs.selectNoeud('+ lien.n2.id + ');">';
        
        if(lien.n2.labels[0].replaceAll("_", " ") != lien.n2.texte)
            infos += lien.n2.texte + " (" + lien.n2.labels[0].replaceAll("_", " ") + ")</button>";
        else
            infos += lien.n2.texte + "</button>";
        
        infos += '</div>';
        
        return infos;
    }
    
    //////////////////////////////////////////////////////////////////////
    // Recupération de la grape à laquel appartient le lien selectionné //
    //////////////////////////////////////////////////////////////////////
    recupGrapeSelect(lien)
    {
        for(var i = 0; i < visualGraphs.grapes.length; i++)
            for(var j = 0; j < visualGraphs.grapes[i].noeuds.length; j++)
                if(visualGraphs.grapes[i].noeuds[j].id == lien.n1.id)
                    return i;
        
        return -1;
    }
}