
////////////////////////////////////////////////////////////////////////////
// Liée à l'interface d'information, affiche les élements liées au Noueud //
////////////////////////////////////////////////////////////////////////////
class AffichageElementsLiees
{
    constructor(noeud)
    {
        this.noeud = noeud;
        
        this.typeLiens = new Array();
        this.tabLiens = new Array();
    }
    
    //////////////////////////////////
    // Affichage des elements liées //
    //////////////////////////////////
    afficher()
    {
        var infos = "";
        
        if(this.noeud.liens.length > 0)
            var infos = "<h4>Liens (" + this.noeud.liens.length + ") : </h4>";
	else
            return "<h4>Aucun Liens</h4>";
        
        this.trierLiens();
        
        infos += "<div id='interf_infos_noeud_element_lies'>";
        
        infos += this.affichageType();
        
        infos += "</div>";
        
        return infos;
    }
    
    //////////////////////////////////////////////////////////
    // Affichage des elements liées regroupés en catégories //
    //////////////////////////////////////////////////////////
    affichageType()
    {
        var infos = "";
        
        for(var i = 0; i < this.typeLiens.length; i++)
        {
            // Affichage du cadre indiquand la catégorie des liens
            infos += '<button id="cacher_elementlies_' + this.typeLiens[i] + '" name="' + this.typeLiens[i] + '" class="interf_infos_noeud_type_lien">';
            
            infos += '<img id="img_cacher_elementlies_' + this.typeLiens[i] + '" src="img/agrandir_menu6.png" height="12" width="12" style="border:1px solid black;margin-left:2px;margin-right:7px;margin-bottom:-1px"/>';
            
            // Dessin de la fleche
            infos += '<img src="img/lien_noeuds1.png" style="margin-bottom:-4px;background-color:' + this.tabLiens[i][0].couleurTexte + '" >';
            infos += '<img src="img/lien_noeuds2.png" style="margin-bottom:-4px;background-color:' + this.tabLiens[i][0].couleurTexte + '" >'
            
            // Affichage du type de lien et du nombre de liens
            infos += "<div class='interf_infos_noeud_aff_lien'>";
            infos += this.tabLiens[i][0].type.replaceAll("_", " ")  + " (" + this.tabLiens[i].length + ")</div>";
            
            infos += '</button><br/>';

            infos += "<div id='interf_infos_noeud_element_lies_" + this.typeLiens[i] + "' style='display:none'>";

            for(var j = 0; j < this.tabLiens[i].length; j++)
            {
                var idLien = this.tabLiens[i][j].id;
                var couleurTexteLien = visualGraphs.style.RecupElementLien(this.tabLiens[i][j].type, "CouleurTexte", "rgb(0, 0, 0)");
                
                // Récupération des infos selon le sens du lien
                if(this.tabLiens[i][j].n1.id === this.noeud.id)
                {
                    var idNoeuds = this.tabLiens[i][j].n2.id;
                    var couleurNoeuds = this.tabLiens[i][j].n2.couleur;
                    var couleurTexteNoeuds = this.tabLiens[i][j].n2.couleurTexte;
                    var texteNoeuds = this.tabLiens[i][j].n2.texte; 
                    
                    var img1 = "lien_noeuds1.png";
                    var img2 = "lien_noeuds2.png";
                }
                else
                {
                    var idNoeuds = this.tabLiens[i][j].n1.id;
                    var couleurNoeuds = this.tabLiens[i][j].n1.couleur;
                    var couleurTexteNoeuds = this.tabLiens[i][j].n1.couleurTexte;
                    var texteNoeuds = this.tabLiens[i][j].n1.texte;
                    
                    var img1 = "lien_noeuds2r.png";
                    var img2 = "lien_noeuds1r.png";
                }
                
                infos += "<div class='interf_infos_noeud_aff_element_lies'>";
                
                // Cercle representant le noeuds actuel
                infos += "<div style='display:inline-block;border-radius:8px;margin-bottom:-3px;margin-right:5px;background-color:" + this.noeud.couleur + ";width:22px;height:17px'></div>";
                
                // Affichage du lien 
                infos += '<button class="interf_infos_noeud_bouton_lien" onClick="visualGraphs.selectLien('+ idLien + ');" >';
                    infos += '<img src="img/' + img1 + '" style="background-color:' + this.tabLiens[i][0].couleurTexte + '" >';
                    infos += '<img src="img/' + img2 + '" style="background-color:' + this.tabLiens[i][0].couleurTexte + '" >';
                infos += '</button>';
                
                // Affichage du noeud lié
                infos += '<button '; 
                infos += 'class="interf_infos_noeud_bouton_noeud_lier" onClick="visualGraphs.selectNoeud('+ idNoeuds + ')"';
                infos += ' style="background-color:' + couleurNoeuds + ';color: '+ couleurTexteNoeuds +'" >';
                infos += '' + texteNoeuds + '';
                infos += '</button>';
                
                infos += "</div>";
            }
            
            infos += "</div>";
        }

        return infos;
    }
    
    //////////////////////////////////////////
    // Trie des lien du noeud en catégories //
    //////////////////////////////////////////
    trierLiens()
    {
        for(var i = 0; i < this.noeud.liens.length; i++)
        {
            // Verifie si la catégorie à été crée
            var creeTypeLien = true;
            var caseType = 0;
            for(var j = 0; j < this.typeLiens.length; j++)
            {
                if(this.noeud.liens[i].type === this.typeLiens[j])
                {
                    creeTypeLien = false;
                    caseType = j;
                }
            }
            
            // Création d'un catégorie ou ajout du lien à la catégorie
            if(creeTypeLien)
            {
                this.typeLiens.push(this.noeud.liens[i].type);
                this.tabLiens.push(new Array());
                this.tabLiens[this.tabLiens.length - 1].push(this.noeud.liens[i]);
            }
            else
            {
                this.tabLiens[caseType].push(this.noeud.liens[i]);
            }
        }
    }
    
    ///////////////////////////////////////////////////////////////
    // Gestion du clic sur les boutons de la barre d'exploration //
    ///////////////////////////////////////////////////////////////
    gestionEvenements()
    {
        // Gestion cacher/afficher les sous menus (catégories de liens)
        for(var i = 0; i < this.typeLiens.length; i++)
	{
            $("#cacher_elementlies_"+this.typeLiens[i]).click(function()
            {
                var nomLien = $(this).attr("name");
                
                if($("#interf_infos_noeud_element_lies_"+nomLien).css('display') === 'none')
                {
                    $("#interf_infos_noeud_element_lies_"+nomLien).css('display', 'inline');
                    $("#img_cacher_elementlies_"+nomLien).attr('src','img/retrecir_menu6.png');
                }
                else
                {
                    $("#interf_infos_noeud_element_lies_"+nomLien).css('display', 'none');
                    $("#img_cacher_elementlies_"+nomLien).attr('src','img/agrandir_menu6.png');
                }
            });
        }
        
    }
    
}