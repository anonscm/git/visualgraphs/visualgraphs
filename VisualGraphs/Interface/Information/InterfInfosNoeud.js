
///////////////////////////////////////////////////////////////////////////
// Interface permettant d'afficher des informations du noeud selectionné //
///////////////////////////////////////////////////////////////////////////
class InterfInfosNoeud extends InterfInfos
{
    constructor(noeuds)
    {
        super(noeuds);
    }
    
    ///////////////////////////////////////////////////////////////////
    // Récupération des boutons de requete liée au noeud selectionné //
    ///////////////////////////////////////////////////////////////////
    recupBoutonsRequete(noeud)
    {
        if(visualGraphs.params.boutonsRequetesNoeuds != undefined)
        {
            for (var id in visualGraphs.params.boutonsRequetesNoeuds["*"])
            {
                this.boutonsRequetes[id] =  new BoutonAction(visualGraphs.params.boutonsRequetesNoeuds["*"][id], id, noeud);
            }

            for(var i = 0; i < noeud.labels.length; i++)
            {
                for (var id in visualGraphs.params.boutonsRequetesNoeuds[noeud.labels[i]])
                {
                    this.boutonsRequetes[id] =  new BoutonAction(visualGraphs.params.boutonsRequetesNoeuds[noeud.labels[i]][id], id, noeud);
                }
            }
        }
    }
    
    /////////////////////////////////////////
    // Affichage des informations du noeud //
    /////////////////////////////////////////
    afficher(noeud)
    {
        var infos = "";
        
        infos += this.afficherLabels(noeud);
	infos += this.afficherProprietes(noeud);
        
        infos += this.affElementsLiees.afficher();
        
        infos += "<br/><h4>Actions : </h4>";
        infos += this.barreExploration.afficher();
        infos += this.afficherBoutonsRequetes();
        
        infos += this.afficherCadreAnnotation(noeud);
        
        infos += this.afficherInfosGrappe(noeud);
        
        infos += this.affichageBoutonExport(noeud);
        
        $("#" + this.nom_div_contenu).html(infos); // Affichage du nouveau contenu
        
        this.gestionClicExport(noeud);
        this.gestionCadreAnnotation(noeud);
    }
    
    ///////////////////////////////////////
    // Affichage du/des labels du noeuds //
    ///////////////////////////////////////
    afficherLabels(noeud)
    {
        var infos = "";
        
        for(var i = 0; i < noeud.labels.length; i++)
	{
            infos += "<h4 class='interf_infos_noeud_aff_label'";
            infos += ' style="background-color:' + noeud.couleur + ';color: '+ noeud.couleurTexte +'" >';
            infos += noeud.labels[i].replaceAll("_", " ") + "</h4>";
	}
        
        return infos;
    }
    
    ////////////////////////////////////////////////////////////////////////
    // Recupération de la grape à laquel appartient le noeuds selectionné //
    ////////////////////////////////////////////////////////////////////////
    recupGrapeSelect(noeud)
    {
        for(var i = 0; i < visualGraphs.grapes.length; i++)
            for(var j = 0; j < visualGraphs.grapes[i].noeuds.length; j++)
                if(visualGraphs.grapes[i].noeuds[j].id == noeud.id)
                    return i;
        
        return -1;
    }
    
    ///////////////////////////////////////////////////////////////////////
    // Affichage du bouton d'export des informations dans un fichier PDF //
    ///////////////////////////////////////////////////////////////////////
    affichageBoutonExport(noeud)
    {
        var infos = "";
        
        infos += '<br/><br/>';
        
        infos += "<a id='bouton_export' href='#' ><p><img id='image_export' src='img/export.png'/> Exporter les Informations</p></a>";
       
        return infos;
    }
    
    /////////////////////////////////////////////
    // Gestion du click sur le bouton d'export //
    /////////////////////////////////////////////
    gestionClicExport(noeud)
    {
        $("#bouton_export").click(function()
        {
            var params = "";
            
            // Récupération du label et du titre
            params += "label="+noeud.labels[0].replaceAll('_', ' ');
            params += "&titre="+encodeURIComponent(noeud.texte.slice(0, 70));
            
            // Récupération de propriétés
            params += "&propriete=";
            for(var id in noeud.values)
            {
                params += id.replaceAll('_', ' ') + ";" + encodeURIComponent(noeud.values[id]) + ";";
            }
            
            // Récupération des liens (type de lien, type du nom lié et texte du noeuds lié)
            params += "&liens=";
            for(var i = 0; i < noeud.liens.length; i++)
            {
                if(noeud.liens[i].type.replaceAll('_', ' ') != noeud.liens[i].texte)
                    params += encodeURIComponent(noeud.liens[i].texte) + ' (' + noeud.liens[i].type.replaceAll('_', ' ') + ");";
                else
                    params += noeud.liens[i].type.replaceAll('_', ' ') + ";";
                
                if(noeud.liens[i].n1.id != noeud.id)
                {
                    params += noeud.liens[i].n1.labels[0].replaceAll('_', ' ') + ";";
                    params += encodeURIComponent(noeud.liens[i].n1.texte) + ";";
                    params += "sens2" + ";";
                }
                else
                {
                    params += noeud.liens[i].n2.labels[0].replaceAll('_', ' ') + ";";
                    params += encodeURIComponent(noeud.liens[i].n2.texte) + ";";
                    params += "sens1" + ";";
                }
            }
            
            // Récupération des annotations
            if(localStorage.getItem('Noeud_'+noeud.id) != null)
                params += "&annotation=" + localStorage.getItem('Noeud_'+noeud.id);
            
            LancerRequete(params, "ScriptsPHP/Export.php", visualGraphs.interf.infos.chargerExport(noeud)); // Lancement auto de la requete de création du fichier
        });
    }
    
    /*
    ///////////////////////////////////////////////////////////
    // Lance la seconde méthode d'export (après 1.5 seconde) //
    ///////////////////////////////////////////////////////////
    chargerExport(noeud)
    {
        setTimeout(this.chargerExport2(noeud), 0);
    }
    */
   
    ///////////////////////////////////////////////////////////////////////
    // Lance le téléchargement du fichier d'export des données du noeuds // 
    ///////////////////////////////////////////////////////////////////////
    chargerExport(noeud)
    {
        var url = "exports/export.txt";
        
        var element = document.createElement('a');
        
        element.setAttribute('href', url);
        
        element.setAttribute('download', noeud.texte.slice(0, 70)+".txt");
        
        element.style.display = 'none';
        document.body.appendChild(element);
        
        element.click();
        
        document.body.removeChild(element);
    }
    
}