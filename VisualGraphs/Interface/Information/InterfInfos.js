
///////////////////////////////////////////////////////////////////////////////
// Interface permettant d'afficher des information sur l'élément selectionné //
///////////////////////////////////////////////////////////////////////////////
class InterfInfos extends Interface
{
    constructor(element)
    {
        super("infos", "Informations<br/><i><small>Détail de la selection<small></i>");
        
        if(element == null)
            $("#" + this.nom_div_contenu).html("<br/><p style='text-align:center'>Aucun élément selectionné</p><br/>");
        
	this.boutonsRequetes = new Array(); // Contient les boutons de lancement de requete à partir des infos d'un élément  	
        
        this.barreExploration = new BarreExploration(element);
        
        if(element != null && element.constructor.name === "Noeud")
            this.affElementsLiees = new AffichageElementsLiees(element);
        
        this.recupBoutonsRequete(element);
        this.afficher(element);
        
        this.gestionEvenementsBoutons();
        this.barreExploration.gestionEvenement();
        
        if(element != null && element.constructor.name === "Noeud")
            this.affElementsLiees.gestionEvenements();
    }
    
    //////////////////////////////////////////////////////////////////////
    // Récupération des boutons de requete liée à l'élément selectionné //
    //////////////////////////////////////////////////////////////////////
    recupBoutonsRequete(element)
    {
        
    }
    
    ////////////////////////////////
    // Affichage des informations //
    ////////////////////////////////
    afficher(element)
    {
        
    }
    
    ///////////////////////////////////////
    // Affichage des boutons de requêtes //
    ///////////////////////////////////////
    afficherBoutonsRequetes()
    {
        var valeur = "";
        
        for (var id in this.boutonsRequetes)
        {
            valeur += this.boutonsRequetes[id].afficher();
        }
	
        return valeur;
    }
    
    ///////////////////////////////////////////
    // Affichage des propriétés du l'element //
    ///////////////////////////////////////////
    afficherProprietes(element)
    {
        var infos = "<br/><br/><h4>Propriétés : </h4>";
        var vide = true;
        
        for(var id in element.values)
        {
            var valeur = "" + element.values[id];

            if(visualGraphs.params.verifDateElement(element, id)) // Gestion ajout '/' si l'élément à été défini comme une date
            {
                valeur = valeur.slice(6, 8) + '/' + valeur.slice(4, 6) + '/' + valeur.slice(0, 4);
            }
            
            var nom_propriete = id.replaceAll('_', ' '); // Remplace '_' par un espace (' ')
            
            if(valeur.indexOf('.png') >= 0 || valeur.indexOf('.jpeg') >= 0 || valeur.indexOf('.jpg') >= 0 ||valeur.indexOf('.bmp') >= 0 ||valeur.indexOf('.gif') >= 0 ||valeur.indexOf('.svg') >= 0) // Si la chaine est une image, on affiche l'image
            {
                if(valeur.indexOf('http://') >= 0 || valeur.indexOf('https://') >= 0) // Si l'image vient d'internet
                    infos += "<p class='interf_infos_propriete'><em>" + nom_propriete + " :</em><img class='img_interf_info' src='" + valeur + "' /></p>";
                else // Sinon on la récupère en local depuis notre dossier 'Images'
                    infos += "<p class='interf_infos_propriete'><em>" + nom_propriete + " :</em><img class='img_interf_info' src='Images/" + valeur + "' /></p>";
            }
            // Si la chaine est l'adresse d'un site, on crée un lien
            else if(valeur.indexOf('http://www.') >= 0 || valeur.indexOf('https://www.') >= 0) 
            {
                infos += "<p class='interf_infos_propriete'><em>" + nom_propriete + " :</em><a href='" + valeur + "'> " + valeur + "</a></p>";
            }
            // Met en gras la propriété si elle est affichée dans le noeud ou le lien
            else if((element.constructor.name === "Noeud" && visualGraphs.style.noeuds[element.labels[0]] != null && visualGraphs.style.noeuds[element.labels[0]]["Texte"] == id) || element.constructor.name === "Lien" && visualGraphs.style.liens[element.type] != null && visualGraphs.style.liens[element.type]["Texte"] == id) 
            {
                infos += "<b><p class='interf_infos_propriete'><em>" + nom_propriete + " :</em> " + valeur + "</p></b>";
            }
            // Affichage normale de la propriété
            else
                infos += "<p class='interf_infos_propriete'><em>" + nom_propriete + " :</em> " + valeur + "</p>";
            
            vide = false;
        }
        
        if(vide)
            return "<br/>";
        else
            return infos + '<br/>';
    }
    
    ///////////////////////////////////////////////////////////////
    // Gestion du click sur les boutons de lancement de requêtes //	
    ///////////////////////////////////////////////////////////////
    gestionEvenementsBoutons()
    {
        for (var id in this.boutonsRequetes)
        {
            this.boutonsRequetes[id].gestionEvenement();
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////////////
    // Affichage des infos de la grappe à laquelle l'élément selectionné est rataché //
    ///////////////////////////////////////////////////////////////////////////////////
    afficherInfosGrappe(element)
    {
        var numGrappe = this.recupGrapeSelect(element);
        
        if(numGrappe >= 0)
        {
            var nbLiens = visualGraphs.grapes[numGrappe].recupNbLiens();
            var nbNoeuds = visualGraphs.grapes[numGrappe].noeuds.length;
            return "<br/><br/><h4>Informations sur la grappe :</h4>La grappe de l'élément selectionné est composée de "+nbNoeuds+" Noeuds et de "+nbLiens+" Liens.";
        }
        else
            return "";
    }
    
    ///////////////////////////////////////
    // Affichage d'un cadre d'annotation //
    ///////////////////////////////////////
    afficherCadreAnnotation(element)
    {
        var valeur = '';
        
        valeur += "<br/><br/><h4>Annotations : </h4>";
        
        // Récupération du contenu du localStorage
        var contenu = '';
        if(localStorage.getItem(element.constructor.name+'_'+element.id) != null)
            contenu = localStorage.getItem(element.constructor.name+'_'+element.id);
        
        valeur += '<textarea id="textarea_annotation">';
        valeur += contenu;
        valeur += '</textarea>';
        
        valeur += '<input id="sav_annotation" type="submit" value="Sauvegarder" />';
        valeur += '<input id="liste_annotations" type="submit" value="Liste des annotations" />';
        
        return valeur;
    }
    
    ///////////////////////////////////////////////////////////
    // Gestion des actions des boutons liées aux annotations //
    ///////////////////////////////////////////////////////////
    gestionCadreAnnotation(element)
    {
        // Gestion de la sauvegarde de l'annotation au click sur le bouton de sauvegarde (suppression si vide)
        $("#sav_annotation").click(function()
        {
            if($("#textarea_annotation").val() == "")
                localStorage.removeItem(element.constructor.name+'_'+element.id);
            else
                localStorage.setItem(element.constructor.name+'_'+element.id, $("#textarea_annotation").val());
            
            // Effet de couleur pour montrer que la sauvegarde à eu lieu
            $("#textarea_annotation").css("background-color", "#eaffe8");
            setTimeout(function() {
                 $("#textarea_annotation").css("background-color", "#ffffff");
            }, 250);
        });
        
        // Click sur bouton 'Liste des annotations' : Ouverture de la page contenant la liste des annotations à partir des localStorage passé en paramètre
        $("#liste_annotations").click(function()
        {
           window.open("Annotations/ListeAnnotations.html");
        });
        
    }
}