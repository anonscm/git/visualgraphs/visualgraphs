
//////////////////////////////////////////////////////////
// Barre permettant de lancer l'exploration des données //
//////////////////////////////////////////////////////////
class BarreExploration
{
    constructor(element)
    {
        this.element = element;
    }
    
    /////////////////////////////////////////
    // Affichage de la barre d'exploration //
    /////////////////////////////////////////
    afficher()
    {
        var infos = "";
        
        infos += '<div class="interf_infos_barre_exploration">';
        infos += '<abbr title="Longueur maximal du chemin à partir du noeud">Exploration : ';
        infos += '<button id="interf_infos_barre_exploration_bouton_moins">-</button>'; 
        infos += '<input type="text" id="interf_infos_quantite_barre_exploration" min="1" max="4" readonly="true" disabled="true" value="1" />';
        infos += '<button id="interf_infos_barre_exploration_bouton_plus">+</button></abbr>'; 
        infos += '<input id="interf_infos_bouton_valider_exploration" type="submit" value="Lancer" />';
        infos += '</div>';
        
        return infos;
    }
    
    ///////////////////////////////////////////////////////////////
    // Gestion du clic sur les boutons de la barre d'exploration //
    ///////////////////////////////////////////////////////////////
    gestionEvenement()
    {
        // Gestion du clic sur le bouton '-'
        $("#interf_infos_barre_exploration_bouton_moins").click(function() 
        {
            var val = $("#interf_infos_quantite_barre_exploration").val();
            
            if(val > 1)
                val--;
            
            $("#interf_infos_quantite_barre_exploration").val(val);
        });
        
        // Gestion du clic sur le bouton '+'
        $("#interf_infos_barre_exploration_bouton_plus").click(function() 
        {
            var val = $("#interf_infos_quantite_barre_exploration").val();
            
            if(val < 4)
                val++;
            
            $("#interf_infos_quantite_barre_exploration").val(val);
        });
        
        // Gestion du clic sur le bouton 'Lancer' -> Création d'une requête 
        var element = this.element;
        $("#interf_infos_bouton_valider_exploration").click(function() 
        {
            if(!visualGraphs.chargement.actif)
            {
                // Modification du texte de titre de l'écran
                if(element.constructor.name === "Noeud")
                {
                    var nomElement = element.texte;

                    if(nomElement.length > 30)
                        nomElement = nomElement.substring(0, 30) + "...";

                    var txtElement = "'" + nomElement + "' (" + element.labels[0].replaceAll("_", " ") + ")";

                }
                else if(element.constructor.name === "Lien")
                {
                    if(element.texte == element.type) 
                        var txtElement = "'" + element.texte + "'";
                    else
                        var txtElement = "'" + element.texte + "' (" + element.type.replaceAll("_", " ") + ")";
                }

                var legende = "Exploration de " + txtElement;

                // Generation de la requête selon le type et la valeur d'exploration
                if(element.constructor.name === "Noeud")
                {
                    var cible = 'el1';
					
                    if (element.values['id'] == null)
                        var requete = 'MATCH (el1) WHERE id(el1) = ' + element.id + ''; // Si ne possède pas de propriété 'id' utilisation de l'id de Neo4j
                    else
                        var requete = 'MATCH (el1:' + element.labels[0] + ') WHERE el1.id = ' + element.values['id'] + ''; // Sinon utiliser propriété id

                    var sReturn = ' RETURN el1';
                    var idebut = 0;
                }
                else if(element.constructor.name === "Lien" || element.constructor.name === "LienMemeNoeud")
                {
                    var cible = 'r1';
					
                    if (element.values['id'] == null)
                        var requete = 'MATCH (el1)-[r1]-(el2) WHERE id(r1) = ' + element.id;  // Si ne possède pas de propriété 'id' utilisation de l'id de Neo4j
                    else
                        var requete = 'MATCH (el1)-[r1:' + element.type + ']-(el2) WHERE r1.id = ' + element.values['id']; // Sinon utiliser propriété id

                    var sReturn = ' RETURN r1, el1, el2';
                    var idebut = 1;
                }

                var max = $("#interf_infos_quantite_barre_exploration").val();
                for(var i = idebut; i < max; i++)
                {
                    if (element.values['id'] == null)  // Si ne possède pas de propriété 'id' utilisation de l'id de Neo4j
                        requete += ' OPTIONAL MATCH (el' + (i+1) + ')-[r' + (i+1) + ']-(el' + (i+2) + ') WHERE id(' + cible + ') = ' + element.id;
                    else  // Sinon utiliser propriété id
                        requete += ' OPTIONAL MATCH (el' + (i+1) + ')-[r' + (i+1) + ']-(el' + (i+2) + ') WHERE ' + cible + '.id = ' + element.values["id"];

                    sReturn += ', el' + (i+2) + ', r' + (i+1);
                }
                
                requete += sReturn;
                
                // Lancement de la requête (rechargement des données)
                var result = "requete='" + requete + "'";
                visualGraphs.chargerDonnees(result, legende, element);
            }
        });
    }
    
}