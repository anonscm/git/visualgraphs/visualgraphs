
////////////////////////////////////////////////////
// Classe principale de visualisation des graph //
//////////////////////////////////////////////////
class VisualGraphs
{
    constructor(canvas)
    {
        this.xhr = null;
        
  	this.style = new StyleGraph();
	this.params = new Params();
        
        this.style.recupData(this);
        this.params.recupData(this);
  	
	this.camera = new Camera(canvas.width, canvas.height);
	this.noeuds = new Array(); // 
	this.liens = new Array(); // 
        this.grapes = new Array(); // Tableau contenant les grappes
	
	this.gestEvents = new GestionnaireEvenements();
	this.gestPlacement = new GestionnairePlacement();
        
        this.chargement = new IconeChargement();
	
	this.ctx = canvas.getContext("2d");
	
	var elSelect = null; // Variable global correspondant à l'élément actuellement séléctionné (noeuds)
	var elAffInterface = null; // Variable global correspondant à l'élément actuelement afficher dans l'interface (noeuds ou lien)
        
        this.interf = new GestionnaireInterface(this.params);
        this.interfCanvas = new InterfaceCanvas();
        this.zoneTitre = new ZoneTitre(this.params);
        this.zoneLegend = new ZoneLegend(this.params);
        
        this.xhrRequete = null; // Lanceur de requête
        
        this.requete = ""; // Stockage de la requete précédente
        
        this.elAffInterface = null;
        this.typeElSelect = null;
        this.idElSelect = null;
        
        this.ecranIntroduction = new EcranIntroduction();
        
        this.recuperationDonnees();
        
        window.requestAnimationFrame(this.gestionActions);
    }
    
    ///////////////////////////////////////////////////////////////////////////////////
    // Récupération des informations fournis dans l'url pour initialiser le logiciel //
    ///////////////////////////////////////////////////////////////////////////////////
    recuperationDonnees()
    {
        if(!$_GET('requete') && this.ecranIntroduction.imageValide(this.params))
        {
            // Si il n'y a pas de requête, on affiche l'écran d'introduction
            this.ecranIntroduction.init(this.params);
            
            this.interf.afficherInterface("Recherche");
            
            window.parent.document.title = 'VisualGraphs - Introduction';
        }
        else
        {
            if(!$_GET('requete'))
            {
                // Si il n'y a pas de requête et pas d'image d'intro, on lance une requete de base (récupération de toutes les données)
                var requete = "MATCH (el1) OPTIONAL MATCH (el1)-[r]-(el2) RETURN el1, r, el2";
            }
            else
            {
                var requete = $_GET('requete');
                requete = decodeURIComponent(unescape(requete));
            }
            
            this.xhrRequete = LancerRequete("requete=" + requete, "ScriptsPHP/RecupData.php", this.init, this.xhrRequete);

            // Récupération de la legende
            if($_GET('legende'))
                this.zoneLegend.modifTexte(decodeURI($_GET('legende')));
            else
                this.zoneLegend.modifTexte("Affichage de toutes les données");
            
            window.parent.document.title = 'VisualGraphs - ' + this.zoneLegend.texte;

            // Récupération des éléments séléctionné
            if($_GET('typeElSelect') && $_GET('idElSelect'))
            {
                this.typeElSelect = $_GET('typeElSelect');
                this.idElSelect = $_GET('idElSelect');
            }
        }
       
        this.chargement.init();
    }
    
    ////////////////////////////////////////////
    // Intervalle de deplacement de la camera //
    ////////////////////////////////////////////
    gestionActions(timestamp) 
    {
        if(visualGraphs.gestPlacement.generationEnCours)
            visualGraphs.gestPlacement.placementElement(visualGraphs.chargement);
        
        visualGraphs.camera.gestionDeplacement(); 
        
        visualGraphs.interfCanvas.update(visualGraphs.noeuds, visualGraphs.camera);
        
        if(visualGraphs.ecranIntroduction.actif)
            visualGraphs.ecranIntroduction.afficher(visualGraphs.ctx);
        else
            visualGraphs.dessin();
        
        window.requestAnimationFrame(visualGraphs.gestionActions);
    }
    
    //////////////////////////////////////////////////////////////
    // Relance le logiciel afin de charger de nouvelles données //
    //////////////////////////////////////////////////////////////
    chargerDonnees(requete, legende, elSelect = null)
    {
        var a = '/';
        var b = window.location.href;
        
        // Ajout de la requete et de la legende à l'URL actuelle
        var url = b.split('?')[0];
        url = url + '?' + requete + "&legende=" + legende;
        
        // Si la requete conserne un noeud ou un lien selectionné, on envoit les infos sur cette element dans la requête
        if(elSelect != null)
        {
            if(elSelect.constructor.name === "Noeud")
                url = url + "&typeElSelect=Noeud";
            else
                url = url + "&typeElSelect=Lien";
            
            url = url + "&idElSelect=" + elSelect.id;
        }
        
        // Recharge la page
        window.location.href = url; 
    }
    
    ///////////////////////////////////////////////////////////////////
    // Chargement des données ( appellé depuis fonction de requête ) //
    ///////////////////////////////////////////////////////////////////
    init(tabResult)
    {
        try
        {
            visualGraphs.camera.pos = new Vector2(0, 0);
            visualGraphs.noeuds = new Array();
            visualGraphs.liens = new Array();
            visualGraphs.grapes = new Array();
            
            visualGraphs.recupElements(tabResult);
            
            visualGraphs.lieeNoeudsLiens(); 
            
            visualGraphs.initNumAffichageLiens();
             
            if(visualGraphs.noeuds.length > 0)
            {
                visualGraphs.gestPlacement.init(visualGraphs.noeuds, true);
            }
            else
            {
                visualGraphs.chargement.fin();
                alert("Aucune données ne correspond à votre recherche");
            }
            
            if(visualGraphs.typeElSelect != null && visualGraphs.idElSelect != null)
            {
                if(visualGraphs.typeElSelect === "Noeud")
                    visualGraphs.elAffInterface = visualGraphs.recupNoeud(visualGraphs.idElSelect);
                else if(visualGraphs.typeElSelect === "Lien")
                    visualGraphs.elAffInterface = visualGraphs.recupLien(visualGraphs.idElSelect);
                
                visualGraphs.interf.selectionElement(visualGraphs.elAffInterface);
                
                visualGraphs.interf.afficherInterface("Informations");
            }

            visualGraphs.interf.visualisation.majInfos(visualGraphs.noeuds, visualGraphs.liens);
           
            visualGraphs.cacherElements();
        }
        catch (e)
        {
           alert("Echec du chargement des données");

           visualGraphs.chargement.fin();
        }
    }
    
    /////////////////////////////////////////////
    // Création des éléments (noeuds et liens) //
    /////////////////////////////////////////////
    recupElements(tabRecup)
    {
       for(var id in tabRecup["Nodes"])
       {
           this.noeuds.push(new Noeud(tabRecup["Nodes"][id], this.style));
       }
       
       for(var id in tabRecup["Links"])
       {
            var n1 = this.recupNoeud(tabRecup["Links"][id]["StartNode"]);
            var n2 = this.recupNoeud(tabRecup["Links"][id]["EndNode"]);
            
            if(n1 !== null && n2 !== null)
            {
                if(n1 !== n2)
                    this.liens.push(new Lien(tabRecup["Links"][id], this.style, this.liens, n1, n2));
                else
                    this.liens.push(new LienMemeNoeud(tabRecup["Links"][id], this.style, this.liens, n1, n2));
            }
       }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Initialisation des grappes à partir des données des block de placement //
    ////////////////////////////////////////////////////////////////////////////
    initGrapes()
    {
        for (var i = 0; i < this.gestPlacement.blocks.length; i++) 
        {
            this.grapes.push(new Grape(this.gestPlacement.blocks[i].noeuds));
            this.grapes[i].initPosition();
        }
        
        this.gestPlacement.blocks = new Array();
    }
    
    ///////////////////////
    // Gestion du Dessin //
    ///////////////////////
    dessin()
    {
		/********** version paper.js **********/
		paper.project.activeLayer.removeChildren();
		/**************************************/
		
		/********** version html5 **********/
        //this.ctx.clearRect(0, 0, canvas.width, canvas.height);
		/***********************************/
        
        for (var i = 0; i < this.grapes.length; i++) 
            this.grapes[i].dessin(this.camera, this.ctx, this.style);
	
        // Affichage des éléments griser
        if(!this.gestPlacement.generationEnCours)
            for (var i = 0; i < this.liens.length; i++)
                if(this.liens[i].griser && this.liens[i].VerifVisible(this.camera))
                    this.liens[i].dessin(this.camera, this.ctx, this.style);
	if(!this.gestPlacement.generationEnCours && this.style.affichageTexteLiens == 1)
	{
            for (var i = 0; i < this.liens.length; i++) 
		if(this.liens[i].griser && this.liens[i].VerifVisible(this.camera))
                    this.liens[i].dessinText(this.camera, this.ctx, this.style);
	}
	for (var i = 0; i < this.noeuds.length; i++) 
            if(this.noeuds[i].griser && this.noeuds[i].VerifVisible(this.camera))
		this.noeuds[i].dessin(this.camera, this.ctx, this.style);
        
        // Affichage des éléments non griser
	if(!this.gestPlacement.generationEnCours)
            for (var i = 0; i < this.liens.length; i++)
                if(!this.liens[i].griser && this.liens[i].VerifVisible(this.camera))
                    this.liens[i].dessin(this.camera, this.ctx, this.style);
        if(!this.gestPlacement.generationEnCours && this.style.affichageTexteLiens == 1)
	{
            for (var i = 0; i < this.liens.length; i++) 
		if(!this.liens[i].griser && this.liens[i].VerifVisible(this.camera))
                    this.liens[i].dessinText(this.camera, this.ctx, this.style);
	}
        for (var i = 0; i < this.noeuds.length; i++)
            if(!this.noeuds[i].griser && this.noeuds[i].VerifVisible(this.camera))
		this.noeuds[i].dessin(this.camera, this.ctx, this.style);
        
        this.interfCanvas.afficherInfos(this.ctx);
        this.zoneTitre.afficher(this.ctx, this.style);
        this.zoneLegend.afficher(this.ctx, this.style);
        
        this.chargement.dessin(this.camera, this.ctx);
		
		/********** dessin paper.js **********/
		paper.view.update();
		/**************************************/
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // Ajout des liens liées au noeuds, si les liens ne sont plus valide, suppression du lien //
    ////////////////////////////////////////////////////////////////////////////////////////////
    lieeNoeudsLiens()
    {
	for(var i = 0; i < this.liens.length; i++)
	{
            this.liens[i].n1.addLien(this.liens[i]);
            
            if(this.liens[i].n1.id != this.liens[i].n2.id)
                this.liens[i].n2.addLien(this.liens[i]);
            
	}
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Grise tous les elements (noeuds et liens), sauf ceux proche du noeuds ou le lien passé en paramètre //
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    griserElements(element)
    {
        this.noeuds.forEach(function(n2) 
        {
            n2.griser = true;
        });
        this.liens.forEach(function(l) 
        {
            l.griser = true;
        });
        
        if(element.constructor.name === "Noeud")
        {
            element.griser = false;
            element.liens.forEach(function(l) 
            {
                l.griser = false;
                l.n1.griser = false;
                l.n2.griser = false;
            });
        }
        else if(element.constructor.name === "Lien" || element.constructor.name === "LienMemeNoeud")
        {
            element.griser = false;
            element.n1.griser = false;
            element.n2.griser = false;
        }
    }

    /////////////////////////////////////////////////
    // Degrise tous les elements (noeuds et liens) //
    /////////////////////////////////////////////////
    degriserElements()
    {
	this.noeuds.forEach(function(n) 
	{
	   n.griser = false;
	});
	
	this.liens.forEach(function(l) 
	{
            l.griser = false;
	});
    }

    ////////////////////////////////////////////////////////////
    // Selection d'un Noeuds (centrage + affichage des infos) //
    ////////////////////////////////////////////////////////////
    selectNoeud(id)
    {
	for(var i = 0; i < this.noeuds.length; i++)
	{
            if(this.noeuds[i].id === id)
            {
		this.camera.pos = new Vector2(this.noeuds[i].pos.x, this.noeuds[i].pos.y);

                this.interf.selectionElement(this.noeuds[i]);
		       
                this.elAffInterface = this.noeuds[i];				
            }
	}
    }

    /////////////////////////
    // Selection d'un Lien //
    /////////////////////////
    selectLien(id)
    {
        for(var i = 0; i < this.liens.length; i++)
	{
            if(this.liens[i].id === id)
            {
		this.camera.pos = new Vector2((this.liens[i].n1.pos.x+this.liens[i].n2.pos.x)/2, (this.liens[i].n1.pos.y+this.liens[i].n2.pos.y)/2);
				    
                this.interf.selectionElement(this.liens[i]);
		       
		this.elAffInterface = this.liens[i];	
            }
	}
    }
	
    /////////////////////////////////////////////////////////
    // Récupération d'un noeud à partir de son identifiant //
    /////////////////////////////////////////////////////////
    recupNoeud(id)
    {
        for(var i = 0; i < this.noeuds.length; i++)		
	{
            if(this.noeuds[i].id == id)
            {
                
		return this.noeuds[i];
            }
        }
		
	return null;
    }
	
    ////////////////////////////////////////////////////////
    // Récupération d'un lien à partir de son identifiant //
    ////////////////////////////////////////////////////////
    recupLien(id)
    {
        for(var i = 0; i < this.liens.length; i++)		
	{
            if(this.liens[i].id == id)
                return this.liens[i];
	}
        
	return null;
    }
    
    /////////////////////////////////////////////////////////////////
    // Cache les éléments choisi dans l'interface de visualisation //
    /////////////////////////////////////////////////////////////////
    cacherElements()
    {
        for(var i = 0; i < this.noeuds.length; i++)
        {
            var suppr = false;
            
            for(var j = 0; j < this.noeuds[i].labels.length; j++)
            {
                if(this.interf.visualisation.typeNoeuds[this.noeuds[i].labels[j]].cacher == true)
                {
                   suppr = true;
                }
            }
            
            if(suppr)
            {
                this.noeuds[i].cacher = true;
                
                // Cacher les lien liées
                for(var j = 0; j < this.noeuds[i].liens.length; j++)
                    this.noeuds[i].liens[j].cacher = true;
            }
        }
        
        for(var i = 0; i < this.liens.length; i++)
        {
            if(this.interf.visualisation.typeLiens[this.liens[i].type].cacher == true)
            {
                this.liens[i].cacher = true;
            }
        }
    }
    
    ///////////////////////////////////////////////////////////////////
    // Initialisation des valeurs d'affichage des liens (multi-lien) //
    ///////////////////////////////////////////////////////////////////
    initNumAffichageLiens()
    {
        for(var i = 0; i < this.liens.length; i++)
        {
            for(var j = i+1; j < this.liens.length; j++)
            {
                if((this.liens[i].n1.id == this.liens[j].n1.id || this.liens[i].n1.id == this.liens[j].n2.id) && (this.liens[i].n2.id == this.liens[j].n1.id || this.liens[i].n2.id == this.liens[j].n2.id) && (this.liens[i].n1.id != this.liens[i].n2.id && this.liens[j].n1.id != this.liens[j].n2.id))
                {
                    this.liens[i].nbAffichage ++;
                    this.liens[j].numAffichage = this.liens[i].nbAffichage;
                    this.liens[j].nbAffichage = this.liens[i].nbAffichage;
                }
            }
        }
    }
    
}
