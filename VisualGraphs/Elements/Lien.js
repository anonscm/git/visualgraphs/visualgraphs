
/////////////////////////////////////////////
// Classe correspondant à un lien de Neo4J //
/////////////////////////////////////////////
class Lien 
{
    constructor(jsonTab, style, l, n1, n2)
    {
        this.values = jsonTab["Values"];
        this.id = jsonTab["ID"];
        this.type = jsonTab["Type"];
        this.n1 = n1;
        this.n2 = n2;
	
        if(this.values[style.RecupElementLien(this.type, "Texte", "")] != null) {
			this.texte = this.values[style.RecupElementLien(this.type, "Texte", "")]; // Récupération du texte affiché dans le noeud
		}
        else
            this.texte = this.type.replaceAll('_', ' ');
    	
        this.longueur; // Stocke la longeur du lien
    	
        this.tailleTexte = style.RecupElementLien(this.type, "TailleTexte", 15); // Taille de l'affichage du texte
        this.taille = style.RecupElementLien(this.type, "Taille", 2); // Taille d'affichage du lien
        
        // Gestion taille par rapport aux propriétées
        var valeurPropriete = this.values[visualGraphs.style.RecupElementLien(this.type, "TailleParPropriete", "")];
        if(valeurPropriete != null && !isNaN(valeurPropriete))
        {
            this.taille += valeurPropriete * visualGraphs.style.RecupElementLien(this.type, "MultiplicateurTailleParPropriete", 1);
        }
        
        this.diriger = visualGraphs.style.RecupElementLien(this.type, "Diriger", 0); // Indique si le lien est dirigé
 
        this.griser = false; // Indique si l'élément est griser 
    	
        this.cacher = false; // Indique si le lien est caché ou pas
        
        this.posDebut = new Vector2(0, 0); // Position d'affichage du debut du lien
        this.posFin = new Vector2(0, 0); // Position d'affichage de la fin du lien
        this.posControl = null; // Position utilisé pour la gestion de la collision de courbes
        
        this.numAffichage = 1; // Numero d'affichage (influe sur la forme de l'affichage : courbes)
        this.nbAffichage = 1; // Nombre de liens entre les deux mêmes noeuds
        
        this.couleur = style.RecupElementLien(this.type, "Couleur", "rgb(150, 150, 150)");
        this.couleurGriser = style.RecupElementLien("Griser", "Couleur", "rgb(200, 200, 200)");
        
        this.couleurTexte = style.RecupElementLien(this.type, "CouleurTexte", "rgb(150, 150, 150)");
        this.couleurTexteGriser = style.RecupElementLien("Griser", "CouleurTexte", "rgb(200, 200, 200)");
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Verifie si la position est placé sur le noueuds (position d'affichage) --> Génère une figure à partir des position de la courbe et verifi que position de la souris est dedant //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    verifCollision(x, y, camera)
    {
        var tabCollision = new Array(); // Tableau contenant les points de la forme representant les position de collision du trait
        
        var distReel = this.n1.pos.distance(this.n2.pos); // Calcule distance réel
        var tailleCollision = 2 + 6 * camera.zoom; // Taille de la zone de collision (en position d'affichage)
        
        if(Math.abs(this.n1.pos.x - this.n2.pos.x) < Math.abs(this.n1.pos.y - this.n2.pos.y)) // Si écart en Y entre les points est plus grand que écart X
        {
            // Ajout position de debut dans le tableau des points de collision
            tabCollision.push(new Vector2(this.posDebut.x - tailleCollision, this.posDebut.y));
            tabCollision.push(new Vector2(this.posDebut.x + tailleCollision, this.posDebut.y));
            
            if(this.posControl !== null) // Si courbe genere une liste de points selon la taille de la courbe (coté 1)
            {
                var nbPoints = 1 + Math.ceil(distReel / 300);
                for(var i = 0; i < nbPoints; i++)
                {
                    var ratio = (i+1) / (nbPoints+1);
                    tabCollision.push(RecupPositionCourbe([this.posDebut, this.posControl, this.posFin], ratio).addition(new Vector2(+tailleCollision, 0)));
                }
            }
            
            // Ajout position de fin dans le tableau des points de collision
            tabCollision.push(new Vector2(this.posFin.x + tailleCollision, this.posFin.y));
            tabCollision.push(new Vector2(this.posFin.x - tailleCollision, this.posFin.y));
            
            if(this.posControl !== null) // Si courbe genere une liste de points selon la taille de la courbe (coté 2)
            {
                var nbPoints = 1 + Math.ceil(distReel / 300);
                for(var i = 0; i < nbPoints; i++)
                {
                    var ratio = ((i+1) / (nbPoints+1));
                    tabCollision.push(RecupPositionCourbe([this.posFin, this.posControl, this.posDebut], ratio).addition(new Vector2(-tailleCollision, 0)));
                }
            }
        }
        else //  Si écart en X entre les points est plus grand que écart Y
        {
            // Ajout position de debut dans le tableau des points de collision
            tabCollision.push(new Vector2(this.posDebut.x, this.posDebut.y - tailleCollision));
            tabCollision.push(new Vector2(this.posDebut.x, this.posDebut.y + tailleCollision));
           
            if(this.posControl !== null) // Si courbe genere une liste de points selon la taille de la courbe (coté 1)
            {
                var nbPoints = 1 + Math.ceil(distReel / 300);
                for(var i = 0; i < nbPoints; i++)
                {
                    var ratio = (i+1) / (nbPoints+1);
                    tabCollision.push(RecupPositionCourbe([this.posDebut, this.posControl, this.posFin], ratio).addition(new Vector2(0, +tailleCollision)));
                }
            }
           
            // Ajout position de fin dans le tableau des points de collision
            tabCollision.push(new Vector2(this.posFin.x, this.posFin.y + tailleCollision));
            tabCollision.push(new Vector2(this.posFin.x, this.posFin.y - tailleCollision));
           
            if(this.posControl !== null) // Si courbe genere une liste de points selon la taille de la courbe (coté 2)
            {
                var nbPoints = 1 + Math.ceil(distReel / 300);
                for(var i = 0; i < nbPoints; i++)
                {
                    var ratio = ((i+1) / (nbPoints+1));
                    tabCollision.push(RecupPositionCourbe([this.posFin, this.posControl, this.posDebut], ratio).addition(new Vector2(0, -tailleCollision)));
                }
            }
        }

        // Lancement de la vérification de la collision à partir du tableau généré précédament
        var r = this.testCollision(tabCollision, new Vector2(x, y));
        
        return r;
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // Detection de la collision entre le trait et une position (ex: souris) //
    ///////////////////////////////////////////////////////////////////////////
    testCollision(tab, P)
    {
        var i = 0;
        var I = new Vector2(0, 0);
        I.x = 10000000 + Math.random()*100;   // 10000 + un nombre aléatoire entre 0 et 99
        I.y = 10000000 + Math.random()*100;
        var nbintersections = 0;
        
        for(var i=0; i<tab.length; i++)
        {
           var A = tab[i];
           var B = new Vector2(0, 0);
           
           if (i==tab.length-1)  // si c'est le dernier point, on relie au premier
               B = tab[0];
           else           // sinon on relie au suivant.
               B = tab[i+1];
           
           var iseg = IntersectSegment(A,B,I,P);
           if (iseg == -1)
               return this.testCollision(tab,tab.length,P);  // cas limite, on relance la fonction.
           
           nbintersections+=iseg;
        }
        
        if (nbintersections%2==1)  // nbintersections est-il impair ?
           return true;
        else
           return false;
    }
    
    ///////////////////////////////////////
    // Dessin du lien entre les éléments //
    ///////////////////////////////////////  
    dessin(camera, ctx, style)
    {
		/********** version paper.js **********/
		var strokeWidth, strokeColor;
		/**************************************/
		
        // Si le lien est selectionné --> La taille du lien est doublé
        if(visualGraphs.elAffInterface == this) {
			/********** version paper.js **********/
			strokeWidth = (this.taille * camera.zoom * 2) | 0;
			/**************************************/
			
			/********** version html5 **********/
		    //ctx.lineWidth = (this.taille * camera.zoom * 2) | 0;
			/***********************************/
		}
        else {
			/********** version paper.js **********/
			strokeWidth = (this.taille * camera.zoom) | 0;
			/**************************************/
			
			/********** version html5 **********/
		    //ctx.lineWidth = (this.taille * camera.zoom) | 0;
			/***********************************/
		}
        
       // Récupération des positions d'affichage
        var x1 = camera.recupPosAffX(this.n1.pos.x);
        var x2 = camera.recupPosAffX(this.n2.pos.x);
        var y1 = camera.recupPosAffY(this.n1.pos.y);
        var y2 = camera.recupPosAffY(this.n2.pos.y);
        
        // Calcule des position réel (au bord du cercle)
        var distT = Math.sqrt(Math.pow((x2-x1),2) + Math.pow((y2-y1),2)); // Distance total
        
        if(this.n1.id == this.n2.id)
            distT = 300;
        
        var ratio1 = this.n1.taille / distT * camera.zoom; // Ratio par rapport aux rayons
        var ratio2 = this.n2.taille / distT * camera.zoom; // Ratio par rapport aux rayons

        // Détermine les 2 points du liens (là ou il entre en collision avec le noeud)
        var A = new Vector2(x1 + (x2 - x1) * ratio1, y1 + (y2 - y1) * ratio1);
        var B = new Vector2(x2 + (x1 - x2) * ratio2, y2 + (y1 - y2) * ratio2);
       
        if(this.diriger == 1)
        {
            var degrader = this.creerDegrader(ctx, A, B);
			
			/********** version paper.js **********/
			strokeColor = degrader;
			/**************************************/
			
			/********** version html **********/
            //ctx.strokeStyle = degrader;
			/**********************************/
        }
        else
        {
            if(!this.griser) {
				/********** version paper.js **********/
				strokeColor = this.couleur;
				/**************************************/
				
				/********** version html **********/
                //ctx.strokeStyle = this.couleur;
				/**********************************/
			}
            else {
				/********** version paper.js **********/
				strokeColor = this.couleurGriser;
				/**************************************/
				
				/********** version html **********/
			    //ctx.strokeStyle = this.couleurGriser;
				/**********************************/
			}
        }
        
        // Calcule de la position du milieu
        var C = new Vector2(0, 0);
        var M = new Vector2((A.x+B.x)/2, (A.y+B.y)/2);

        // Gestion d'un variable d'inversion (Utiliser pour récupérer positino intermédaire correcte)
        var inverser = false;
        if(this.n1.id > this.n2.id)
            inverser = true;
            
        // Variable permettant de réduire la taille des courbe trop grande
        var amplificationCourbe = (distT / camera.zoom) / 500;
        if(amplificationCourbe > 5)
            amplificationCourbe = 5;
        if(amplificationCourbe < 0.5)
            amplificationCourbe = 0.5;
        
        // Récupération de la position intermédiaire selon leur numero et le nombre de liens entre les noeuds et l'amplification de la courbe
        if(this.nbAffichage == 1)
        {
            C = M;
        }
        else if(this.nbAffichage == 2)
        {
            if((this.numAffichage == 1 && !inverser) || (this.numAffichage == 2 && inverser))
                C = new Vector2(M.x + (B.y-M.y) / 2 / amplificationCourbe , M.y - (B.x-M.x) / 2 / amplificationCourbe);
            else if((this.numAffichage == 2 && !inverser) || (this.numAffichage == 1 && inverser))
                C = new Vector2(M.x - (B.y-M.y) / 2 / amplificationCourbe, M.y + (B.x-M.x) / 2 / amplificationCourbe);
        }
        else if(this.nbAffichage == 3)
        {
            if(this.numAffichage == 1)
                C = M;
            else if((this.numAffichage == 2 && !inverser) || (this.numAffichage == 3 && inverser))
                C = new Vector2(M.x + (B.y-M.y) / 1.5 / amplificationCourbe, M.y - (B.x-M.x) / 1.5 / amplificationCourbe);
            else if((this.numAffichage == 3 && !inverser) || (this.numAffichage == 2 && inverser))
                C = new Vector2(M.x - (B.y-M.y) / 1.5 / amplificationCourbe, M.y + (B.x-M.x) / 1.5 / amplificationCourbe);
        }
        else if(this.nbAffichage == 4)
        {
            if((this.numAffichage == 1 && !inverser) || (this.numAffichage == 2 && inverser))
                C = new Vector2(M.x + (B.y-M.y) / 2.5 / amplificationCourbe, M.y - (B.x-M.x) / 2.5 / amplificationCourbe);
            else if((this.numAffichage == 2 && !inverser) || (this.numAffichage == 1 && inverser))
                C = new Vector2(M.x - (B.y-M.y) / 2.5 / amplificationCourbe, M.y + (B.x-M.x) / 2.5 / amplificationCourbe);
            else if((this.numAffichage == 3 && !inverser) || (this.numAffichage == 4 && inverser))
                C = new Vector2(M.x + (B.y-M.y) / amplificationCourbe, M.y - (B.x-M.x) / amplificationCourbe);
            else if((this.numAffichage == 4 && !inverser) || (this.numAffichage == 3 && inverser))
                C = new Vector2(M.x - (B.y-M.y) / amplificationCourbe, M.y + (B.x-M.x) / amplificationCourbe);
        }
        else if(this.nbAffichage >= 5)
        {
            if(this.numAffichage == 1)
                C = new Vector2(M.x, M.y);
            else if((this.numAffichage == 1 && !inverser) || (this.numAffichage == 2 && inverser))
                C = new Vector2(M.x + (B.y-M.y) / 2 / amplificationCourbe, M.y - (B.x-M.x) / 2 / amplificationCourbe);
            else if((this.numAffichage == 2 && !inverser) || (this.numAffichage == 1 && inverser))
                C = new Vector2(M.x - (B.y-M.y) / 2 / amplificationCourbe, M.y + (B.x-M.x) / 2 / amplificationCourbe);
            else if((this.numAffichage == 3 && !inverser) || (this.numAffichage == 4 && inverser))
                C = new Vector2(M.x + (B.y-M.y) / amplificationCourbe, M.y - (B.x-M.x) / amplificationCourbe);
            else if((this.numAffichage == 4 && !inverser) || (this.numAffichage == 3 && inverser))
                C = new Vector2(M.x - (B.y-M.y) / amplificationCourbe, M.y + (B.x-M.x) / amplificationCourbe);
            else 
                C = M;
        }
        
        // Stockage des position de debut, de fin et de controle (Pour gestion des collisions)
        this.posDebut = A;
        this.posFin = B;
        if(C != M)
            this.posControl = new Vector2(C.x, C.y);

        // Dessin du trait ou de la courbe
        if(C == M) {
			/********** version paper.js **********/
			this.TracerTrait(A, B, C, (10 + 5*this.taille)*camera.zoom, (8 + 4*this.taille)*camera.zoom, ctx, strokeWidth, strokeColor);
			/**************************************/
			
			/********** version html5 **********/
			//this.TracerTrait(A, B, C, (10 + 5*this.taille)*camera.zoom, (8 + 4*this.taille)*camera.zoom, ctx);
			/***********************************/
		}
        else {
			/********** version paper.js **********/
			this.TracerCourbe(A, B, C, (10 + 5*this.taille)*camera.zoom, (8 + 4*this.taille)*camera.zoom, ctx, strokeWidth, strokeColor);
			/**************************************/
			
			/********** version html5 **********/
			//this.TracerCourbe(A, B, C, (10 + 5*this.taille)*camera.zoom, (8 + 4*this.taille)*camera.zoom, ctx);
			/**********************************/
		}
        
        this.posTexte = new Vector2((C.x+M.x)/2, (C.y+M.y)/2);
        this.longueur = A.distance(B);
    }

    ///////////////////////////////////////
    // Dessin du texte placé sur le lien //
    ///////////////////////////////////////
    dessinText(camera, ctx, style)
    {
        var tailleTexte = this.tailleTexte * camera.zoom;
        
        // On affiche le texte seulement si on à la place de le mettre sur le lien et qu'il est assez grand pour etre visible
        if(tailleTexte > 5 && this.longueur >= ctx.measureText(this.texte).width + 15*camera.zoom)
        {
            // Gestion du calcule des angles du texte à partir de la position des noeuds
            this.angleTexte = Math.acos((this.n2.pos.x-this.n1.pos.x)/(Math.sqrt(Math.pow((this.n2.pos.x-this.n1.pos.x),2)+Math.pow((this.n2.pos.y-this.n1.pos.y),2)))); 
            if(this.n2.pos.y < this.n1.pos.y)
                this.angleTexte = -this.angleTexte;
            if(!(this.angleTexte > -Math.PI / 2 && this.angleTexte < Math.PI / 2))
                this.angleTexte += Math.PI;
			
			/********** version paper;js **********/
			var text = new PointText({
				point: [0, 0],
				justification: 'center',
				content: this.texte,
				fontFamily: 'Arial',
				fontWeight: 'bold',
				fontSize: tailleTexte | 0
			});
			text.translate(new Point(this.posTexte.x, this.posTexte.y));
			var rectPoint = new Point(text.bounds.topLeft.x - 5 * camera.zoom, text.bounds.topLeft.y);
			var rectWidth = text.bounds.width + 10 * camera.zoom;
			var rectHeight = text.bounds.height;
			var rect = new Path.Rectangle({
				point: rectPoint,
				size: [rectWidth, rectHeight]
			});
			text.rotate(this.angleTexte * 180 / Math.PI);
			rect.rotate(this.angleTexte * 180 / Math.PI);
		    rect.fillColor = $('canvas').css('background-color');
			text.insertAbove(rect);
			
			// Affichage du texte (nom de la relation ou une de ses propriété)
            if(!this.griser)
                text.fillColor = this.couleurTexte;
            else
                text.fillColor = this.couleurTexteGriser;
			/**************************************/
			
			/********** version html5 **********/
			/*ctx.font = "bold " + (tailleTexte | 0) + "px arial";  
            ctx.save();
            ctx.translate(this.posTexte.x, this.posTexte.y);
            ctx.rotate(this.angleTexte);
			
            // Dessin d'un rectangle derriere le texte
            ctx.fillStyle = $('canvas').css('background-color');
            ctx.fillRect (0 - ctx.measureText(this.texte).width / 2 - 3, 0 - ((this.tailleTexte/2+1)*camera.zoom), ctx.measureText(this.texte).width + 6, ((this.tailleTexte+0)*camera.zoom));
			
            // Affichage du texte (nom de la relation ou une de ses propriété)
            if(!this.griser)
                ctx.fillStyle = this.couleurTexte;
            else
                ctx.fillStyle = this.couleurTexteGriser;
			
			ctx.textAlign="center";
            ctx.textBaseline = "middle";
            ctx.fillText(this.texte, 0, 0);
			ctx.restore();*/
			/***********************************/
        }
    }
    
    /////////////////////////////////////////////////////////////
    // Dessin d'un trait et des fleches indiquand la direction //
    /////////////////////////////////////////////////////////////
    //TracerTrait(A, B, Z, ArrowLength, ArrowWidth, ctx)
    TracerTrait(A, B, Z, ArrowLength, ArrowWidth, ctx, strokeWidth, strokeColor) // version paper.js
    {
        // Calculs des coordonnées des points C, D et E
        var AB = A.distance(B);
        var C = new Vector2(B.x + ArrowLength * (Z.x-B.x) / AB, B.y + ArrowLength * (Z.y-B.y) / AB);
        var D = new Vector2(C.x + (ArrowWidth * (-(B.y-Z.y)) / AB) / 1.5, C.y + (ArrowWidth * ((B.x-Z.x)) / AB) / 1.5);
        var E = new Vector2(C.x - (ArrowWidth * (-(B.y-Z.y)) / AB) / 1.5, C.y - (ArrowWidth * ((B.x-Z.x)) / AB) / 1.5);
        
        // Dessin du trait	
		/********** version paper;js **********/
		var lien = new Path.Line({
			from: new Point(A.x, A.y),
			to: new Point(B.x, B.y),
			strokeWidth: strokeWidth,
			strokeColor: strokeColor
		});
		
		if(this.diriger == 1)
        {
            // Dessin de la flèche
			var arrow1 = new Path.Line({
				from: new Point(D.x, D.y),
				to: new Point(B.x, B.y),
				strokeWidth: strokeWidth,
				strokeColor: strokeColor
			});
			var arrow2 = new Path.Line({
				from: new Point(B.x, B.y),
				to: new Point(E.x, E.y),
				strokeWidth: strokeWidth,
				strokeColor: strokeColor
			});
        }
		/**************************************/
		
		/********** version html5 **********/
        /*ctx.beginPath();

        ctx.moveTo(A.x, A.y);
        ctx.lineTo(B.x, B.y);
        
        if(this.diriger == 1)
        {
            // Dessin de la flèche
            ctx.moveTo(D.x, D.y);
            ctx.lineTo(B.x, B.y);
            ctx.lineTo(E.x, E.y);
        }
        
        ctx.stroke();*/
		/***********************************/
    }
    
    //////////////////////////////////////////////////////////////
    // Dessin d'un courbe et des fleches indiquand la direction //
    //////////////////////////////////////////////////////////////
    //TracerCourbe(A, B, Z, ArrowLength, ArrowWidth, ctx)
    TracerCourbe(A, B, Z, ArrowLength, ArrowWidth, ctx, strokeWidth, strokeColor) // version paper.js
    {
        // Calculs des coordonnées des points C, D et E
        var AB = A.distance(B);
        var C = new Vector2(B.x + ArrowLength * (Z.x-B.x) / AB, B.y + ArrowLength * (Z.y-B.y) / AB);
        var D = new Vector2(C.x + (ArrowWidth * (-(B.y-Z.y)) / AB) / 2, C.y + (ArrowWidth * ((B.x-Z.x)) / AB) / 2);
        var E = new Vector2(C.x - (ArrowWidth * (-(B.y-Z.y)) / AB) / 2, C.y - (ArrowWidth * ((B.x-Z.x)) / AB) / 2);
		
        // Dessin du trait	
		/********** version paper.js **********/
		var lien = new Path({
			strokeWidth: strokeWidth,
			strokeColor: strokeColor
		});
		lien.add(new Point(A.x, A.y));
		lien.quadraticCurveTo(new Point(Z.x, Z.y), new Point(B.x, B.y));
		
		if(this.diriger == 1)
        {
            // Dessin de la fleche
			var arrow1 = new Path.Line({
				from: new Point(D.x, D.y),
				to: new Point(B.x, B.y),
				strokeWidth: strokeWidth,
				strokeColor: strokeColor
			});
			var arrow2 = new Path.Line({
				from: new Point(B.x, B.y),
				to: new Point(E.x, E.y),
				strokeWidth: strokeWidth,
				strokeColor: strokeColor
			});
        }
		/**************************************/
		
		/********** version html5 **********/
        /*ctx.beginPath();

        ctx.moveTo(A.x, A.y);
        ctx.quadraticCurveTo(Z.x, Z.y, B.x, B.y);

        if(this.diriger == 1)
        {
            // Dessin de la fleche
            ctx.moveTo(D.x, D.y);
            ctx.lineTo(B.x, B.y);
            ctx.lineTo(E.x, E.y);
        }

        ctx.stroke();*/
		/***********************************/
    }
    
    ////////////////////////////////////////////////
    // Verifie si l'élement est visible à l'écran //
    ////////////////////////////////////////////////
    VerifVisible(camera)
    {
        if(this.cacher)
            return false;  		
        else if(this.n1.pos.x + this.n1.taille < camera.pos.x - camera.taille.x/2/camera.zoom
        && this.n2.pos.x + this.n2.taille < camera.pos.x - camera.taille.x/2/camera.zoom)
            return false;
        else if(this.n1.pos.x - this.n1.taille > camera.pos.x + camera.taille.x/2/camera.zoom
        && this.n2.pos.x - this.n2.taille > camera.pos.x + camera.taille.x/2/camera.zoom)
            return false;
        else if(this.n1.pos.y + this.n1.taille < camera.pos.y - camera.taille.y/2/camera.zoom
        && this.n2.pos.y + this.n2.taille < camera.pos.y - camera.taille.y/2/camera.zoom)
            return false;
        else if(this.n1.pos.y - this.n1.taille > camera.pos.y + camera.taille.y/2/camera.zoom
        && this.n2.pos.y - this.n2.taille > camera.pos.y + camera.taille.y/2/camera.zoom)
            return false; 
        else
            return true;
    }
    
    /////////////////////////////////
    // Création du dégradé du lien //
    /////////////////////////////////
    creerDegrader(ctx, A, B)
    {
		if(!this.griser)
        {
            var couleur1 = ModifierCouleur(this.couleur, 1.07);
            var couleur2 = ModifierCouleur(this.couleur, 0.93);   
        }
        else
        {
            var couleur1 = ModifierCouleur(this.couleurGriser, 1.07);
            var couleur2 = ModifierCouleur(this.couleurGriser, 0.93);   
        }
		
		/********** version paper.js **********/
		var gradient = {
			stops: [couleur1, couleur2],
			radial: false
		};
		var origin = new Point(A.x, A.y);
		var destination = new Point(B.x, B.y);
		
		return {
			gradient: gradient,
			origin: origin,
			destination: destination
		};
		/**************************************/
		
		/********** version html5 **********/
        /*var gradient = ctx.createLinearGradient(A.x, A.y, B.x, B.y);
        gradient.addColorStop(0, couleur1);
        gradient.addColorStop(1, couleur2);

        return gradient;*/
		/***********************************/
    }
    
    
    /*
    //////////////////////////////////////////////
    // Affichage de test des zones de collision //
    //////////////////////////////////////////////
    AfficherZonesCollision(camera, ctx)
    {
        ctx.lineWidth = 1 * camera.zoom;
        ctx.strokeStyle = "#FF0000";
        
        var j = 0;
        for(var i = 0; i < this.tabCollision.length; i++)
        {
            if(i+1 >= this.tabCollision.length)
                j = 0;
            else
                j=i+1;
            
            var x1 = this.tabCollision[i].x;
            var x2 = this.tabCollision[j].x;
            var y1 = this.tabCollision[i].y;
            var y2 = this.tabCollision[j].y;
           
            this.TracerTrait(new Vector2(x1, y1), new Vector2(x2, y2), new Vector2(x1, y1), 0, 0, ctx);
        }  
    }
    */
}
