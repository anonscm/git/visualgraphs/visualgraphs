/////////////////////////////////////////////////////////////////
// Classe correspondant à un lien reliant un noeuds à lui même //
/////////////////////////////////////////////////////////////////
class LienMemeNoeud extends Lien
{
    constructor(jsonTab, style, l, n1, n2)
    {
        super(jsonTab, style, l, n1, n2);
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Verifie si la position passé en paramétre se situe dans le zone du lien ( considérée comme un cercle ) //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    verifCollision(x, y, camera)
    {
        // Conversion en position réel		
        x = (-camera.taille.x/2 + x) /camera.zoom + camera.pos.x;
        y = (-camera.taille.y/2 + y) /camera.zoom + camera.pos.y;
        
        var c = new Vector2(this.n1.pos.x, this.n1.pos.y - 85);
        var tailleCollision = 55;
        
        // Verifie si la distance est plus petite que le rayon
        if(Math.sqrt(Math.pow((c.x-x),2) + Math.pow((c.y-y),2)) <= tailleCollision)
            return true;
        else
            return false;
    }
    
    ///////////////////////////////////////
    // Dessin du lien entre les éléments //
    ///////////////////////////////////////
    dessin(camera, ctx, style)
    {
        // Récupération des positions d'affichage
        var x1 = camera.recupPosAffX(this.n1.pos.x);
        var x2 = camera.recupPosAffX(this.n2.pos.x);
        var y1 = camera.recupPosAffY(this.n1.pos.y);
        var y2 = camera.recupPosAffY(this.n2.pos.y);
        
        var A = new Vector2(x1, y1);
        var B = new Vector2(x2, y2);

        var C1 = new Vector2(A.x - 30 * camera.zoom, A.y - 100 * camera.zoom);
        var C2 = new Vector2(A.x + 30 * camera.zoom, A.y - 100 * camera.zoom);
		
		/********** version paper.js **********/
		var lien = new Path();
		
        // Si le lien est selectionné --> taille x2
        if(visualGraphs.elAffInterface == this)
            lien.strokeWidth = (this.taille * camera.zoom * 2) | 0;
        else 
            lien.strokeWidth = (this.taille * camera.zoom)  | 0;

        // Récupération de la couleur du lien        
        if(!this.griser)
            lien.strokeColor = this.couleur;
        else
            lien.strokeColor = this.couleurGriser;
		
		lien.add(new Point(A.x, A.y));
		lien.add(new Point(C1.x, C1.y));
		lien.quadraticCurveTo(new Point(A.x, C2.y - 45 * camera.zoom), new Point( C2.x, C2.y));
		this.TracerTrait(new Vector2(C2.x, C2.y - 5 *  camera.zoom), new Vector2(B.x + this.n1.taille / 3 *  camera.zoom, B.y - this.n1.taille * camera.zoom), new Vector2(C2.x, C2.y), (5 + 5*this.taille)*camera.zoom, (4 + 4*this.taille)*camera.zoom, ctx, lien.strokeWidth, lien.strokeColor);
		/**************************************/
		
		/********** version html5 **********/
        /*// Si le lien est selectionné --> taille x2
        if(visualGraphs.elAffInterface == this)
            ctx.lineWidth = (this.taille * camera.zoom * 2) | 0;
        else 
            ctx.lineWidth = (this.taille * camera.zoom)  | 0;

        // Récupération de la couleur du lien        
        if(!this.griser)
            ctx.strokeStyle = this.couleur;
        else
            ctx.strokeStyle = this.couleurGriser;

        // Dessin
        ctx.beginPath();
        ctx.moveTo(A.x, A.y);
        ctx.lineTo(C1.x, C1.y);
        ctx.quadraticCurveTo(A.x, C2.y - 45 * camera.zoom, C2.x, C2.y);
        ctx.moveTo(C2.x, C2.y);
        ctx.stroke();
        this.TracerTrait(new Vector2(C2.x, C2.y - 5 *  camera.zoom), new Vector2(B.x + this.n1.taille / 3 *  camera.zoom, B.y - this.n1.taille * camera.zoom), new Vector2(C2.x, C2.y), (5 + 5*this.taille)*camera.zoom, (4 + 4*this.taille)*camera.zoom, ctx);*/
		/***********************************/
        
        this.posTexte = new Vector2((C1.x+C2.x)/2, (C1.y+C2.y)/2);
        this.longueur = 500 * camera.zoom;
    }
    
}
