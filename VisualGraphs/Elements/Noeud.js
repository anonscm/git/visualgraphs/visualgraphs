
//////////////////////////////////////////////
// Classe correspondant à un noeud de Neo4J //
//////////////////////////////////////////////
class Noeud
{
    constructor(jsonTab, style)
    {
        this.labels = jsonTab["Labels"];
    	this.values = jsonTab["Values"]; // Propriétés du noeud
    	this.id = jsonTab["ID"];
    	
        if(this.values[style.RecupElementNoeud(this.labels[0], "Texte", "")] != null)
            this.texte = "" +  this.values[style.RecupElementNoeud(this.labels[0], "Texte", "")]; // Récupération du texte affiché dans le noeud
    	else
            this.texte = this.labels[0].replaceAll('_', ' ');
        
        this.grape = null; // Grappe dans lequel est contenu le noeuds
         
    	this.pos = new Vector2(1000000000, 1000000000);
        
    	this.placer = false; // Boolean indiquand si le noeuds a été placé
    	
	this.tailleBase = style.RecupElementNoeud(this.labels[0], "Taille", 40); // Taille de l'affichage du noeud   	
    	this.tailleTexte = style.RecupElementNoeud(this.labels[0], "TailleTexte", 18); // Taille de l'affichage du texte
    	
        this.tailleParLien = style.RecupElementNoeud(this.labels[0], "TailleParLien", 1); // Taille supplementaire de l'affichage du noeud pour chaque Lien liée
        
    	this.taille = this.tailleBase;
    	
    	this.griser = false;
		
    	this.liens = new Array(); // Liens liées au noeuds
		
	this.distanceGenerationMin = 0;
        
    	this.cacher = false; // Indique si le noeuds est caché ou pas
        
        this.couleur = style.RecupElementNoeud(this.labels[0], "Couleur", "rgb(150, 150, 150)");
        this.couleurGriser = style.RecupElementNoeud("Griser", "Couleur", "rgb(200, 200, 200)");
        
        this.couleurTexte = style.RecupElementNoeud(this.labels[0], "CouleurTexte", "rgb(0, 0, 0)");
        this.couleurTexteGriser = style.RecupElementNoeud("Griser", "CouleurTexte", "rgb(255, 255, 255)");
    }
    
    //////////////////////////////////////////////////
    // Placement du noeud à une position spécifique //
    //////////////////////////////////////////////////
    initPosition(x, y)
    {
	this.placer = true;	  	
  	
	this.pos = new Vector2(x, y);
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Calcule de la taille du noeuds a partir de sa taille de base et de son nombre de liens et de ses parametrage //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    calculeTaille()
    {
        this.taille = this.tailleBase + this.liens.length * this.tailleParLien;
        
        // Gestion taille par rapport aux propriétées
        var valeurPropriete = this.values[visualGraphs.style.RecupElementNoeud(this.labels[0], "TailleParPropriete", "")];
        if(valeurPropriete != null && !isNaN(valeurPropriete))
        {
            this.taille += valeurPropriete * visualGraphs.style.RecupElementNoeud(this.labels[0], "MultiplicateurTailleParPropriete", 1);
        }
    }
    
    ///////////////////////////////////////////////
    // Tri des liens selon leurs nombre de liens //
    ///////////////////////////////////////////////
    trierLiens()
    {
        for(var i = 0; i < this.liens.length - 1; i++)
        {
            for(var j = i+1; j < this.liens.length; j++)
            {
                var n1, n2;
                if(this.id != this.liens[i].n1.id)
                    n1 = this.liens[i].n1;
                else
                    n1 = this.liens[i].n2;
		
                if(this.id != this.liens[j].n1.id)
                    n2 = this.liens[j].n1;
                else
                    n2 = this.liens[j].n2;
				
                if(n1.liens.length < n2.liens.length)
                {
                    var temp = this.liens[j];
                    this.liens[j] = this.liens[i];
                    this.liens[i] = temp;
                }
            }
        }
        
        this.calculeTaille();
    }
  
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Placement de l'élement par rapport à la position d'un autre noeud (n = noeuds à partir duquel est placé le noeuds actuel) //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    initPositionRelative(n, noeuds)
    {
        // Récupération des variables de placements
        var distancePlacementBase = visualGraphs.params.placement["DistanceGeneration"]["Min"] + (this.liens.length * visualGraphs.params.placement["DistanceGeneration"]["ParNoeud"]) + this.taille + n.taille;
        
        if(distancePlacementBase > visualGraphs.params.placement["DistanceGeneration"]["Max"]) // Limite de la distance de placement maximal
            distancePlacementBase = visualGraphs.params.placement["DistanceGeneration"]["Max"];
        
        var distancePlacement = distancePlacementBase;
  	
        do
        {
            var listPositions = new Array();
            var distMin = new Array();
			
            var distancePlacementPosition;        
            var t;
            
            // Génération d'une liste de positions possible (100)
            for(var i = 0; i < 100; i++)
            {
                // Ajout de l'aléatorie de la distance de placement de l'élément
                distancePlacementPosition = distancePlacement + (Math.random() * ((distancePlacementBase/8) - (-distancePlacementBase/8)) + (-distancePlacementBase/8));
                
                // Génération d'une position
                t =  Math.random() * (Math.PI - (-Math.PI)) + -Math.PI; // t = Position sur le cercle
                listPositions.push(new Vector2(n.pos.x + (distancePlacementPosition) * Math.cos(t), n.pos.y + (distancePlacementPosition) * Math.sin(t)));
              
                // Si la position n'est pas valide on la supprimme
                this.pos = listPositions[listPositions.length - 1];
                if(!this.verifPositionValide(noeuds))
                {
                    listPositions.pop();
                }
            }
            
            // Calcule des distance Min (avec tous les noeuds)
            for(var i = 0; i < listPositions.length - 1; i++)
            {
                distMin.push(100000000000)
                
                for(var j = 0; j < noeuds.length; j++)
                {
                    if(noeuds[j].id !== this.id && noeuds[j].placer)
                    {
                        var dist = listPositions[i].distance(noeuds[j].pos);

                        if(dist < distMin[i])
                        {
                            distMin[i] = dist;
                        }
                    }
                }
            }
                
            // Tri des positions selon leur distances avec les autres noeuds calculées  précédament
            var changement = 0;
            do {
                changement = 0;
                for(var i = 0; i < listPositions.length - 1; i++)
                {
                    if(distMin[i] < distMin[i+1])
                    {
                        var tempP = listPositions[i];
                        listPositions[i] = listPositions[i+1];
                        listPositions[i+1] = tempP;

                        var tempD = distMin[i];
                        distMin[i] = distMin[i+1];
                        distMin[i+1] = tempD;

                        changement ++;
                    }
                }

            } while(changement !== 0);

            // Si aucune position valide aggrandissement de la distance de placement
            distancePlacement += distancePlacementBase/5;
			
        } while(listPositions.length === 0); // Vérifie si au moins une position est valide
        
        // Récupération de la 'meilleur position' (plus éloignée des autres noeuds)
        this.pos = listPositions[0];
        this.placer = true;
        
        // Modification de la distance de génération minimum de noeuds 'parent'
        if(distancePlacementPosition > n.distanceGenerationMin)
            n.distanceGenerationMin = distancePlacementPosition;
    }
    
    ////////////////////////////////////////////////
    // Verifie si la position du noeud est valide //
    ////////////////////////////////////////////////  
    verifPositionValide(noeuds)
    {
        var distMin = 1;
	
        this.calculeTaille();
	
  	for(var i = 0; i < noeuds.length; i++)
  	{
            if(noeuds[i].id !== this.id)
            {
                if(noeuds[i].distanceGenerationMin !== 0 && this.verifNoeudsNonLiees(noeuds[i]))
                    distMin = noeuds[i].distanceGenerationMin;
		else
                {
                    if(this.verifLienEnCommun(noeuds[i]))
                        distMin = (this.taille + noeuds[i].taille) * visualGraphs.params.placement["DistanceMinLiee"]["Multiplicateur"] + visualGraphs.params.placement["DistanceMinLiee"]["Min"];
                    else
                        distMin = (this.taille + noeuds[i].taille) * visualGraphs.params.placement["DistanceMinNonLiee"]["Multiplicateur"] + visualGraphs.params.placement["DistanceMinNonLiee"]["Min"];
                }
                    
                if(this.pos.distance(noeuds[i].pos) < distMin)
                {
                    return false;			
 		}
            }
  	}
  	
  	return true;
    }
    
    //////////////////////////////////////////////////////////////
    // Vérifie si les noeuds ont unou plusieurs liens au commun //
    //////////////////////////////////////////////////////////////
    verifLienEnCommun(n)
    {
        for(var i = 0; i < this.liens.length; i++)
        {
            for(var j = 0; j < n.liens.length; j++)
            {
                if(this.liens[i].n1.id == n.liens[j].n1.id || this.liens[i].n1.id == n.liens[j].n2.id || this.liens[i].n2.id == n.liens[j].n1.id || this.liens[i].n2.id == n.liens[j].n2.id)
                    return true;
            }
        }
        
        return false;
    }
    
    ///////////////////////////////////////////////////////////////////////////////////
    // Verifie que le noeud passé en paramétre n'est pas directement liée à celui ci //
    ///////////////////////////////////////////////////////////////////////////////////
    verifNoeudsNonLiees(n)
    {
        for(var i = 0; i < this.liens.length; i++)
	{
            if(this.liens[i].n1.id === n.id)
		return false;
            if(this.liens[i].n2.id === n.id)
                return false;
	}
		
	return true;
    }
  
    /////////////////////////////////////////////////// 
    // Création d'un lien entre le noueud et le lien //
    /////////////////////////////////////////////////// 	
    addLien(new_lien)
    {
  	this.liens.push(new_lien);	
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Verifie si la position est placé sur le noueuds (position d'affichage) //
    ////////////////////////////////////////////////////////////////////////////  
    verifCollision(x, y, camera)
    {
        if(!this.cacher)
  	{
            // Conversion en position réel		
            x = (-camera.taille.x/2 + x) /camera.zoom + camera.pos.x;
            y = (-camera.taille.y/2 + y) /camera.zoom + camera.pos.y;
	
            // Verifie si la distance est plus petite que le rayon
            if(Math.sqrt(Math.pow((this.pos.x-x),2) + Math.pow((this.pos.y-y),2)) <= this.taille)
                return true;
            else
	  	return false;
  	}
  	else 
            return false;
    }
  
    ///////////////////////////
    // Deplacement du noeuds //
    ///////////////////////////
    deplacer(x, y, camera)
    {
  	this.pos = camera.recupPositionReel(x, y);
    }
    
    ////////////////////////////////////////////
    // Dessin du noeuds et affichage du texte //
    ////////////////////////////////////////////
    dessin(camera, ctx, style)
    {
  	    // Dessin du noeud
		/********** version paper.js **********/
        var circle = new Path.Circle(
		    new Point(camera.recupPosAffX(this.pos.x),
			camera.recupPosAffY(this.pos.y)),
			(this.taille * camera.zoom) | 0
		);
        var degrader = this.creerDegrade(ctx, camera);
        circle.fillColor = degrader;
		
		if(visualGraphs.elAffInterface === this)
        {
			circle.strokeWidth = (3 * camera.zoom * 2) + 1;
			circle.strokeColor = ModifierCouleur(this.couleur, 0.85);
        }
		/**************************************/
		
		/********** version html5 **********/
	    /*ctx.beginPath();
        
        ctx.arc(camera.recupPosAffX(this.pos.x), camera.recupPosAffY(this.pos.y), (this.taille * camera.zoom) | 0, 0, 2*Math.PI);
        
        var degrader = this.creerDegrade(ctx, camera);
            ctx.fillStyle = degrader;
 
     	ctx.fill();
     	
        // Si le noeud est séléctionné, on affiche la bordure
     	if(visualGraphs.elAffInterface === this)
        {
            ctx.lineWidth =  (3 * camera.zoom * 2) + 1;
            ctx.strokeStyle = ModifierCouleur(this.couleur, 0.85);
            ctx.stroke();
        }*/
		/***********************************/
		
		var tailleTexte = (this.tailleTexte*camera.zoom);
		
		if(tailleTexte > 5)
		{
            this.afficherTexte(ctx, camera, tailleTexte);
        }
		
		/********** version paper.js **********/
		//view.draw();
		/**************************************/
    }
    
    ///////////////////////////////////////////////////////////////
    // Gestion de l'affichage du texte placé au centre du noeuds //
    ///////////////////////////////////////////////////////////////
    afficherTexte(ctx, camera, tailleTexte)
    {
        // Découpage du texte dans un tableau afin de l'afficher en plusieurs parties
        var tabText = new Array();
        if(this.texte != null)
            tabText = this.texte.split(' ');

        // Assemblage des parties du texte pour mettre plusieurs mots par ligne
        for(var i = 0; i < tabText.length-1; i++)
        {
            while(tabText.length - 1 > i && (tabText[i].length + tabText[i+1].length) * this.tailleTexte / 3 < this.taille)
            {
                tabText[i] = tabText[i] + ' ' + tabText[i+1];

                tabText.splice(i+1, 1);
            }
        }

        // Retrait des parties du texte en trop
        while((tabText.length-1) * this.tailleTexte > this.taille)
        {
            tabText.pop();

            if((tabText[tabText.length - 1].length) * this.tailleTexte / 3 < this.taille)
                tabText[tabText.length - 1] += " ...";
            else
                tabText[tabText.length - 1] = "...";
        }
		
		/********** version paper.js **********/
		for(var i = 0; i < tabText.length; i++)
        {
            // Affichage du texte
			var text = new PointText({
				point: [
				    camera.recupPosAffX(this.pos.x),
					camera.recupPosAffY(this.pos.y + this.tailleTexte/3.5 - (tabText.length * this.tailleTexte / 2) + ((i + 0.5) * this.tailleTexte))
				],
				justification: 'center',
				content: tabText[i],
				fontFamily: 'Arial',
				fontSize: tailleTexte | 0
			});
			
			if(!this.griser)
				text.fillColor = this.couleurTexte; // Récupération de la couleur du texte
			else
				text.fillColor = this.couleurTexteGriser; // Récupération de la couleur du texte lorsuq'il est griser
        }
		/**************************************/
        
        /********** version html5 **********/
		/*if(!this.griser)
            ctx.fillStyle = this.couleurTexte; // Récupération de la couleur du texte
        else
            ctx.fillStyle = this.couleurTexteGriser; // Récupération de la couleur du texte lorsuq'il est griser
        
        ctx.textAlign = "center";
        ctx.font = (tailleTexte | 0) + "px Arial";

        for(var i = 0; i < tabText.length; i++)
        {
            // Affichage du texte
            ctx.fillText(tabText[i], camera.recupPosAffX(this.pos.x), camera.recupPosAffY(this.pos.y + this.tailleTexte/3.5 - (tabText.length * this.tailleTexte / 2) + ((i + 0.5) * this.tailleTexte)));
        }*/
		/***********************************/
    }  
    
    ////////////////////////////////////////////////
    // Verifie si l'élement est visible à l'écran //
    ////////////////////////////////////////////////
    VerifVisible(camera)
    {
  	if(!this.cacher
  	&& this.pos.x + this.taille > camera.pos.x - camera.taille.x/2/camera.zoom
  	&& this.pos.x - this.taille < camera.pos.x + camera.taille.x/2/camera.zoom
  	&& this.pos.y + this.taille > camera.pos.y - camera.taille.y/2/camera.zoom
  	&& this.pos.y - this.taille < camera.pos.y + camera.taille.y/2/camera.zoom)
            return true;
  	else
            return false;
    }
    
    //////////////////////////////////////
    // Création du dégradé sur le noeud //
    //////////////////////////////////////
    creerDegrade(ctx, camera)
    {
        if(!this.griser)
        {
            var couleur1 = ModifierCouleur(this.couleur, 1.1);
            var couleur2 = ModifierCouleur(this.couleur, 0.95);
        }
        else
        {
            var couleur1 = ModifierCouleur(this.couleurGriser, 1.1);
            var couleur2 = ModifierCouleur(this.couleurGriser, 0.95);
        }
		
		/********** version paper.js **********/
		var gradient = {
			stops: [couleur1, couleur2],
			radial: true
		};
		var origin = new Point(camera.recupPosAffX(this.pos.x), camera.recupPosAffY(this.pos.y));
		var destination = new Point(camera.recupPosAffX(this.pos.x + (this.taille * camera.zoom)), camera.recupPosAffY(this.pos.y));
		
		return {
			gradient: gradient,
			origin: origin,
			destination: destination
		};
		/**************************************/
		
	    /********** version html5 **********/
		/*var pos = new Vector2(camera.recupPosAffX(this.pos.x), camera.recupPosAffY(this.pos.y));
        var gradient = ctx.createRadialGradient(pos.x, pos.y, 0, pos.x, pos.y, (this.taille * camera.zoom));
        gradient.addColorStop(0, couleur1);
        gradient.addColorStop(1, couleur2);

        return gradient;*/
		/***********************************/
    }
    
}
