
///////////////////////////////////////////////////////////////////////////////
// Classe correspondant à un groupe de noeuds reliés entre eux par des liens //
///////////////////////////////////////////////////////////////////////////////
class Grape
{
    constructor(noeuds)
    {
        this.noeuds = noeuds; // Tableau des noeuds contenu dans la grappe
        
        this.pos = new Vector2(0, 0); // Position centrale de la grappe
        this.taille = new Vector2(0, 0); // Taille de la grappe
        
        // Lie les noeuds et les grappes
        for(var i = 0; i < this.noeuds.length; i++)
            this.noeuds[i].grape = this;
    }
    
    //////////////////////////////////////////////////////////////////
    // Calcule de la position centrale et de la taille de la grappe //
    //////////////////////////////////////////////////////////////////
    initPosition()
    {
        var posMin = new Vector2(10000000, 10000000);
        var posMax = new Vector2(-10000000, -10000000);
        
        for(var i = 0; i < this.noeuds.length; i++)
        {
            if(this.noeuds[i].pos.x - this.noeuds[i].taille * 2 < posMin.x)
                posMin.x = this.noeuds[i].pos.x - this.noeuds[i].taille * 2;
            if(this.noeuds[i].pos.y - this.noeuds[i].taille * 2 < posMin.y)
                posMin.y = this.noeuds[i].pos.y - this.noeuds[i].taille * 2;
            
            if(this.noeuds[i].pos.x + this.noeuds[i].taille * 2 > posMax.x)
                posMax.x = this.noeuds[i].pos.x + this.noeuds[i].taille * 2;
            if(this.noeuds[i].pos.y + this.noeuds[i].taille * 2 > posMax.y)
                posMax.y = this.noeuds[i].pos.y + this.noeuds[i].taille * 2;
        }

        this.taille = new Vector2(posMax.x - posMin.x, posMax.y - posMin.y);
        this.pos = new Vector2((posMax.x + posMin.x) / 2, (posMax.y + posMin.y) / 2);
    }
    
    /////////////////////////////////////
    // Dessin des bordure de la grappe //
    /////////////////////////////////////
    dessin(camera, ctx, style)
    {
        this.initPosition();
		
		/********** version paper.js **********/
		var bordure = new Path.Rectangle({
			point: [camera.recupPosAffX(this.pos.x) - (this.taille.x * camera.zoom) / 2, camera.recupPosAffY(this.pos.y) - (this.taille.y * camera.zoom) / 2],
			size: [this.taille.x * camera.zoom, this.taille.y * camera.zoom],
			strokeWidth: style.cadre["BordureGrapes"]["Taille"] * camera.zoom,
			strokeColor: style.cadre["BordureGrapes"]["Couleur"]
		});
		/**************************************/
        
		/********** version html5 **********/
        /*ctx.beginPath();
        
        ctx.rect(camera.recupPosAffX(this.pos.x) - (this.taille.x * camera.zoom) / 2, camera.recupPosAffY(this.pos.y) - (this.taille.y * camera.zoom) / 2, this.taille.x * camera.zoom, this.taille.y * camera.zoom);
        
        ctx.strokeStyle = style.cadre["BordureGrapes"]["Couleur"];
        ctx.lineWidth = style.cadre["BordureGrapes"]["Taille"] * camera.zoom;
	
        ctx.stroke();*/
		/***********************************/
    }
    
    ///////////////////////////////////////////////////////
    // Renvoit le nombre de liens contenu dans la grappe //
    ///////////////////////////////////////////////////////
    recupNbLiens()
    {
        var nbLiens = 0;
        
        for(var i = 0; i < this.noeuds.length; i++)
        {
            for(var j = 0; j < this.noeuds[i].liens.length; j++)
            {
                if(this.noeuds[i].liens[j].n1.id === this.noeuds[i].id)
                    nbLiens ++;
            }
        }
        
        return nbLiens;
    }
}