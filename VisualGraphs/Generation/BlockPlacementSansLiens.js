
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Classe permettant de gerer des blocks de noeuds utiliser pour placer les noeuds qui n'ont pas de liens avec d'autres noeuds //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class BlockPlacementSansLiens extends BlockPlacement
{
    constructor(tabIdNoeuds, numero)
    {
        super(tabIdNoeuds, numero);
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////
    // Placement des noeuds dans le cas d'un block regroupant tous les noeuds sans liens //
    ///////////////////////////////////////////////////////////////////////////////////////
    placementNoeuds()
    {
        this.noeuds[0].pos = new Vector2(0, 0);
        this.noeuds[0].placer = true;
        
        for(var i = 1; i < this.noeuds.length; i++)
        {
            // Génération d'un tableau de positions autours d'un noeud choisi aléatoirement
            var tabPos = new Array();
            
           var nbPositions = 15 + i;
           if(nbPositions > 250)
                nbPositions = 250;
           
            for(var j = 0; j < nbPositions; j++)
            {
                var num = Math.floor((Math.random() * (i-1)) + 0);  // Choix du noeud aléatoire
                var numPosition = Math.floor((Math.random() * 4) + 1); // Choix de la position
                tabPos.push(new Vector2(0, 0));
                
                if(numPosition === 1)
                    tabPos[tabPos.length-1] = new Vector2(this.noeuds[num].pos.x + (this.noeuds[num].taille + this.noeuds[i].taille) * 1.5, this.noeuds[num].pos.y);
                else if(numPosition === 2)
                    tabPos[tabPos.length-1] = new Vector2(this.noeuds[num].pos.x - (this.noeuds[num].taille + this.noeuds[i].taille) * 1.5, this.noeuds[num].pos.y);
                else if(numPosition === 3)
                    tabPos[tabPos.length-1] = new Vector2(this.noeuds[num].pos.x, this.noeuds[num].pos.y + (this.noeuds[num].taille + this.noeuds[i].taille) * 1.5);
                else if(numPosition === 4)
                    tabPos[tabPos.length-1] = new Vector2(this.noeuds[num].pos.x, this.noeuds[num].pos.y - (this.noeuds[num].taille + this.noeuds[i].taille) * 1.5);
                
                // Verifie si la position n'est pas en collision avec un autre noeud (sinon suppression)
                for(var k = 0; k < i; k++)
                {
                    if(tabPos[tabPos.length-1].distance(this.noeuds[k].pos) < this.noeuds[i].taille + this.noeuds[k].taille)
                    {
                        tabPos.pop();

                        break;
                    }
                }
            }
            
            // Trie des positions par rapport à leur distance du premier noeuds (plus proche possible)
            var changement = 0;			
            do {
                changement = 0;
                for(var j = 0; j < tabPos.length - 1; j++)
                {
                    if(tabPos[j].distance(this.noeuds[0].pos) > tabPos[j+1].distance(this.noeuds[0].pos))
                    {
                        var tempP = tabPos[j];
                        tabPos[j] = tabPos[j+1];
                        tabPos[j+1] = tempP;

                        changement ++;
                    }
                }
            } while(changement !== 0);

            // La position la plus proche du centre est choisie
            if(tabPos.length > 0)
               this.noeuds[i].pos = tabPos[0];
            else
                i --;
        }
    }
    
}