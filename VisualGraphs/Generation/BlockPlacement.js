
///////////////////////////////////////////////////////////////////////////////////////
// Classe permettant de gerer des blocke de noeuds utiliser pour placer les éléments //
///////////////////////////////////////////////////////////////////////////////////////
class BlockPlacement 
{
    constructor(tabIdNoeuds, numero)
    {
        this.numero = numero;
        this.noeuds = new Array(); // Tableau des noeuds contenu dans le block
        this.taille = new Vector2(0, 0); // Taille du block
        this.pos = new Vector2(0, 0); // Position centrale du block
        this.posMin = new Vector2(10000000, 10000000); // Position Minimum (Haut et Gauche)
        this.posMax = new Vector2(-10000000, -10000000); // Position Maximum (Bas et Droite)
        this.noeudsEnCoursPlacement = 0; // Indique si le placement est tooujours en cours
        
        // Récupération des noeuds à partir de leur ID
        for(var i = 0; i < tabIdNoeuds.length; i++)
        {
            this.noeuds.push(visualGraphs.recupNoeud(tabIdNoeuds[i]));
        }
    }
    
    /////////////////////////////////////
    // Gestion du placement des noeuds //
    /////////////////////////////////////
    placementNoeuds()
    {
        this.generationEnCours = true;
        var placementOK = false;
        
        if(this.noeuds.length === 0) // Si aucun noeuds
            placementOK = true; 
        else 
	{
            if(!this.noeuds[0].placer)
		this.noeuds[0].initPosition(0, 0); // placement du noeud centrale

            if(this.noeuds.length === 1) // Si un seul noeud
		placementOK = true;
	}
        
        // boucle de placement des noeuds
        placementOK = true;

        // Placement des noeuds par rapport à leurs liens
        var i = this.noeudsEnCoursPlacement;
        if(this.noeuds[i].placer)
        {
            for(var j = 0; j < this.noeuds[i].liens.length; j++)
            {
                if(!this.noeuds[i].liens[j].n1.placer)
                    this.noeuds[i].liens[j].n1.initPositionRelative(this.noeuds[i], this.noeuds);
                if(!this.noeuds[i].liens[j].n2.placer)
                    this.noeuds[i].liens[j].n2.initPositionRelative(this.noeuds[i], this.noeuds);
            }
        }

        this.noeudsEnCoursPlacement ++;
        if(this.noeudsEnCoursPlacement == this.noeuds.length) 
            this.noeudsEnCoursPlacement = 0;

        // Vérifie si tous les noeuds ont été placé correctement 
        for(var i = 0; i < this.noeuds.length; i++)
        {
            if(!this.noeuds[i].placer)
            {
                placementOK = false;
                break;
            }
        }		
        
        if(placementOK)
            this.generationEnCours = false;
    }
    
    ///////////////////////////////////////////////////////////////
    // Determine la taille du block selon la position des noeuds //
    ///////////////////////////////////////////////////////////////
    determinerTaille()
    {
        for(var i = 0; i < this.noeuds.length; i++)
        {
            if(this.noeuds[i].pos.x - this.noeuds[i].taille * 2 < this.posMin.x)
                this.posMin.x = this.noeuds[i].pos.x - this.noeuds[i].taille * 2;
            if(this.noeuds[i].pos.y - this.noeuds[i].taille * 2 < this.posMin.y)
                this.posMin.y = this.noeuds[i].pos.y - this.noeuds[i].taille * 2;
            
            if(this.noeuds[i].pos.x + this.noeuds[i].taille * 2 > this.posMax.x)
                this.posMax.x = this.noeuds[i].pos.x + this.noeuds[i].taille * 2;
            if(this.noeuds[i].pos.y + this.noeuds[i].taille * 2 > this.posMax.y)
                this.posMax.y = this.noeuds[i].pos.y + this.noeuds[i].taille * 2;
        }

        this.taille = new Vector2(this.posMax.x - this.posMin.x, this.posMax.y - this.posMin.y);
        
        // Deccallage des éléments par rapport au centre
        var decallage = new Vector2(this.posMin.x + this.taille.x/2, this.posMin.y + this.taille.y/2);
        for(var i = 0; i < this.noeuds.length; i++)
        {
            this.noeuds[i].pos.x -= decallage.x;
            this.noeuds[i].pos.y -= decallage.y;
        }
        // Replacement de la camera pour le noeud centrale
        if(this.numero == 0)
        {
            visualGraphs.camera.pos.x -= decallage.x;
            visualGraphs.camera.pos.y -= decallage.y;
        }
    }
    
    //////////////////////////////////////////////////////////////////
    // Gestion du placement du block, par rapport aux autres blocks //
    //////////////////////////////////////////////////////////////////
    placementBlock(tabBlock)
    {
        if(this.numero !== 0)
        {
            do {
                // Génération d'un tableau de 50 positions autours d'un block choisi aléatoirement
                var tabPos = new Array();
                for(var i = 0; i < 50; i++)
                {
                    var num = Math.floor((Math.random() * (this.numero-1)) + 0);  // Choix du block aléatoire
                    var numPosition = Math.floor((Math.random() * 8) + 1);  // Chois d'une position aléatoire ( par rapport au block )

                    tabPos.push(new Vector2(0, 0));

                    if(numPosition === 1)
                        tabPos[tabPos.length - 1] = new Vector2(tabBlock[num].pos.x + (tabBlock[num].taille.x / 2 + this.taille.x / 2) + 1, tabBlock[num].pos.y);
                    else if(numPosition === 2)
                        tabPos[tabPos.length - 1] = new Vector2(tabBlock[num].pos.x - (tabBlock[num].taille.x / 2 + this.taille.x / 2) - 1, tabBlock[num].pos.y);
                    else if(numPosition === 3)
                        tabPos[tabPos.length - 1] = new Vector2(tabBlock[num].pos.x, tabBlock[num].pos.y + (tabBlock[num].taille.y / 2 + this.taille.y / 2) + 1);
                    else if(numPosition === 4)
                        tabPos[tabPos.length - 1] = new Vector2(tabBlock[num].pos.x, tabBlock[num].pos.y - (tabBlock[num].taille.y / 2 + this.taille.y / 2) - 1);
                    else if(numPosition === 5)
                        tabPos[tabPos.length - 1] = new Vector2(tabBlock[num].pos.x + (tabBlock[num].taille.x / 2 + this.taille.x / 2), tabBlock[num].pos.y + (tabBlock[num].taille.y / 2 + this.taille.y / 2));
                    else if(numPosition === 6)
                        tabPos[tabPos.length - 1] = new Vector2(tabBlock[num].pos.x + (tabBlock[num].taille.x / 2 + this.taille.x / 2), tabBlock[num].pos.y - (tabBlock[num].taille.y / 2 + this.taille.y / 2));
                    else if(numPosition === 7)
                        tabPos[tabPos.length - 1] = new Vector2(tabBlock[num].pos.x - (tabBlock[num].taille.x / 2 + this.taille.x / 2), tabBlock[num].pos.y + (tabBlock[num].taille.y / 2 + this.taille.y / 2));
                    else if(numPosition === 8)
                        tabPos[tabPos.length - 1] = new Vector2(tabBlock[num].pos.x - (tabBlock[num].taille.x / 2 + this.taille.x / 2), tabBlock[num].pos.y - (tabBlock[num].taille.y / 2 + this.taille.y / 2));  

                    // Verifie si la position n'est pas en collision avec une autre block (sinon suppression)
                    for(var j = 0; j < this.numero; j++)
                    {
                        if (tabPos[tabPos.length - 1].x - this.taille.x / 2 < tabBlock[j].pos.x + tabBlock[j].taille.x / 2 &&
                        tabPos[tabPos.length - 1].x + this.taille.x / 2 > tabBlock[j].pos.x - tabBlock[j].taille.x / 2 &&
                        tabPos[tabPos.length - 1].y - this.taille.y / 2 < tabBlock[j].pos.y + tabBlock[j].taille.y / 2 &&
                        tabPos[tabPos.length - 1].y + this.taille.y / 2 > tabBlock[j].pos.y - tabBlock[j].taille.y / 2)
                        {
                            tabPos.pop();

                            break;
                        }
                    }
                }

                // Trie des positions par rapport à leur distance du centre du premier block (plus proche possible)
                var changement = 0;			
                do {
                    changement = 0;
                    for(var i = 0; i < tabPos.length - 1; i++)
                    {
                        if(tabPos[i].distance(tabBlock[0].pos) > tabPos[i+1].distance(tabBlock[0].pos))
                        {
                            var tempP = tabPos[i];
                            tabPos[i] = tabPos[i+1];
                            tabPos[i+1] = tempP;

                            changement ++;
                        }
                    }
                } while(changement !== 0);
                 
            } while(tabPos.length == 0);

            // La position la plus proche du centre est choisie
            this.pos = tabPos[0];

            // Deccallage des noeuds du block
            for(var i = 0; i < this.noeuds.length; i++)
            {
                this.noeuds[i].pos.x += this.pos.x;
                this.noeuds[i].pos.y += this.pos.y;
            }
        }
    }
}