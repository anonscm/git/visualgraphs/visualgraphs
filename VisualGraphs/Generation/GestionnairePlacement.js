
//////////////////////////////////////////////////////////
// Classe permettant de gerer le placement des éléments //
//////////////////////////////////////////////////////////
class GestionnairePlacement 
{
    constructor()
    {
        this.blocks = new Array(); // Tableau des blocks de placement
        
        this.generationEnCours = false; // Indique si la génération est encore en cours
        this.numBlockPlacementActuel = 0; // Numéro du block qui est actuelement en cours de placement
        
        this.rechargement; // Rechargement complet du graph ou pas
    }
    
    ////////////////////////////////////////////
    // Initialision du placement des élements //
    ////////////////////////////////////////////
    init(noeuds, rechargement)
    {
        this.rechargement = rechargement;
        
        if(rechargement)
        {
            var premierNoeudsTrier = 0;
            
            // Si on a fait une requête depuis un noeuds séléctionnée, on place ce noeuds dans la première case du tableau
            if(visualGraphs.elAffInterface != null && visualGraphs.elAffInterface.constructor.name === "Noeud")
            {
                premierNoeudsTrier = 1;
                var numPremierNoeud = 0;

                for(var i = 0; i < noeuds.length; i++)
                {
                    if(visualGraphs.elAffInterface.id == noeuds[i].id)
                    {
                        var temp = noeuds[0];
                        noeuds[0] = noeuds[i];
                        noeuds[i] = temp;
                    }
                }
            }
            
            // Tri des noeuds selon le nombre de lien
            for(var i = premierNoeudsTrier; i < noeuds.length - 1; i++)
            {
                for(var j = i+1; j < noeuds.length; j++)
                {
                    if(noeuds[i].liens.length < noeuds[j].liens.length)
                    {
                        var temp = noeuds[j];
                        noeuds[j] = noeuds[i];
                        noeuds[i] = temp;
                    }
                }
            }
           
            // Tri du nombre de liens de charque noeuds
            for(var i = 0; i < noeuds.length - 1; i++)
            {
                noeuds[i].trierLiens();
            }
              
            this.blocks = new Array();
        }
        
        // 
        if(rechargement)
            this.genererBlock(noeuds);
        else // Inutilisé !!
            this.ajoutNoeudsBlock(noeuds);
     
        this.generationEnCours = true;
        this.numBlockPlacementActuel = 0;
    }
    
    ///////////////////////////////////////////////////
    // Fonction permettant de placer tous nos noeuds //
    ///////////////////////////////////////////////////
    placementElement(chargementData)
    {
        var tempsDebut = new Date().getTime();
        
        do {
            
            this.blocks[this.numBlockPlacementActuel].placementNoeuds();

            if(!this.blocks[this.numBlockPlacementActuel].generationEnCours)
            {
                if(this.rechargement)
                {
                    this.blocks[this.numBlockPlacementActuel].determinerTaille();
                    this.blocks[this.numBlockPlacementActuel].placementBlock(this.blocks);
                }

                this.numBlockPlacementActuel ++;
            }
            
            // Si tous les blocks ont été placé
            if(this.numBlockPlacementActuel === this.blocks.length)
            {
                this.generationEnCours = false;
                
                visualGraphs.initGrapes();
                chargementData.fin();
                
                // Si un élément est charger dans l'interface, on zoom automatiquement sur lui (noeuds ou lien)
                if(visualGraphs.elAffInterface != null)
                {
                    if(visualGraphs.elAffInterface.constructor.name === "Noeud")
                        visualGraphs.camera.pos = new Vector2(visualGraphs.elAffInterface.pos.x, visualGraphs.elAffInterface.pos.y);
                    else if(visualGraphs.elAffInterface.constructor.name === "Lien")
                        visualGraphs.camera.pos = new Vector2((visualGraphs.elAffInterface.n1.pos.x + visualGraphs.elAffInterface.n2.pos.x) / 2, (visualGraphs.elAffInterface.n1.pos.y + visualGraphs.elAffInterface.n2.pos.y) / 2);
                }
                
                return;
            }
            
            var tempsActuel = new Date().getTime();
        
        } while(tempsActuel - tempsDebut < 25); // Executer le placement tant qu'il depasse pas les 25 millisecondes
        
    }

    ///////////////////////////
    // Génération des blocks //
    ///////////////////////////
    genererBlock(noeuds)
    {
        var blockSansLiens = false; // Variable permettant de vérifier s'il existe des blocks sans liens
        
        this.blocks = new Array();

        var nBlockActuel = new Array(); // ID des noeuds du block actuel
        var nLiensARecup = new Array(); // ID des noeuds dont les liens sont à récupérer

        var nLibres = new Array(); // Numero des noeuds non utilisés
        for(var i = 0; i < noeuds.length; i++)
            nLibres.push(noeuds[i].id);

        do {

            // Récupération du permier noeuds restant
            nLiensARecup.push(nLibres[0]);

            nBlockActuel.push(nLibres[0]);
            nLibres = nLibres.splice(1);

            // Création d'un block
            do {
                if(visualGraphs.recupNoeud(nLiensARecup[0]).liens.length == 0)
                    blockSansLiens = true;
                
                // Récupération des noeuds liées 
                for(var j = 0; j < visualGraphs.recupNoeud(nLiensARecup[0]).liens.length; j++)
                {
                    if(visualGraphs.recupNoeud(nLiensARecup[0]).liens[j].n1.id !== nLiensARecup[0])
                    {
                        if(nBlockActuel.indexOf(visualGraphs.recupNoeud(nLiensARecup[0]).liens[j].n1.id)  === -1)
                        {
                            nLiensARecup.push(visualGraphs.recupNoeud(nLiensARecup[0]).liens[j].n1.id);
                            nBlockActuel.push(visualGraphs.recupNoeud(nLiensARecup[0]).liens[j].n1.id);
                        }
                    }
                    else
                    {
                        if(nBlockActuel.indexOf(visualGraphs.recupNoeud(nLiensARecup[0]).liens[j].n2.id)  === -1)
                        {
                            nLiensARecup.push(visualGraphs.recupNoeud(nLiensARecup[0]).liens[j].n2.id);
                            nBlockActuel.push(visualGraphs.recupNoeud(nLiensARecup[0]).liens[j].n2.id);
                        }
                    }
                }

                nLiensARecup = nLiensARecup.splice(1);

            } while(nLiensARecup.length > 0);

            // Suppresson des noeuds du block de la liste des noeuds libres
            for(var i = 0; i < nLibres.length; i++)
            {
                for(var j = 0; j < nBlockActuel.length; j++)
                {
                    if(nLibres[i] === nBlockActuel[j])
                    {
                        var elASuppr = nLibres[i];

                        for(var k = i; k < nLibres.length - 1; k++)
                        {
                            nLibres[k] = nLibres[k+1];
                        }

                        nLibres[nLibres.length - 1] = elASuppr;
                        nLibres.pop();

                        i--;
                    }
                }
            }

            // Création du block
            this.blocks.push(new BlockPlacement(nBlockActuel, this.blocks.length));
            nBlockActuel = new Array();

        } while(nLibres.length > 0);
        
        if(blockSansLiens)
            this.creationBlockSansLiens();
    }
    
    /////////////////////////////////////////////////////
    // Ajout des noeuds non placés au blocks existants //
    /////////////////////////////////////////////////////
    creationBlockSansLiens()
    {
        var blockSansLiens = new BlockPlacementSansLiens(new Array(), 0); // Création du block
        
        // Suppression des block avec un seul noeuds et récupération de leurs noeuds
        for(var i = 0; i < this.blocks.length; i++)
        {
            if(this.blocks[i].noeuds.length == 1)
            {
                blockSansLiens.noeuds.push(this.blocks[i].noeuds[0]);
                
                this.blocks.splice(i, 1);
                i--;
            }
        }
        
        blockSansLiens.numero = this.blocks.length; // Initialisation du numero du block selon le nombre de blocks restants
        this.blocks.push(blockSansLiens);
    }
    
    //////////////////////////////////////////////////////
    // Ajout des noeuds non placés aux blocks existants //
    //////////////////////////////////////////////////////
    ajoutNoeudsBlock(noeuds)
    {
        var liee = false;
        
        for(var i = 0; i < noeuds.length; i++)
        {
            if(!noeuds[i].placer)
            {
                liee = false;
                
                for(var j = 0; j < noeuds[i].liens.length; j++)
                {
                    for(var k = 0; k < this.blocks.length; k++)
                    {
                        for(var l = 0; l < this.blocks[k].noeuds.length; l++)
                        {
                            if(noeuds[i].liens[j].n1.id === this.blocks[k].noeuds[l].id && !liee)
                            {
                                this.blocks[k].noeuds.push(noeuds[i]);
                                liee = true;
                            }
                            if(noeuds[i].liens[j].n2.id === this.blocks[k].noeuds[l].id && !liee)
                            {
                                this.blocks[k].noeuds.push(noeuds[i]);
                                liee = true;
                            }
                        } 
                    }
                }
            }
        }
    }
    
}
  