
///////////////////////////////////////////////////////////////
// Classe correspondant à une position ou une taille en x, y //
///////////////////////////////////////////////////////////////
class Vector2 
{
    constructor(x, y)
    {
        this.x = x;
  	this.y = y;
    }
  
    //////////////////////////////////////////////////
    // Calcule de la distance avec un autre Vector2 //
    //////////////////////////////////////////////////
    distance(v)
    {
        return Math.sqrt(Math.pow(v.x-this.x,2)+Math.pow(v.y-this.y,2));
    }
    
    ////////////////////////////////////
    // Addition avec un autre Vector2 //
    ////////////////////////////////////
    addition(v)
    {
        return new Vector2(this.x + v.x, this.y + v.y);
    }
}

