
///////////////////////////////////////////////////////////////////////////////////
// Classe permettant de récupérer les données du visuel des éléments d'affichage //
///////////////////////////////////////////////////////////////////////////////////
class StyleGraph
{
    constructor(visualGraphs)
    {

    }
    
    //////////////////////////////////////////////////
    // Lance la requête de récupération des données //
    //////////////////////////////////////////////////
    recupData(VisualGraphs)
    {
        VisualGraphs.xhr = new XMLHttpRequest();
        
        VisualGraphs.xhr.onload = function() 
        {
            if (VisualGraphs.xhr.readyState === 4) 
            {
                if (VisualGraphs.xhr.status === 200) 
                {
                    try
                    {
                        var tabResult = JSON.parse(VisualGraphs.xhr.responseText);
                        
                        VisualGraphs.style.init(tabResult);
                    }
                    catch (e) 
                    {
                        alert("Echec du chargement du fichier de design (Style.json)");
                    }
                }
                else 
                {
                    alert("Echec du chargement du fichier de design (Style.json)");
                }
            }
        };
        
        VisualGraphs.xhr.open("POST", "json/Style.json", false);
        VisualGraphs.xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        VisualGraphs.xhr.send(null);
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // Intialisation du design à partir des informations passé en paramètres //
    ///////////////////////////////////////////////////////////////////////////
    init(tabDonnes)
    {
        this.noeuds = tabDonnes['Noeuds'];
        this.liens = tabDonnes['Liens'];
        this.cadre = tabDonnes['Cadre'];
        
        this.affichageTexteLiens = this.cadre['AffichageTexteLiens']; // Boolean indiquand si on affiche le texte de description des liens

        this.majStyleCadre();
    }

    /////////////////////////////////////////////////////////////////////////
    // Mise à jour de Design du cadre à partir des informations récupérées //
    /////////////////////////////////////////////////////////////////////////
    majStyleCadre(visualGraphs)
    {
        if(this.cadre["CouleurFond"] != null)
            $('canvas').css('background-color', this.cadre["CouleurFond"]);
    }

    ///////////////////////////////////////////////
    // Vérifie si le noeud et son élément existe //
    ///////////////////////////////////////////////
    verifElementNoeudExistant(label, nomEl)
    {
        if(typeof this.noeuds[label] != "undefined" && this.noeuds[label][nomEl] != undefined)
            return true;
        else
            return false;
    }

    //////////////////////////////////////////
    // Récupération d'un élément d'un noeud //
    //////////////////////////////////////////
    RecupElementNoeud(label, nomEl, defaut)
    {
        if(this.verifElementNoeudExistant(label, nomEl))		
            return this.noeuds[label][nomEl];
        else if(this.verifElementNoeudExistant("Defaut", nomEl))	
            return this.noeuds["Defaut"][nomEl];
        else
            return defaut;
    }

    //////////////////////////////////////////////
    // Vérifie si le lien et son élément existe //
    //////////////////////////////////////////////
    verifElementLienExistant(typ, nomEl)
    {
        if(typeof this.liens[typ] != "undefined" && typeof this.liens[typ][nomEl] != "undefined")
            return true;
        else 
            return false;
    }

    /////////////////////////////////////////
    // Récupération d'un élément d'un lien //
    /////////////////////////////////////////
    RecupElementLien(typ, nomEl, defaut)
    {
        if(this.verifElementLienExistant(typ, nomEl))		
            return this.liens[typ][nomEl];
        else if(this.verifElementLienExistant("Defaut", nomEl))	
            return this.liens["Defaut"][nomEl];
        else
            return defaut;
    }

}

