
//////////////////////////////////////////
// Classe permettant de gerer la Camera //
//////////////////////////////////////////
class Camera 
{
    constructor(tailleX, tailleY)
    {
        this.vitesseScroll = 10; // Vitesse de deplacement de la camera;

	this.pos = new Vector2(0, 0); // Position centrale de la camera
	this.dep = new Vector2(0, 0); // Quantitée de deplacement de la camera à chaque tours de boucle
	this.taille = new Vector2(tailleX, tailleY);	// Taille de la camera    	
    	
        this.zoom = 1.00; // Zoom d'affichage des éléments
        this.zoomMax = 5.00; // Zoom MAX permis
        this.zoomMin = 0.10; // Zoom MIN permis 
    	
        this.deplacementSouris = false;
        this.positionSouris = new Vector2(0, 0);
    }
  	
    //////////////////////////////////////////////////////////////////////////////////
    // Fonction appellé continuellement permet de gerer le deplacement de la camera //
    //////////////////////////////////////////////////////////////////////////////////
    gestionDeplacement()
    {
        this.pos.x += this.dep.x / this.zoom;
	this.pos.y += this.dep.y / this.zoom;
    }
  	
    //////////////////////////////////////////////////////////////////////
    // Gestion de la modification du zoom (vers la position du curseur) //
    //////////////////////////////////////////////////////////////////////
    modifZoom(modif, x, y)
    {
        var posElement = this.recupPositionReel(x, y);
	var oldZoom = this.zoom;
  		
        this.zoom *= modif;

        if(this.zoom < this.zoomMin)
            this.zoom = this.zoomMin;
        else if(this.zoom > this.zoomMax)
            this.zoom = this.zoomMax;
  			
        var pixelperdu = new Vector2(this.taille.x/oldZoom - this.taille.x/this.zoom, this.taille.y/oldZoom - this.taille.y/this.zoom);
  		
        this.pos.x += ((posElement.x-this.pos.x) / (this.taille.x/2)) * (pixelperdu.x/2) * this.zoom;
        this.pos.y += ((posElement.y-this.pos.y) / (this.taille.y/2)) * (pixelperdu.y/2) * this.zoom;
    }
  	
    //////////////////////////////////////////////////
    // Récupération de la position d'affichage en X //
    //////////////////////////////////////////////////  
    recupPosAffX(x)
    {
	return (x - this.pos.x) * this.zoom + this.taille.x/2;
    }

    //////////////////////////////////////////////////
    // Récupération de la position d'affichage en Y //
    //////////////////////////////////////////////////    	
    recupPosAffY(y)
    {
        return (y - this.pos.y) * this.zoom + this.taille.y/2;
    }
  	
    //////////////////////////////////////////////////////////////////////////////////
    // Récupération de la position d'un élément à partir de sa position d'affichage //
    //////////////////////////////////////////////////////////////////////////////////
    recupPositionReel(x, y)
    {
	var v = new Vector2(0, 0);
	v.x = (-this.taille.x/2 + x) /this.zoom + this.pos.x;
	v.y = (-this.taille.y/2 + y) /this.zoom + this.pos.y; 	
		
	return v;
    }
  	
    //////////////////////////////////////////////////
    // Initialisation du deplacement avec la souris // 
    //////////////////////////////////////////////////
    initDeplacementSouris(x, y)
    {
        this.deplacementSouris = true;
  	this.positionSouris = new Vector2(x, y);
    }
  	
    ///////////////////////////////////////////
    // Gestion du deplacement avec la souris //
    ///////////////////////////////////////////
    gestionDeplacementSouris(x, y)
    {
        this.pos.x += (this.positionSouris.x - x) / this.zoom;
  	this.pos.y += (this.positionSouris.y - y) / this.zoom;
  		
  	this.positionSouris = new Vector2(x, y);
    }
}

