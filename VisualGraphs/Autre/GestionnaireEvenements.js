
///////////////////////////////////////////////
// Classe permettant de gerer les évènements //
///////////////////////////////////////////////
class GestionnaireEvenements 
{
    constructor()
    {
        this.gestionEvenementsSouris();
	this.gestionEvenementsClavier();
    }
  
    ///////////////////////////////////////////////////////////////////////////
    // Gestion des évènements souris permettant l'interaction avec le canvas //
    ///////////////////////////////////////////////////////////////////////////
    gestionEvenementsSouris()
    {
        // Lorsque la souris est préssé, l'élément est séléctionné
	canvas.addEventListener("mousedown", function(event) {
			
            //var x = event.pageX - canvas.offsetLeft;
            //var y = event.pageY - canvas.offsetTop;
            var posSouris = GestionnaireEvenements.recupPositionSouris(event);
            
            //alert("X : " + x + " || Y : " + y);

            // Gestion de la selection des liens
            visualGraphs.liens.forEach(function(l)
            {
                if(!visualGraphs.chargement.actif && !l.cacher && l.VerifVisible(visualGraphs.camera) && l.verifCollision(posSouris.x, posSouris.y, visualGraphs.camera))
                {
                    visualGraphs.elAffInterface = l;
                    
                    visualGraphs.griserElements(l);
                    
                    visualGraphs.interf.selectionElement(l);
                }
            });

            // Gestion de la selection des noeuds
            visualGraphs.noeuds.forEach(function(n)
            {
                if(!n.cacher && n.VerifVisible(visualGraphs.camera) && n.verifCollision(posSouris.x, posSouris.y, visualGraphs.camera))        
                {
                    visualGraphs.elSelect = n;
                    visualGraphs.elAffInterface = n;	

                    visualGraphs.griserElements(n);

                    visualGraphs.interf.selectionElement(n);
                }
            });

            if(visualGraphs.elSelect === null)
            {
                visualGraphs.camera.initDeplacementSouris(posSouris.x, posSouris.y);    
            }

	}, false);
		
        // Lorsque la souris est relaché; on désectionne l'element
	canvas.addEventListener("mouseup", function(event) {
			
            visualGraphs.elSelect = null;
			
            visualGraphs.degriserElements();
			
            visualGraphs.camera.deplacementSouris = false;
			
	}, false);
		
	// Lors du mouvement de la souris, si un élément est selectionné, on le deplace  ********** A RANGER *********
	canvas.addEventListener("mousemove", function(event) {
		
            //var x = event.pageX - canvas.offsetLeft;
	    //var y = event.pageY - canvas.offsetTop;
            var posSouris = GestionnaireEvenements.recupPositionSouris(event);
            
            if(visualGraphs.zoneLegend.zoneTitre.VerifCollision(new Vector2(posSouris.x, posSouris.y)))
                visualGraphs.zoneLegend.cacherTitre = true;
            else
                visualGraphs.zoneLegend.cacherTitre = false;
            
            if(visualGraphs.zoneTitre.zoneTitre.VerifCollision(new Vector2(posSouris.x, posSouris.y)))
                visualGraphs.zoneTitre.cacherTitre = true;
            else
                visualGraphs.zoneTitre.cacherTitre = false;
	    
	    if(visualGraphs.elSelect != null)
            {
                visualGraphs.elSelect.deplacer(posSouris.x, posSouris.y, visualGraphs.camera);
                
                if(visualGraphs.elSelect.grape != null)
                    visualGraphs.elSelect.grape.initPosition();
            }
            else if(visualGraphs.camera.deplacementSouris)
                visualGraphs.camera.gestionDeplacementSouris(posSouris.x, posSouris.y);    
            
            visualGraphs.interfCanvas.mouvementSouris(new Vector2(posSouris.x, posSouris.y));
		    	
	}, false);
		
	// Gestion du scroll de la souris
	var mousewheelevt=(/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x		
	canvas.addEventListener(mousewheelevt, function(event) {
		
            var evt=window.event || event; // equalize event object
            var delta=evt.detail? evt.detail*(-120) : evt.wheelDelta; // delta returns +120 when wheel is scrolled up, -120 when scrolled down
    	
            //var x = event.pageX - canvas.offsetLeft;
            //var y = event.pageY - canvas.offsetTop;
            var posSouris = GestionnaireEvenements.recupPositionSouris(event);
    	
            if(delta <= -120)  // Modification du zoom selon l'etat du 'mousewheelevt'
                visualGraphs.camera.modifZoom(0.9, posSouris.x, posSouris.y);
            else
                visualGraphs.camera.modifZoom(1.1, posSouris.x, posSouris.y);
    	
            event.preventDefault(); // Bloque le scrool de la fenetre du navigateur
	
	}, false);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Gestion des évènements clavier permettant l'interaction avec le canvas //
    ////////////////////////////////////////////////////////////////////////////
    gestionEvenementsClavier()
    {
	//document.onkeypress = keypressHandler;
  	document.onkeydown = this.keydownHandler;
  	document.onkeyup = this.keyupHandler;
    }
	
    ///////////////////////////////////////////////////////////
    // Gestion des évènements lorsqu'une touche est enfoncée //
    ///////////////////////////////////////////////////////////
    keydownHandler(evt) 
    {
        if(evt.keyCode === 37) // Fleche de Gauche
            visualGraphs.camera.dep.x = -visualGraphs.camera.vitesseScroll;
	else if(evt.keyCode === 39) // Fleche de Droite
            visualGraphs.camera.dep.x = visualGraphs.camera.vitesseScroll;
	else if(evt.keyCode === 40) // Fleche du Bas
            visualGraphs.camera.dep.y = visualGraphs.camera.vitesseScroll;
    	else if(evt.keyCode === 38) // Fleche du Haut
            visualGraphs.camera.dep.y = -visualGraphs.camera.vitesseScroll;
    }
	
    ///////////////////////////////////////////////////////////
    // Gestion des évènements lorsqu'une touche est relachée //
    ///////////////////////////////////////////////////////////
    keyupHandler(evt) 
    {
        if(evt.keyCode === 37) // Fleche de Gauche
            visualGraphs.camera.dep.x = 0;
    	else if(evt.keyCode === 39) // Fleche de Droite
            visualGraphs.camera.dep.x = 0;
	else if(evt.keyCode === 40) // Fleche du Bas
            visualGraphs.camera.dep.y = 0;
    	else if(evt.keyCode === 38) // Fleche du Haut
            visualGraphs.camera.dep.y = 0;
    }
    
    //////////////////////////////////////
    // Renvoit la position de la souris //
    //////////////////////////////////////
    static recupPositionSouris(event)
    {
        // return new Vector2(event.pageX - canvas.offsetLeft, event.pageY - canvas.offsetTop);
        return new Vector2(event.pageX, event.pageY);
    }
    
}
