
////////////////////////////////////////////////////////////////////////////
// Classe permettant de récupérer les données de paramétrage du programme //
////////////////////////////////////////////////////////////////////////////
class Params
{
    
    constructor()
    {
        
    }
    
    //////////////////////////////////////////////////
    // Lance la requête de récupération des données //
    //////////////////////////////////////////////////
    recupData(VisualGraphs)
    {
        VisualGraphs.xhr = new XMLHttpRequest();
        
        VisualGraphs.xhr.onload = function() 
        {
            if (VisualGraphs.xhr.readyState === 4) 
            {
                if (VisualGraphs.xhr.status === 200) 
                {
                    try
                    {
                        var tabResult = JSON.parse(VisualGraphs.xhr.responseText);
                        
                        VisualGraphs.params.init(tabResult);
                    }
                    catch (e) 
                    {
                        alert("Echec du chargement du fichier de paramétrage (Params.json)");
                    }
                }
                else 
                {
                    alert("Echec du chargement du fichier de paramétrage (Params.json)");
                }
            }
        };
        
        VisualGraphs.xhr.open("POST", "json/Params.json", false);
        VisualGraphs.xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        VisualGraphs.xhr.send(null);
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    // Intialisation des paramètres à partir des informations passé en paramètres //
    ////////////////////////////////////////////////////////////////////////////////
    init(tabDonnes)
    {
        this.titre = tabDonnes['General']['Titre']; // Titre de la base actuel
        this.imageIntro = tabDonnes['General']['ImageIntro']; // Image d'intro affichée au lancement du logiciel
        
        this.placement = tabDonnes['Placement']; // Récupération des données relatives au placement des noeuds
        
        this.boutonsRequetes = tabDonnes['BoutonsRequetes']; // Requete (Afficher dans menu recherche)
        this.boutonsRequetesNoeuds = tabDonnes['BoutonsRequetesNoeuds']; // Requetes liées au noeuds (Afficher lors de selection noeuds)
        this.boutonsRequetesLiens = tabDonnes['BoutonsRequetesLiens']; // Requetes liées au noeuds (Afficher lors de selection lien)
        
        this.date = tabDonnes['Date']; // Récupération des dates
    }

    ///////////////////////////////////////////////////////////////////////////////
    // Adapte et renvoit la chaine de requête de base choisi dans les paramètres //
    ///////////////////////////////////////////////////////////////////////////////
    recupRequeteBase()
    {
        var requeteAdater = this.requeteBase.replaceAll("'", '"');

        var result = "requete='" + requeteAdater + "'";

        return result;
    }

    ///////////////////////////////////////////////////////////////////////////////
    // Adapte et renvoit la chaine de requete de base choisi dans les paramètres //
    ///////////////////////////////////////////////////////////////////////////////
    recupRequete(requete)
    {
        var requeteAdater = requete.replaceAll("'", '"');

        var result = "requete='" + requeteAdater + "'";

        return result;
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    // Verifie si l'élement en paramètre à été défini comme une date dans le json //
    ////////////////////////////////////////////////////////////////////////////////
    verifDateElement(element, propriete)
    {
        if(element.constructor.name === "Noeud")
        {
            if(this.date != null && (typeof this.date["Noeuds"][element.labels[0]] !== 'undefined') && (typeof this.date["Noeuds"][element.labels[0]][propriete] !== 'undefined') && this.date["Noeuds"][element.labels[0]][propriete] == 1)
                return true;
        }
        else if(element.constructor.name === "Lien")
        {
            if(this.date != null && (typeof this.date["Liens"][element.type] !== 'undefined') && (typeof this.date["Liens"][element.type][propriete] !== 'undefined') && this.date["Liens"][element.type][propriete] == 1)
                return true;
        }
    
        return false;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Verifie si l'élement possedant le type et le nom en paramètre à été défini comme une date dans le json //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    verifDate(typeElement, nomElement, propriete)
    {
        if(typeElement === "noeud")
        {
            if((typeof this.date !== 'undefined') && (typeof this.date["Noeuds"] !== 'undefined') && (typeof this.date["Noeuds"][nomElement] !== 'undefined') && (typeof this.date["Noeuds"][nomElement][propriete] !== 'undefined') && this.date["Noeuds"][nomElement][propriete] == 1)
                return true;
        }
        else if(typeElement === "lien")
        {
            if((typeof this.date !== 'undefined') && (typeof this.date["Liens"] !== 'undefined') && (typeof this.date["Liens"][nomElement] !== 'undefined') && (typeof this.date["Liens"][nomElement][propriete] !== 'undefined') && this.date["Liens"][nomElement][propriete] == 1)
                return true;
        }
        
        return false;
    }
}

