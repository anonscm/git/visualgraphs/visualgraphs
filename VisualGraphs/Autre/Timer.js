
///////////////////////////////////
// Classe correspondant un Timer //
///////////////////////////////////
class Timer
{
    constructor(duree)
    {
        this.duree = duree; // Durée total du Timer en millisecondes
        
        this.tempsEcoule = 0; // Temps écoulé depuis le debut du lancement du Timer
        this.derniereDate = Date.now(); // Date de rentrée dans la dernière boucle
        
        this.fin = false; // Indique si le Timer est fini
    }
    
    ///////////////////////////////
    // Reinitialisation du Timer //
    ///////////////////////////////
    reInit()
    {
        this.derniereDate = Date.now();
        
        this.tempsEcoule = 0;
        
        this.fin = false;
    }
    
    ////////////////////////////////////////////////////
    // Mise à jour du Timer (renvoit true si terminé) //
    ////////////////////////////////////////////////////
    update()
    {
        var dateActuel = Date.now();
        var newTempsEcoule = dateActuel - this.derniereDate 
        
        this.tempsEcoule += newTempsEcoule;
        
        if(this.tempsEcoule >= this.duree)
        {
            this.fin = true;
            return true;
        }
        else
            return false;
    }
}