////////////////////////////////////////////////////////////////////////////
// Contient des fonctions qu'il est possible dans n'importe quelle classe //
////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////
// Fonction permettant de lancer un script PHP qui nous permet de récupérer des données Neo4J //
////////////////////////////////////////////////////////////////////////////////////////////////
function LancerRequete(params, lienScript, fonction, xhr, async=true)
{
    try 
    {
        if(xhr == null)
            xhr = new XMLHttpRequest();
        else
        {
            xhr.abort();
            xhr = new XMLHttpRequest();
        }
        
        xhr.onload = function() 
        {
            if (xhr.readyState === 4) 
            {
                if (xhr.status === 200) 
                {
                    try
                    {
                        var tabResult = JSON.parse(xhr.responseText);
                        
                        if (typeof tabResult.Erreur !== 'undefined')
                        {
                            alert(tabResult.Erreur);
                            
                            fonction(null);
                        }
                        else
                            fonction(tabResult);
                    }
                    catch (e) 
                    {
                        fonction(null);
                    }
                } 
                else 
                {
                    fonction(null);
                }
            }
        };
        
        xhr.open("POST", lienScript, async);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        xhr.send(params);
    }
    catch(e)
    {
        fonction(null);
    }
   
    return xhr; 
}

/////////////////////////////////////////////////////
// Récupération d'une position d'une courbe (lien) //
/////////////////////////////////////////////////////
function RecupPositionCourbe(point, multiplicateur)
{
    var x = (1 - multiplicateur) * (1 - multiplicateur) * point[0].x + 2 * (1 - multiplicateur) * multiplicateur * point[1].x + multiplicateur * multiplicateur * point[2].x;
    var y = (1 - multiplicateur) * (1 - multiplicateur) * point[0].y + 2 * (1 - multiplicateur) * multiplicateur * point[1].y + multiplicateur * multiplicateur * point[2].y;
    
    return new Vector2(x, y);
}

/////////////////////////////////////////////////////////
// Verifie si il y a une intersection entre 2 segments //
/////////////////////////////////////////////////////////
function IntersectSegment(A, B, I, P)
{
    var D = new Vector2(0, 0);
    var E = new Vector2(0, 0);

    D.x = B.x - A.x;
    D.y = B.y - A.y;
    E.x = P.x - I.x;
    E.y = P.y - I.y;

    var denom = D.x*E.y - D.y*E.x;
    if (denom==0)
        return -1;   // erreur, cas limite

    var t = - (A.x*E.y-I.x*E.y-E.x*A.y+E.x*I.y) / denom;

    if (t<0 || t>=1)
        return 0;

    var u = - (-D.x*A.y+D.x*I.y+D.y*A.x-D.y*I.x) / denom;

    if (u<0 || u>=1)
        return 0;

    return 1;
}

//////////////////////////////////////////////////////////////////////////////////////
// Remplace tous les caractère 'target' de la chaine par le caractère 'replacement' //
//////////////////////////////////////////////////////////////////////////////////////
String.prototype.replaceAll = function(target, replacement)
{
    return this.split(target).join(replacement);
};

////////////////////////////////////////////////////////
// Récupére une valeur transmi par la barre d'adresse //
////////////////////////////////////////////////////////
function $_GET(param) 
{
    var vars = {};
    window.location.href.replace( location.hash, '' ).replace( 
            /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
            function( m, key, value ) { // callback
                    vars[key] = value !== undefined ? value : '';
            }
    );

    if ( param ) {
            return vars[param] ? vars[param] : null;	
    }
    return vars;
}

/////////////////
//
/////////////////
function ModifierCouleur(couleur, modificateur)
{
    // Récupération des couleur
    var rouge = parseInt(couleur.slice(1, 3), 16);
    var vert = parseInt(couleur.slice(3, 5), 16);
    var bleu = parseInt(couleur.slice(5, 7), 16);

    // 
    var modif = (modificateur-1) * 250;

    // Création de la partie la plus clair
    var r = Math.round(rouge * modificateur + modif);
    var v = Math.round(vert * modificateur + modif);
    var b = Math.round(bleu * modificateur + modif);

    if(modificateur < 1)
    {
        if(r < 0) r = 0;
        if(v < 0) v = 0;
        if(b < 0) b = 0;
    }
    else
    {
        if(r > 255) r = 255;
        if(v > 255) v = 255;
        if(b > 255) b = 255;
    }
    
    // Création de nouvelles couleur de bordure
    return "rgb("+r+", "+v+", "+b+")";
}


