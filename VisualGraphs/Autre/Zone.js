
//////////////////////////////////////////
// COrrespond à une zone dans le canvas //
//////////////////////////////////////////
class Zone 
{
    constructor(position, taille)
    {
        this.position = position; // position de la zone (Bord Haut-Gauche)
        this.taille = taille; // Taille de la zone
    }
    
    //////////////////////////////////////////////////////////
    // Verifie si la position est en collision avec la zone //
    //////////////////////////////////////////////////////////
    VerifCollision(pos)
    {
        if(this.position.x <= pos.x && this.position.y <= pos.y && this.position.x + this.taille.x >= pos.x && this.position.y + this.taille.y >= pos.y)
            return true;
        else
            return false;
    }
}

