
//////////////////////////////////////////////////////////////////////////////////////////////
// Classes permettant de gerer et d'afficher l'icone de chargement et d'echec du chargement //
//////////////////////////////////////////////////////////////////////////////////////////////
class IconeChargement
{
    constructor()
    {
        this.actif = false; // Etat d'activitée de l'icone de chargement
        this.aEchouer = false; // Le chargement à échoué
        
        this.imgChargement = new Image(); 
        this.imgChargement.src = 'img/chargement.png'; // Image de chargement en cours
        
        this.imgEchec = new Image(); 
        this.imgEchec.src = 'img/echec.png'; // Image d'echec du chargement
        
        this.angle = 0; // Angle actuel de l'image de chargement
    }
    
    //////////////////////////////////////////////
    // Initialisation du chargement des données //
    //////////////////////////////////////////////
    init()
    {
	this.actif = true;
        this.aEchouer = false;
    }
    
    ///////////////////////////////////
    // Fin du chargement des données //
    ///////////////////////////////////
    fin()
    {
	this.actif = false;
    }
    
    /////////////////////////
    // Echec du chargement //
    /////////////////////////
    echec()
    {
	this.aEchouer = true;
    }
    
    //////////////////////////////////////////////////////////////
    // Dessin de l'icone de chargement ou d'echec du chargement //
    //////////////////////////////////////////////////////////////
    dessin(camera, ctx)
    {
        if(this.actif)
        {
            ctx.save();
            
            ctx.translate(camera.taille.x/2, camera.taille.y/2);
            
            if(this.aEchouer)
            {
				/********** version paper.js **********/
				var raster = new Raster({
					source: this.imgEchec.src,
					position: view.center
				});
				/**************************************/
				
				/********** version html5 **********/
                //ctx.drawImage(this.imgEchec, -75, -75);
				/***********************************/
            }
            else
            {
                this.angle += 4;
				
				/********** version paper.js **********/
				var raster = new Raster({
					source: this.imgChargement.src,
					position: view.center
				});
				raster.rotate(this.angle);
				/******************************/
				
				/********** version html5 **********/
                /*ctx.rotate(this.angle*Math.PI/180);
                ctx.drawImage(this.imgChargement, -150, -150);*/
				/***********************************/
            }
            
            ctx.restore();
        }
    }
}