
<?php
	//////////////////////////////////////////////////////////////////
	// Script d'importation de la base de données Editef dans Neo4J //
	//////////////////////////////////////////////////////////////////
	
	set_time_limit(0);

	include 'Importeur.php';

	// Création de notre Importateur
	$importeur = new Importeur();
	$importeur->connexion("mysql", "localhost", "editef", "root", "", "neo4j", "msh");
	
	// Récupération des Noeuds
	// Récupération des personnes
	$r = "SELECT Id_personne as id, nom_personne as nom, prenom_personne as prenom, nom_vedette, pseudonyme, sexe, date_naissance_debut, date_naissance_fin, date_mort_debut, date_mort_fin FROM Personne";
	$importeur->recupNoeud($r, 'Personne', ['id', 'nom', 'prenom', 'nom_vedette', 'pseudonyme', 'sexe', 'date_naissance_debut', 'date_naissance_fin', 'date_mort_debut', 'date_mort_fin']);
	
	// Récupération des collectivité
	$r = "SELECT id_entreprise as id, nom_entreprise as nom, date_debut as date_debut, date_fin as date_fin FROM entreprise"; 
	$importeur->recupNoeud($r, 'Collectivité', ['id', 'nom', 'date_debut', 'date_fin']);
	
	// Récupération des lieux
	$r = "SELECT id_lieu as id, ville_norme_fr as nom_fr, ville_norme_pays as nom, ville_source as nom_source, pays, coord FROM lieu";
	$importeur->recupNoeud($r, 'Lieu', ['id', 'nom', 'nom_fr', 'nom_source', 'pays', 'coord']);
	
	// Récupération des éditions
	$r = "SELECT id_edition as id, auteur, titre, date_edition_debut, date_edition_fin	FROM edition";
	$importeur->recupNoeud($r, 'Edition', ['id', 'auteur', 'titre', 'date_edition_debut', 'date_edition_fin']);
	
	// Récupération des exemplaires
	$r = "SELECT id_exemplaire as id, date_possession as date_de_possession, lieu, bibliotheque as bibliothèque, CONCAT(bibliotheque, ' à ', lieu, ' ', cote) as affichage, cote FROM exemplaire"; 
	$importeur->recupNoeud($r, 'Exemplaire', ['id', 'date_de_possession', 'lieu', 'bibliothèque', 'affichage', 'cote']);
	
	// Récupération des professions
	$r = "SELECT id_profession as id, profession as nom FROM profession";
	$importeur->recupNoeud($r, 'Profession', ['id', 'nom']);
	
	// Récupération des étapes professionnel
	$r = "SELECT id_personne_entreprise as id, date_debut, date_fin, CONCAT(date_debut, ' - ', date_fin) as date FROM entreprise_personne"; 
	$importeur->recupNoeud($r, 'Etape_Professionnel', ['id', 'date_debut', 'date_fin', 'date']);

	// Récupération des Liens
	// Récupération des liens entre les étapes professionnel et les lieux
	$r = "SELECT e_p.Id_Personne_Entreprise as el1, e_p.Lieu as el2, 'Lieu_de_profession' as type_relation FROM entreprise_personne e_p";
	$importeur->recupLien($r, 'Lieu_de_profession', 'Etape_Professionnel', 'Lieu', []);
	
	// Récupération des liens entre les étapes professionnel et les type de profession
	$r = "SELECT e_p.Id_Personne_Entreprise as el1, e_p.Profession as el2, 'Type_de_profession' as type_relation FROM entreprise_personne e_p";
	$importeur->recupLien($r, 'Type_de_profession', 'Etape_Professionnel', 'Profession', []);
	
	// Récupération des liens entre les étapes professionnel et les personnes
	$r = "SELECT e_p.Personne as el1, e_p.Id_Personne_Entreprise as el2, 'A_effectué' as type_relation FROM entreprise_personne e_p";
	$importeur->recupLien($r, 'A_effectué', 'Personne', 'Etape_Professionnel', []);
	
	// Récupération des liens entre les étapes professionnel et les entreprises
	$r = "SELECT e_p.Id_Personne_Entreprise as el1, e_p.Entreprise as el2, 'Travaille_pour' as type_relation FROM entreprise_personne e_p";
	$importeur->recupLien($r, 'Travaille_pour', 'Etape_Professionnel', 'Collectivité', []);
	
	// Récupération des relations familiales entre les personnes
	$r = "SELECT rel_p.id_Personne as el1, rel_p.id_Personne_Famille as el2, 'Relation_Familiale' as type_relation, rel.type_relation as type, rel_p.Id_Relation_Personne as id FROM relation_personne rel_p, relation rel WHERE rel.id_Relation = rel_p.id_Relation";
	$importeur->recupLien($r, 'Relation_Familiale', 'Personne', 'Personne', ['type', 'id']);
	
	// Récupération des lieux ou sont situés les entreprises
	$r = "SELECT id_Entreprise as el1, id_Lieu as el2, 'Est_Situé_à' as type_relation, date_debut, date_fin FROM entreprise_lieu";
	$importeur->recupLien($r, 'Est_Situé_à', 'Collectivité', 'Lieu', ['date_debut', 'date_fin']);
	
	// Récupération des liaisons entre les exemplaire et les editions
	$r = "SELECT id_exemplaire as el2, id_Edition as el1, 'Edition_de' as type_relation FROM exemplaire";
	$importeur->recupLien($r, 'Edition_de', 'Edition', 'Exemplaire', []);
	
	// Récupération des exemplaires appartenant aux entreprises
	$r = "SELECT appartenance_entreprise as el2, id_exemplaire as el1, 'Appartient_à' as type_relation, (10000+id_exemplaire) as id FROM exemplaire";
	$importeur->recupLien($r, 'Appartien_à', 'Exemplaire', 'Collectivité', ['id']);
	
	// Récupération des personnes appartenant aux entreprises 
	$r = "SELECT appartenance_personne as el2, id_exemplaire as el1, 'Appartient_à' as type_relation, (20000+id_exemplaire) as id FROM exemplaire";
	$importeur->recupLien($r, 'Appartien_à', 'Exemplaire', 'Personne', ['id']);
	
	// Récupération des personnes imprimeurs des editions
	$r = "SELECT Imprimeur as el1, id_Edition as el2, 'Imprimeur_de' as type_relation FROM edition";
	$importeur->recupLien($r, 'Imprimeur_de', 'Personne', 'Edition', []);
	
	// Récupération des entreprises imprimeurs des editions
	$r = "SELECT Entreprise as el1, id_Edition as el2, 'Imprimeur_de' as type_relation FROM edition";
	$importeur->recupLien($r, 'Imprimeur_de', 'Collectivité', 'Edition', []);
	
	// Récupération des lieux d'édition
	$r = "SELECT id_Edition as el1, lieu_Edition as el2, 'Edité_à' as type_relation FROM edition";
	$importeur->recupLien($r, 'Edité_à', 'Edition', 'Lieu', []);
	
	// Récupération des lieux de naissance
	$r = "SELECT id_Personne as el1, lieu_naissance as el2, 'Né_à' as type_relation, date_naissance_debut as date_debut, date_naissance_fin as date_fin FROM personne";
	$importeur->recupLien($r, 'Né_à', 'Personne', 'Lieu', ['date_debut', 'date_fin']);
	
	// Récupération des lieux de mort
	$r = "SELECT id_Personne as el1, lieu_mort as el2, 'Mort_à' as type_relation, date_mort_debut as date_debut, date_mort_fin as date_fin FROM personne";
	$importeur->recupLien($r, 'Mort_à', 'Personne', 'Lieu', ['date_debut', 'date_fin']);
	
	
	echo "<br/>Import Terminé :)";

?>
