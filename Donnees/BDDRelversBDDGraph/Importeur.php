﻿
<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/VisualGraphs/VisualGraphs/vendorPHP/autoload.php';
	
	use GraphAware\Neo4j\Client\ClientBuilder;
	
	class Importeur
	{
		private $_clientNeo4J; // Connecteur de Neo4J
		private $_bdd; // Connecteur PDO de la BDD
		
		////////////////////////////////////////////////////
		// Initialisation de la connexion de NEO4J et PDO // 
		////////////////////////////////////////////////////
		function connexion($typeBDD, $nomServeur, $nomBDD, $userBDD, $mdpBDD, $userNeo4J, $mdpNeo4J)
		{
			// Connexion à Neo4J
			$this->_clientNeo4J = ClientBuilder::create()
				->addConnection('bolt', 'bolt://'.$userNeo4J.':'.$mdpNeo4J.'@localhost:7687')
				->build();
				
			// Vide la BDD NEO4J
			$this->_clientNeo4J->run("MATCH (n) DETACH DELETE n");

			// Init connexion de PDO
			$this->_bdd = new PDO($typeBDD.':host='.$nomServeur.';dbname='.$nomBDD.';charset=utf8', $userBDD, $mdpBDD);
		}
		
		/////////////////////////////////////////////////////////
		// Récupération et création des données de type Noeuds //
		/////////////////////////////////////////////////////////
		function recupNoeud($requete, $type, $proprietes)
		{
			echo "Récupération des données pour les noeuds $type <br/>";
			$donnees = $this->recupData($requete, $proprietes);
			
			echo "Ecriture des noeuds $type dans Neo4J <br/>";
			$this->creeNoeud($type, $donnees, $proprietes);
		}
		
		///////////////////////////////////////////////////////
		// Récupération et création des données de type Lien //
		///////////////////////////////////////////////////////
		function recupLien($requete, $type, $typeNoeud1, $typeNoeud2, $proprietes)
		{
			echo "Récupération des liaisons $type entre les noeuds $typeNoeud1 et $typeNoeud2 <br/>";
			$donnees = $this->recupData($requete, array_merge($proprietes, ['el1', 'el2', 'type_relation']));	
			
			echo "Ecriture des relations $type entre les noeuds $typeNoeud1 et $typeNoeud2 dans Neo4J <br/>";
			$this->creeLien($typeNoeud1, $typeNoeud2, $donnees, $proprietes);			
		}
		
		////////////////////////////////////////////
		// Récupération de données dans un noeuds //
		////////////////////////////////////////////
		function recupData($txtRequete, $proprietes)
		{
			$tabEl = array();
			
			$requete = $this->_bdd->query($txtRequete);
			while ($donnees = $requete->fetch())
			{
				$tabEl[count($tabEl)] = array();
				
				for($i = 0; $i < count($proprietes); $i++)
				{
					$tabEl[count($tabEl) - 1][$proprietes[$i]] = $donnees[$proprietes[$i]];	
				}		
			}
			
			return $tabEl;
		}

		/////////////////////////////////////
		// Création d'un Noeuds dans Neo4J //
		/////////////////////////////////////
		function creeNoeud($type, $tabEl, $tabProprietes)
		{
			foreach($tabEl as $el)
			{
				$proprietes = $this->initProprietes($el, $tabProprietes);
				
				$this->_clientNeo4J->run('CREATE (n:'.$type.' '.$proprietes.')');
			}
		}
		
		///////////////////////////////////
		// Création d'un Lien dans Neo4J //
		///////////////////////////////////
		function creeLien($typeEl1, $typeEl2, $tabEl, $tabProprietes)
		{
			$id = 0;
			
			foreach($tabEl as $el)
			{
				if($el["el1"] != null && $el["el2"] != null)
				{
					// Si il n'y a pas d'identifiant dans les paramètres en créer un automatiquement
					if (!in_array("id", $tabProprietes))
					{
						$id ++;
						$el['id'] = $id;
						
						$proprietes = $this->initProprietes($el, array_merge($tabProprietes, ['id']));	
					}
					else
						$proprietes = $this->initProprietes($el, $tabProprietes);	
					
					$this->_clientNeo4J->run('MATCH (el1:'.$typeEl1.' {id:'.$el["el1"].'}), (el2:'.$typeEl2.' {id:'.$el["el2"].'}) MERGE (el1)-[:'.$el["type_relation"].' '.$proprietes.']->(el2)');
				}
			}
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Création de propriétés pour un noeud ou un lien, à partir du tableau contenant les éléments, et d'une liste de nom de propriétés //
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		function initProprietes($tab, $nomProprite)
		{
			$nbProprietesValide = 0;
			$result = "{";
			
			foreach($nomProprite as $np)
			{
				if(isset($tab[$np]) && $tab[$np] != null && $tab[$np] != "")
				{
					$value = $tab[$np];
					
					$value = str_replace('"', '\'', $value );
					
					$value = stripslashes($value); // Retrait des antishash qui pourrait crée erreurs
					
					$nbProprietesValide ++;
					
					if (is_numeric($tab[$np]))
						$result .= $np.':'.$value.', ';
					else
						$result .= $np.':"'.$value.'", ';
				}
			}
			
			if($nbProprietesValide == 0)
				return "";
			else
				return substr(($result), 0, -2)."}";
		}
		
		///////////////////////////////////////////////////////
		// Retrait des caractère genant et mise en Majuscule //
		///////////////////////////////////////////////////////
		function adapterDate($j, $m, $a)
		{
			if($a == "")
				$a = 0;
			if($m == "")
				$m = 0;
			if($j == "")
				$j = 0;
			
			RETURN ($a * 10000) + ($m * 100) + $j;
		}
	}

?>
