﻿<?php

////////////////////////////////////////////////////////////////////////////////////////////
// Fichier contenant les fonctions d'écriture dans la base Neo4J à partir de fichiers CSV //
////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////
	// Ajout des noeuds du ficher CSV dans Neo4J //
	///////////////////////////////////////////////
	function ImportNoeuds($client, $nomFichier, $nomReelFichier)
	{
		$fichier = fopen($nomFichier, 'a+');
		
		$ligne = '';
		$requete = '';
		
		$reussi = true; // Boolean si le script à réussi
		
		// 
		$type_fichier = "";
		$tab_titres  = array();
		
		$numLigneDebug = 0;
		
		// Parcours du fichier
		while (!feof($fichier))
		{
			$numLigneDebug ++;

			$ligne = fgets($fichier, 4096); // lecture du contenu de la ligne
			$ligne = trim($ligne); // Retrait des sauts de lignes
			
			$tab_contenu = explode(";", $ligne);
			
			// Si le type est vide, on le récupère (Première ligne du fichier)
			if($type_fichier == "")
			{
				$type_fichier = RecupTypeFichier($tab_contenu[0]);
				
				if($type_fichier == "titre")
					$tab_titres = RecupTitreNoeuds($tab_contenu);
			}
			
			$label_noeuds = $tab_contenu[0];
			
			if($label_noeuds != "")
			{
				if(count($tab_contenu) == 1) 
					$requete = "CREATE (n:".$label_noeuds.")";
				else
				{
					$proprietes = RecupProprietes($tab_contenu, 1, $type_fichier, $tab_titres);
					
					$requete = "CREATE (n:".$label_noeuds." ".$proprietes.")";
				}
			}
			else
				$requete = '';
			
			if($requete != '')
			{
				 // echo $requete.'</br>'; // Affichage de la ligne de requete pour DEBUG
				
				try 
				{
					$client->run($requete);	
				}
				catch (Exception $e) 
				{
					echo '<p class="message_erreur_chargement_CSV">Erreur de syntaxe à la ligne ' . $numLigneDebug . ' de votre fichier '.$nomReelFichier .'</p>';
					$reussi = false;
				}
			}
			
			// echo $requete.'<br/>';
		}
		
		fclose($fichier);
		
		
		return $reussi;
	}
	
	//////////////////////////////////////////////
	// Ajout des liens du ficher CSV dans Neo4J //
	//////////////////////////////////////////////
	function ImportLiens($client, $nomFichier, $nomReelFichier)
	{
		$fichier = fopen($nomFichier, 'a+');

		$ligne = '';
		
		// 
		$type_fichier = "";
		$tab_titres  = array();
		
		$reussi = true; // Boolean si le script à réussi
		
		$numLigneDebug = 0;
		
		// Parcours du fichier
		while (!feof($fichier))
		{
			$numLigneDebug ++;
			
			$ligne = fgets($fichier, 4096); // lecture du contenu de la ligne
			$ligne = trim($ligne); // Retrait des sauts de lignes
			
			$tab_contenu = array();
			$tab_contenu = explode(";", $ligne);
			
			
			// Si le type est vide, on le récupère (Première ligne du fichier)
			if($type_fichier == "")
			{
				$type_fichier = RecupTypeFichier($tab_contenu[0]);
				
				if($type_fichier == "titre")
					$tab_titres = RecupTitreLiens($tab_contenu); // ATTENTION NE DOIT PAS EXECUTER LE CODE SUIVANT
			}
			
			$type_lien = $tab_contenu[0];
			
			if($type_lien != "")
			{
				$tab_noeud1 = RecupInfosNoeudsLiees($tab_contenu, $tab_titres, 1);
				$tab_noeud2 = RecupInfosNoeudsLiees($tab_contenu, $tab_titres, 2);
				
				// Gestion de la récupération des propriétés, seulement si il y en a au moins une
				$proprietes = "";
				if(count($tab_contenu) > 3)
				{
					$proprietes = RecupProprietes($tab_contenu, 3, $type_fichier, $tab_titres);
				}
				
				// Si les types ne sont pas vident on ajoute ':'
				if($tab_noeud1["type"] != '')
					$tab_noeud1["type"] = ':'.$tab_noeud1["type"];
				if($tab_noeud2["type"] != '')
					$tab_noeud2["type"] = ':'.$tab_noeud2["type"];
				
				$requete = 'MATCH (el1'.$tab_noeud1["type"].' {'.$tab_noeud1["type_propriete"].':'.$tab_noeud1["valeur_propriete"].'}), (el2'.$tab_noeud2["type"].' {'.$tab_noeud2["type_propriete"].':'.$tab_noeud2["valeur_propriete"].'}) MERGE (el1)-[:'.$type_lien.' '.$proprietes.']->(el2)';
				
			    // echo $requete.'<br/>'; // Affichage de la ligne de requete pour DEBUG
				
				try 
				{
					$client->run($requete);	
				}
				catch (Exception $e) 
				{
					echo '<p class="message_erreur_chargement_CSV">Erreur de syntaxe à la ligne ' . $numLigneDebug . ' de votre fichier '.$nomReelFichier.'</p>';
					$reussi = false;
				}
			}
			
			// echo $requete.'<br/>';
		}
		
		fclose($fichier);
		
		return $reussi;
	}
	
	
	////////////////////////////////////////////////////////////////////////
	// Récupération du type de fichier selectionné (avec Titre ou Normal) //
	////////////////////////////////////////////////////////////////////////
	function RecupTypeFichier($premiere_case)
	{
		if($premiere_case == "")
			$type_fichier = "titre";
		else
			$type_fichier = "normal";
		
		return $type_fichier;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Récupération des titres de propriètes pour un fichier de type Noeuds //
	//////////////////////////////////////////////////////////////////////////
	function RecupTitreNoeuds($tab_ligne)
	{
		$tab_titres = array();
		$tab_titres[0] = "";
		
		for($i = 1; $i < count($tab_ligne); $i++)
			$tab_titres[$i] = $tab_ligne[$i];
		
		return $tab_titres;
	}
	
	/////////////////////////////////////////////////////////////////////////
	// Récupération des titres de propriètes pour un fichier de type Liens //
	/////////////////////////////////////////////////////////////////////////
	function RecupTitreLiens($tab_ligne)
	{
		$tab_titres = array();
		$tab_titres[0] = "";
		
		for($i = 1; $i < count($tab_ligne); $i++)
			$tab_titres[$i] = $tab_ligne[$i];
		
		return $tab_titres;
	}
	
	///////////////////////////////////////////////////
	// Gestion de la conversion d'une date en nombre //
	///////////////////////////////////////////////////
	function GestionDate($ligne, $titre)
	{
		if($titre == null)
		{
			$ligne = str_replace("(date)", "", $ligne);
			$chaine_decouper = explode("=", $ligne);
			$tab_date = explode("/", $chaine_decouper[1]);
		}
		else 
		{
			$titre = str_replace("(date)", "", $titre);
			$tab_date = explode("/", $ligne);
		}
		
		// Gestion des différents format (j, m, a), (m, a), (a)
		if(count($tab_date) == 3)
		{
			$date = intval($tab_date[2]) * 10000;
			$date += intval($tab_date[1]) * 100;
			$date += intval($tab_date[0]) * 1;
		}
		else if(count($tab_date) == 2)
		{
			$date = intval($tab_date[1]) * 10000;
			$date += intval($tab_date[0]) * 100;
		}
		else if(count($tab_date) == 1)
		{
			$date = intval($tab_date[0]) * 10000;
		}
		
		if($titre == null)
			return $chaine_decouper[0].':'.$date;
		else
			return $titre.':'.$date;
		
		return '';
	}
	
	///////////////////////////////////////////////////////////////////////////////////////
	// Récupération des propriétés d'un noeuds ou d'un lien (génère chaine de caractère) //
	///////////////////////////////////////////////////////////////////////////////////////
	function RecupProprietes($tab_contenu, $num_debut, $type_fichier, $tab_titres)
	{
		$proprietes = "{";
		
		//echo var_dump($tab_contenu).'<br/>';
		
		for($i = $num_debut; $i < count($tab_contenu); $i++)
		{
			if($type_fichier == "titre")
			{
				if($tab_contenu[$i] != "" && $tab_contenu[$i] != " " && $tab_contenu[$i] != null)
				{
					// Gestion des Dates
					if(strstr($tab_titres[$i], '(date)'))
						$proprietes .= GestionDate($tab_contenu[$i], $tab_titres[$i]);
					else if(strstr($tab_titres[$i], '(texte)')) // Gestion de la mise en chaine caractère auto
					{
						$tab_titres[$i] = str_replace("(texte)", "", $tab_titres[$i]);
						$proprietes .= $tab_titres[$i].':"'.$tab_contenu[$i].'"';
					}	
					else 	// Création de la chaine contenant les propriètes
						$proprietes .= $tab_titres[$i].':'.$tab_contenu[$i];
				}
				
				if($i < count($tab_contenu) - 1 && $tab_contenu[$i+1] != '' && $tab_contenu[$i+1] != ' ')
				{
					$proprietes .= ', ';
				}
			}
			else if($type_fichier != "titre") // Type Normal
			{
				// Gestion des Dates
				if(strstr($tab_contenu[$i], '(date)'))
				{
					$tab_contenu[$i] = GestionDate($tab_contenu[$i], null);
				}
				else if(strstr($tab_contenu[$i], '(texte)')) // Gestion de la mise en chaine caractère auto
				{
					$tab_contenu[$i] = str_replace("(texte)", "", $tab_contenu[$i]);
					$tab_contenu[$i] = explode("=", $tab_contenu[$i])[0] . '="' . explode("=", $tab_contenu[$i])[1] . '"';
				}
				// Création de la chaine contenant les propriètes
				$proprietes .= str_replace("=", ":", $tab_contenu[$i]);
				
				if($i < count($tab_contenu) - 1 && strstr($tab_contenu[$i+1], '='))
				{
					$proprietes .= ', ';
				}
			}
		}
		
		$proprietes .= "}";
		
		return $proprietes;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////
	// Récupération des information de noeuds liée sur un document de description des liens //
	//////////////////////////////////////////////////////////////////////////////////////////
	function RecupInfosNoeudsLiees($tab_contenu, $tab_titres, $num_element)
	{
		$r = array();
		
		if(strstr($tab_contenu[$num_element], '=') && strstr($tab_contenu[$num_element], ':')) // Si format sans titre (toutes les infos sur la ligne)
		{
			$el = explode(":", $tab_contenu[$num_element]);
			$r['type'] = $el[0];
			$r['type_propriete'] = explode("=", $el[1])[0];
			$r['valeur_propriete'] = explode("=", $el[1])[1];
		}
		else // Si format avec titre
		{
			$el = explode(":", $tab_titres[$num_element]);
			$r['type'] = $el[0];
			$r['type_propriete'] = $el[1];
			$r['valeur_propriete'] = $tab_contenu[$num_element];
			
			if(strstr($r['type_propriete'], '(texte)')) // Gestion de la mise en chaine caractère auto
			{
				$r['type_propriete'] = str_replace("(texte)", "", $r['type_propriete']);
				$r['valeur_propriete'] = '"'.$r['valeur_propriete'].'"';
			}
		}
		
		return $r;
	}




?>
