﻿
<?php

set_time_limit(0); // Modification du temps d'éxécution maximal

echo '<html>';

	echo '<head>';
        echo '<meta charset="utf-8" />';
		echo '<link rel="stylesheet" type="text/css" href="../style.css">';
		echo '<link rel="icon" type="image/png" href="../VisualGraphs/img/favicon.png" />';
        echo '<title>VisualGraphs - Affichage contenu</title>';
    echo '</head>';
    
	require_once '../VisualGraphs/vendorPHP/autoload.php';
	use GraphAware\Neo4j\Client\ClientBuilder;
	
	include_once("../VisualGraphs/ScriptsPHP/Connexion.php");

    // Connexion à Neo4J
    $client = ClientBuilder::create()
            ->addConnection('bolt', RecupLoginNEO4J())
            ->build();
	
	// Recupération du contenu de la base
	$query = "MATCH (p) OPTIONAL MATCH (p)-[r]-() RETURN p, r"; // AJOUT NOEUDS SANS LIENS
	$result = $client->run($query);
	
	// Création des tableaux correpondant aux éléments
    $noeuds = array();
    $liens = array();
    $r = array();
	
	// Récupération des éléments (noeuds et liens)
	foreach ($result->getRecords() as $record)
    {
		$noeuds = RecupNoeud($noeuds, $record, 'p');
		$liens = RecupLien($liens, $record, 'r');
	}
	
	// Bouton de retour et titre
	echo '<form action="index.php" id="form_retour_menu"><button href= type="button" id="bouton_retour" ><img id="image_retour" src="../VisualGraphs/img/fleche_retour.png" />Retour</button></form>';

	echo '<h2 class="titre_menu_afficher_contenu">Contenu de la base de donnée</h2>';

	// Affichage du nombre de noeuds et de liens
	$nbNoeuds  = count($noeuds);
	$nbLiens  = count($liens);
	
	echo '<p>La base de donnée est composée '.count($noeuds).' Noeuds et de '.count($liens).' Liens.</p>';
	
	// Affichage infos des noeuds 
	echo '<h3 class="titre_menu_afficher_contenu">Noeuds : </h3>';
	echo '<table>';
	foreach ($noeuds as $n)
    {
		echo '<tr id="n'.$n["ID"].'">';
		echo '<td>'.$n["Labels"][0].'</td>';
		
		foreach($n["Values"] as $key => $value)
		{
			if(gettype($value) == "string")
				echo "<td>$key = '$value'</td>";
			else
				echo "<td>$key = $value</td>";
		}
		
		echo '</tr>';
	}
	echo '</table>';

	// Affichage infos des liens 
	echo '<h3 class="titre_menu_afficher_contenu">Liens : </h3>';
	echo '<table>';
	foreach ($liens as $l)
    {
		echo '<tr>';
		echo '<td>'.$l["Type"].'</td>';
		
		// Récupération des noeuds liées
		$n1 = null;
		$n2 = null;
		foreach ($noeuds as $n)
		{
			if($n["ID"] == $l["StartNode"])
			{
				$n1 = $n;
			}
			if($n["ID"] == $l["EndNode"])
			{
				$n2 = $n;
			}
		}
		
		// Affichage des noeuds liées
		echo '<td>';
			echo '<a href="#n'.$l["StartNode"].'">'.$n1["Labels"][0].'</a>';
			echo '<img src="../VisualGraphs/img/fleche.png" class="fleche" />';
			echo '<a href="#n'.$l["EndNode"].'">'.$n2["Labels"][0].'</a>';	
		echo '</td>';

		foreach($l["Values"] as $key => $value)
		{
			if(gettype($value) == "string")
				echo "<td>$key = '$value'</td>";
			else
				echo "<td>$key = $value</td>";
		}
		
		echo '</tr>';
	}
	echo '</table>';

echo '</html>';

	///////////////////////////////////////////////
    // Permet de récupérer le contenu d'un noeud //
    ///////////////////////////////////////////////
	function RecupNoeud($noeuds, $record, $nomN)
    {
        $a = $record->get($nomN);

        if($a != null)
        {
            $noeuds[$a->identity()] = array();
			$noeuds[$a->identity()]["ID"] = $a->identity();
            $noeuds[$a->identity()]["Labels"] = $a->labels();
            $noeuds[$a->identity()]["Values"] = $a->values();
        }

        return $noeuds;
    }
	
	//////////////////////////////////////////////
    // Permet de récupérer le contenu d'un lien //
    //////////////////////////////////////////////
    function RecupLien($lien, $record, $nomL)
    {
        $a = $record->get($nomL);

        if($a != null)
        {
            $lien[$a->identity()] = array();
            $lien[$a->identity()]["ID"] = $a->identity();
            $lien[$a->identity()]["Values"] = $a->values();
            $lien[$a->identity()]["Type"] = $a->type();
            $lien[$a->identity()]["StartNode"] = $a->startNodeIdentity();
            $lien[$a->identity()]["EndNode"] = $a->endNodeIdentity();
        }

        return $lien;
    }




?>
