
<?php

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fichier contenant les fonctions d'export des données contenu dans la base de donnée vers des documents CSV //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////
	// Gestion du lancement de l'export //
	//////////////////////////////////////
	function ExportDonnees($client)
	{
		// Recupération du contenu de la base
		$query = "MATCH (p) OPTIONAL MATCH (p)-[r]-() RETURN p, r"; // AJOUT NOEUDS SANS LIENS
		$result = $client->run($query);
		
		// Création des tableaux correpondant aux éléments
		$noeuds = array();
		$liens = array();
		$r = array();
		
		// Récupération des éléments (noeuds et liens)
		foreach ($result->getRecords() as $record)
		{
			$noeuds = RecupNoeudExport($noeuds, $record, 'p');
			$liens = RecupLienExport($liens, $record, 'r');
		}
		
		ExportNoeud($noeuds, "Export/Noeuds_Export.csv");
		ExportLien($liens, $noeuds, "Export/Liens_Export.csv");
		
		echo "<p class='message_donnees'>Les fichiers CSV d'export se trouvent dans le dossier 'Donnees/Export' ( <a href='Export/Noeuds_Export.csv'>Télécharger le fichier des Noeuds</a> - <a href='Export/Liens_Export.csv'>Télécharger le fichier des Liens</a> )</p>";
	}
	
	/////////////////////////////////////////////////////////
	// Ecriture des noeuds de la base vers un fichiers CSV //
	/////////////////////////////////////////////////////////
	function ExportNoeud($noeuds, $nomfichier)
	{
		// Si le fichier existe deja on le supprime
		if(file_exists($nomfichier))
			unlink($nomfichier);
		
		echo '<p class="message_donnees">Export des Noeuds</p>';
		
		$fp = fopen($nomfichier, "w+");
		
		foreach ($noeuds as $n)
		{
			fwrite($fp, $n["Labels"][0].';');  // Ecriture du label du noeud
			
			// Écriture des propriétés
			foreach($n["Values"] as $key => $value)
			{
				$value = str_replace(';', ',', $value);
				$value = str_replace("\r", '', $value);
				$value = str_replace("\n", '', $value);
				
				if(is_numeric($value))
					fwrite($fp, "$key=$value".";");
				else
					fwrite($fp, "$key(texte)=$value".";");
			}
			
			fwrite($fp, PHP_EOL);
			
			echo '</tr>';
		}
		fclose($fp);
		
		chmod($nomfichier, 0666); // Accorde les droits de lecture et d'écriture pour le fichier CSV
	}
	
	////////////////////////////////////////////////////////
	// Ecriture des liens de la base vers un fichiers CSV //
	////////////////////////////////////////////////////////
	function ExportLien($liens, $noeuds, $nomfichier)
	{
		// Si le fichier existe deja on le supprime
		if(file_exists($nomfichier))
			unlink($nomfichier);
		
		echo '<p class="message_donnees">Export des Liens</p>';
		
		$fp = fopen($nomfichier, "w+");
		
		foreach ($liens as $l)
		{
			fwrite($fp, $l["Type"].';'); // Ecriture du type du lien
			
			// Récupération des noeuds liées
			$n1 = null;
			$n2 = null;
			foreach ($noeuds as $n)
			{
				if($n["ID"] == $l["StartNode"])
					$n1 = $n;
				if($n["ID"] == $l["EndNode"])
					$n2 = $n;
			}
			
			// Ajout des élément liée : ATTENTION ne fonctionne qui si il possède la propriété 'id'
			fwrite($fp, $n1["Labels"][0].':id='.$n1["Values"]["id"].';');
			fwrite($fp, $n2["Labels"][0].':id='.$n2["Values"]["id"].';');

			// Écriture des propriétés
			foreach($l["Values"] as $key => $value)
			{
				if(is_numeric($value))
					fwrite($fp, "$key=$value".";");
				else
					fwrite($fp, "$key(texte)=$value".";");
			}
			
			fwrite($fp, PHP_EOL);
		}
		fclose($fp);
		
		chmod($nomfichier, 0666); // Accorde les droits de lecture et d'écriture pour le fichier CSV
	}
	
	///////////////////////////////////////////////
    // Permet de récupérer le contenu d'un noeud //
    ///////////////////////////////////////////////
	function RecupNoeudExport($noeuds, $record, $nomN)
    {
        $a = $record->get($nomN);

        if($a != null)
        {
            $noeuds[$a->identity()] = array();
			$noeuds[$a->identity()]["ID"] = $a->identity();
            $noeuds[$a->identity()]["Labels"] = $a->labels();
            $noeuds[$a->identity()]["Values"] = $a->values();
        }

        return $noeuds;
    }
	
	//////////////////////////////////////////////
    // Permet de récupérer le contenu d'un lien //
    //////////////////////////////////////////////
    function RecupLienExport($lien, $record, $nomL)
    {
        $a = $record->get($nomL);

        if($a != null)
        {
            $lien[$a->identity()] = array();
            $lien[$a->identity()]["ID"] = $a->identity();
            $lien[$a->identity()]["Values"] = $a->values();
            $lien[$a->identity()]["Type"] = $a->type();
            $lien[$a->identity()]["StartNode"] = $a->startNodeIdentity();
            $lien[$a->identity()]["EndNode"] = $a->endNodeIdentity();
        }

        return $lien;
    }

