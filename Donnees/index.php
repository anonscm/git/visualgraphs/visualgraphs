
<?php

////////////////////////////////////////////////////////////////////////////////////////////////
// Interface permetant d'importer des fichiers CSV et de placer les données sur la base Neo4J //
////////////////////////////////////////////////////////////////////////////////////////////////

set_time_limit(0); // Modification du temps d'éxécution maximal
ini_set('memory_limit', '-1');

echo '<html>';

	echo '<head>';
        echo '<meta charset="utf-8" />';
		
		echo '<link rel="stylesheet" type="text/css" href="../style.css">';
		echo '<script type="text/javascript" src="../VisualGraphs/libsJS/jquery-3.1.1.min.js"></script>';
		
		echo '<link rel="icon" type="image/png" href="../VisualGraphs/img/favicon.png" />';
        echo '<title>VisualGraphs - Gestion de données</title>';
    echo '</head>';
	
	echo '<form action="../" id="form_retour_menu"><button href= type="button" id="bouton_retour" ><img id="image_retour" src="../VisualGraphs/img/fleche_retour.png" />Retour</button></form>';

	// Chargement des fichiers pour permettre la connexion à Neo4j
	require_once '../VisualGraphs/vendorPHP/autoload.php';
	use GraphAware\Neo4j\Client\ClientBuilder;
	
	require_once 'fonctions_import.php';
	require_once 'fonctions_export.php';
	
	include_once("../VisualGraphs/ScriptsPHP/Connexion.php");
	
	echo "<h2>Gestion des Données</h2>";

    // Connexion à Neo4J + afficher message si erreur
    try {
		$client = ClientBuilder::create()
            ->addConnection('bolt', RecupLoginNEO4J())
            ->build();
        
        // Gestion des Actions à partir des données du formulaire
		if(isset($_GET['vider']) && $_GET['vider'] != null && $_GET['vider'] == "true") // Vide la base
			ViderBase($client);
        if(isset($_FILES['nom_fichier_noeuds'])) // Lance l'ajout de noeuds
			SauvegardeNoeuds($client, $_FILES['nom_fichier_noeuds']);
		if(isset($_FILES['nom_fichier_liens'])) // Lance l'ajout de liens
			SauvegardeLiens($client, $_FILES['nom_fichier_liens']);
		if(isset($_GET['export']) && $_GET['export'] != null && $_GET['export'] == "true") // Lance l'export des données vers des documents CSV
			ExportDonnees($client);
			
        $nbElements = RecupNbElementsBase($client);
        
        echo '<br/>';
        
	} catch (Exception $e) 
	{
		echo '<h3 style="color:red"> Echec de la connexion au serveur de Neo4J <br/><br/></h3>';

		return null;
	}
	
	echo '<p>La base de donnée est composée de '.$nbElements['noeuds'].' Noeuds et de '.$nbElements['liens'].' Liens</p><br/>';
	// Bouton d'accès à la page affichant la base de donnée
	echo '<p><a class="bouton_donnees" href="afficher_contenu.php">Afficher le contenu de votre base de données.</a></p>';
	// Bouton permettant de vider la base de donnée
	echo '<p><a id="bouton_vider_base" class="bouton_donnees">Vider la base de données</a></p><br/>';
	
	// Formulaire d'envoit des fichiers (Noeuds et Liens)
	echo '<form action="index.php" method="post" enctype="multipart/form-data"><fieldset class="fieldset_menu_donnees"><legend>Import de Fichiers CSV</legend>';
	
		echo "<p>Ce cadre permet d'ajouter des données sur votre base de données à partir de documents CSV.";
		echo "<br/>Plus d'informations dans <a href='../Documents/Notice_Utilisation_VisualGraphs.pdf'>la notice d'utilisation</a> (Chapitre 3).<br/><br/></p>";
		
		echo '<div class="input_menu_donnees" id="list_nom_fichier_noeuds">';
			echo '<label class="label_ajout_fichier">Fichiers CSV contenant des Noeuds : </label><input class="input_ajout_fichier" type="file" name="nom_fichier_noeuds[]" multiple />';
		echo '</div>';
		
		echo '<div id="list_nom_fichier_liens">';
			echo '<label class="label_ajout_fichier">Fichiers CSV contenant des Liens : </label><input class="input_ajout_fichier" type="file" name="nom_fichier_liens[]" multiple />';
		echo '</div><br/>';
		
		echo '<input type="submit" style="width:500px;height:30px" value="Valider" />';
	echo '</fieldset></form><br/>';
	
	// Formulaire d'export des données
	echo '<form action="index.php?export=true" method="post" enctype="multipart/form-data"><fieldset class="fieldset_menu_donnees"><legend>Export des données</legend>';
		echo "<br/><p>Ce cadre permet d'exporter les données contenu dans la base de donnée sous forme de fichiers CSV.</p><br/>";
		echo '<input type="submit" style="width:500px;height:30px" value="Exporter" />';
	echo '</form>';
	
	
	//////////////////////////////////////////////
	// Script permettant d'ajouter des fichiers //
	//////////////////////////////////////////////
	?>
		<script>
	
			// Ajout d'un selectionneur de fichier noeuds
			$( "#ajout_fichier_noeuds" ).click(function() {
			
			  $('#list_nom_fichier_noeuds').html($('#list_nom_fichier_noeuds').html() + '<label>Fichier CSV contenant des Noeuds : </label><input class="input_ajout_fichier" type="file" name="nom_fichier_noeuds[]" /><br/>');
			  return false;
			  
			});
			
			// Ajout d'un selectionneur de fichier liens 
			$( "#ajout_fichier_liens" ).click(function() {
				
			  $('#list_nom_fichier_liens').html($('#list_nom_fichier_liens').html() + '<label>Fichier CSV contenant des Liens : </label><input class="input_ajout_fichier" type="file" name="nom_fichier_liens[]" /><br/>');
			  return false;
			  
			});
			
			// Gestion d'ajout d'une validation lors que l'utilisateur veut vider la base de donnée
			$('#bouton_vider_base').click(function() {
				
				if (window.confirm("Voulez-vous vraiment vider la base de donnée ?")) 
				{ 
					document.location.href="index.php?vider=true";
				}
			  
			});
			
			
		
		</script>
	<?php
	
	echo '</html>';
	
	///////////////////////////////////
	// Vide la base de données Neo4J //
	///////////////////////////////////
	function ViderBase($client)
	{
		$client->run("MATCH (n) DETACH DELETE n");
		
		echo '<p class="message_donnees">Base de donnée vidée.</p>';
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Fonction permettant de récupérer le nombre de noeuds et de liens contenu dans notre base Neo4J //
	////////////////////////////////////////////////////////////////////////////////////////////////////
	function RecupNbElementsBase($client)
	{
		$r = array();
		
		$query = 'MATCH (p) RETURN count(p)';
		$result = $client->run($query);
		foreach ($result->getRecords() as $record)
			$r['noeuds'] = $record->value('count(p)');
		
		$query = 'MATCH ()-[r]->() RETURN count(r)';
		$result = $client->run($query);
		foreach ($result->getRecords() as $record)
			$r['liens'] = $record->value('count(r)');
		
		return $r;
	}
	
	///////////////////////////
	// Sauvegarde des noeuds //
	///////////////////////////
	function SauvegardeNoeuds($client, $fileNoeuds)
	{
		for($i = 0; $i < count($fileNoeuds['tmp_name']); $i++)
		{
			if($fileNoeuds['tmp_name'][$i] != '')
			{
				if(ImportNoeuds($client, $fileNoeuds['tmp_name'][$i], $fileNoeuds['name'][$i]))
					echo '<p class="message_donnees">Ajout des données du fichier de noeuds '.$fileNoeuds['name'][$i].'.</p>';
				else
					echo '<script>alert("Echec du chargement des Noeuds");</script>';
			}
		}
	}
	
	//////////////////////////
	// Sauvegarde des liens //
	//////////////////////////
	function SauvegardeLiens($client, $fileLiens)
	{
		for($i = 0; $i < count($fileLiens['tmp_name']); $i++)
		{
			if($fileLiens['tmp_name'][$i] != '')
			{
				if(ImportLiens($client, $fileLiens['tmp_name'][$i], $fileLiens['name'][$i]))
					echo '<p class="message_donnees">Ajout des données du fichier de liens '.$fileLiens['name'][$i].'.</p>';
				else
					echo '<script>alert("Echec du chargement des Liens");</script>';
			}
		}
	}

?>

</html>
